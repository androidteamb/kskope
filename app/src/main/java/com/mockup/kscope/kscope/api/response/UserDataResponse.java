package com.mockup.kscope.kscope.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mockup.kscope.kscope.api.model.User;

import java.util.List;

/**
 * Created by Serhii_Slobodianiuk on 03.11.2015.
 */

public class UserDataResponse extends Response {

    private UserRequestParam request_param = new UserRequestParam();
    public List<User> getUserData(){
        return request_param.getUser_data();
    }

    public UserRequestParam getRequestParam(){
        return request_param;
    }

    public static class UserRequestParam extends InvalidParam {
        private List<User> user_data;

        public List<User> getUser_data() {
            return user_data;
        }
    }
}
