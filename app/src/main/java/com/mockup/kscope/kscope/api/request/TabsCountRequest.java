package com.mockup.kscope.kscope.api.request;

public class TabsCountRequest extends Request {

    private TabsCount request_param;

    public TabsCountRequest(String sessionId, String userLogin) {
        setRequestCmd("get_tabs_info");
        setSessionId(sessionId);
        request_param = new TabsCount();
        request_param.user_login = userLogin;
    }

    private class TabsCount {
        @SuppressWarnings("unused")
        private String user_login;
    }

}
