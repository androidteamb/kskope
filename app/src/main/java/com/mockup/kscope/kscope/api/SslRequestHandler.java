package com.mockup.kscope.kscope.api;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mockup.kscope.kscope.api.request.Request;
import com.mockup.kscope.kscope.api.response.Response;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Alexander Rubanskiy on 30.10.2015.
 */

public class SslRequestHandler {


    public static int SERVER_PORT = 5252;
    public static String SERVER_IP = "52.34.106.138"; // new 52.34.106.138:5252    old 52.25.51.200

    public static String IMAGE_URL_PREFIX = "http://52.34.106.138/images";

    protected static final String TAG_RESPONSE = "Response";
    protected static final String TAG_REQUEST = "Request";

    protected static final String REQUEST_PROTOCOL = "TLS";
    protected static final String DEFAULT_CHARSET = "utf-8";

    private ExecutorService executor = Executors.newCachedThreadPool();

    private final Handler handler = new Handler(Looper.getMainLooper());

    private List<SslRequestListener<?>> callbacks = new CopyOnWriteArrayList<>();

    /**
     * Interface definition for a callback to be invoked when request
     * state was changed
     */
    public interface SslRequestListener <T> {
        /**
         * Callback method to be invoked when request has been started
         * Must be called on UI thread.
         */
        void onStart();
        /**
         * Called when request is successful
         *
         * @param response the response
         */
        void onSuccess(T response);
        /**
         * Called when request is failed
         *
         * @param errorMsg error message
         */
        void onFailure(String errorMsg);
        /**
         * Callback method to be invoked when request has been finished
         * Must be called on UI thread.
         */
        void onFinish();
    }

    private SslRequestHandler() {
    }

    private static class Holder {
        private static final SslRequestHandler INSTANCE = new SslRequestHandler();
    }

    public static SslRequestHandler getInstance() {
        return Holder.INSTANCE;
    }

    /**
     * Send request asynchronously
     *
     * @param request
     *            the request.
     * @param callback callback to be invoked when request state was changed
     * @param clazz class of response
     * @throws IllegalStateException
     *             when {@code request == null}
     */
    public <T extends Response> void sendRequest(final Request request, final Class<T> clazz,
                                                              final SslRequestListener<T> callback) {
        if(request == null) {
            throw new IllegalStateException("Request must not be null.");
        }

        callbacks.add(callback);

        handler.post(new Runnable() {
            @Override
            public void run() {
                callback.onStart();
            }
        });

        Runnable task = new Runnable() {
            @Override
            public void run() {
                if(!callbacks.contains(callback)) {
                    return;
                }

                T responseObj = sendRequestSync(request, clazz);

                if (!callbacks.contains(callback)) {
                    return;
                }

                if(responseObj != null) {
                    callback.onSuccess(responseObj);
                } else {
                    if (callback != null) {
                        callback.onFailure("Sorry, no connection to server. Please try again later.");
                    }
                }

                if (!callbacks.contains(callback)) {
                    return;
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (callback != null) {
                            callback.onFinish();
                        }
                    }
                });

                callbacks.remove(callback);
            }
        };

        executor.submit(task);
    }

    /**
     * Send request synchronously
     *
     * @param request
     *            the request.
     * @param clazz
     *            class of response
     * @return the response
     * @throws IllegalStateException
     *             when {@code request == null}
     */
    public <T> T sendRequestSync(Request request, Class<T> clazz) {

        if(request == null) {
            throw new IllegalStateException("Request must not be null.");
        }

        Socket socket = null;
        OutputStream outputStream = null;

        Gson gson = new GsonBuilder().create();

        try {
            SSLContext sslContext = SSLContext.getInstance(REQUEST_PROTOCOL);
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            socket = sslSocketFactory.createSocket(SERVER_IP, SERVER_PORT);
            socket.setSoTimeout(20 * 1000);
            outputStream = socket.getOutputStream();
            final byte[] postData = gson.toJson(request).getBytes();

            /** write specific bytes before data according to server logic */
            outputStream.write(new byte [] {33});
            outputStream.write(reverseByteArray(ByteBuffer.allocate(4).putInt(postData.length).array()));
            outputStream.write(postData);

            Log.v(TAG_REQUEST, gson.toJson(request));

            if (callbacks == null || callbacks.isEmpty()) {
                T emptyResponse = (T) "";
                return emptyResponse;
            }

            InputStream inputStream = socket.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, Charset.forName(DEFAULT_CHARSET)));

            StringBuilder response = new StringBuilder();
            String tmp;

            while ((tmp = br.readLine()) != null) {
                Log.v(TAG_RESPONSE, tmp);
                response.append(tmp);
            }

            T responseObj = gson.fromJson(response.toString(), clazz);

            return responseObj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if(outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                }
            }
            if(socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public void cancelAsyncRequest(SslRequestListener<?> callback) {
        callbacks.remove(callback);
    }

    public void cancelAllAsyncRequest() {
        callbacks.clear();
    }

    private final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public void checkClientTrusted(
                java.security.cert.X509Certificate[] certs, String authType) {
        }

        public void checkServerTrusted(
                java.security.cert.X509Certificate[] certs, String authType) {
        }
    } };

    private byte [] reverseByteArray(byte [] arr) {
        for (int i = 0; i < arr.length / 2; i++) {
            byte temp = arr[i];
            arr[i] = arr[arr.length - 1 - i];
            arr[arr.length - 1 - i] = temp;
        }
        return arr;
    }

}
