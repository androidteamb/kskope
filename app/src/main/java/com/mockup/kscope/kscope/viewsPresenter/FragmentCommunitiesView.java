package com.mockup.kscope.kscope.viewsPresenter;

import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.entity.ItemCommunities;

import java.util.List;

/**
 * Created by Stafiiyevskyi on 04.12.2015.
 */
public interface FragmentCommunitiesView {

    void entranceShow(Response response);
    void exitShow(Response response);
    void showCommunities(List<ItemCommunities> communities);

}
