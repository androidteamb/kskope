package com.mockup.kscope.kscope.events;

import android.net.Uri;

/**
 * Created by Serhii_Slobodianiuk on 30.11.2015.
 */
public class SelectedProfilePhotoEvent {
    private Uri photoUri;

    public SelectedProfilePhotoEvent(Uri photoUri) {
        this.photoUri = photoUri;
    }

    public Uri getPhotoUri() {
        return photoUri;
    }
}
