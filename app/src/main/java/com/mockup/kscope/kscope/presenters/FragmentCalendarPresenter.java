package com.mockup.kscope.kscope.presenters;

/**
 * Created by Stafiiyevskyi on 01.12.2015.
 */
public interface FragmentCalendarPresenter {

    void getCalendarEventPresenter(String time);
    void cancelLastGetCalendarEvents();

    void addEventToCalendarPresenter(String guidEvent, String time);
    void deleteEventFromCalendarPresenter(String guidEvent, String time);

}
