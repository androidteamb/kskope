package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 23.11.2015.
 */
public class AddLikeItemRequest extends Request {

    private AddLikeItem request_param;

    public AddLikeItemRequest(String guid_item, String sessionId){
        setRequestCmd("add_like");
        setSessionId(sessionId);
        request_param = new AddLikeItem();
        request_param.guid_item = guid_item;
    }

    private class AddLikeItem {
        @SuppressWarnings("unused")
        private String guid_item;
    }

}
