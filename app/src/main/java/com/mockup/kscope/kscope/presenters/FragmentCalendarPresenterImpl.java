package com.mockup.kscope.kscope.presenters;

import android.os.AsyncTask;

import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.AddEventToCalendarRequest;
import com.mockup.kscope.kscope.api.request.DeleteEventFromCalendarRequest;
import com.mockup.kscope.kscope.api.request.GetEventItemCalendarRequest;
import com.mockup.kscope.kscope.api.response.GetEventItemCalendarResponse;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.viewsPresenter.FragmentCalendarView;

/**
 * Created by Stafiiyevskyi on 01.12.2015.
 */
public class FragmentCalendarPresenterImpl implements FragmentCalendarPresenter {


    private FragmentCalendarView view;


    public FragmentCalendarPresenterImpl(FragmentCalendarView view) {
        this.view = view;
    }

    @Override
    public void getCalendarEventPresenter(String time) {
        SslRequestHandler.getInstance().sendRequest(new GetEventItemCalendarRequest(PreferencesManager.getInstance().getSessionId(), time), GetEventItemCalendarResponse.class, new SslRequestHandler.SslRequestListener<GetEventItemCalendarResponse>() {
            GetEventItemCalendarResponse getEventItemCalendarResponse;

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(GetEventItemCalendarResponse response) {
                getEventItemCalendarResponse = response;
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                if (getEventItemCalendarResponse!=null){
                    view.showEvents(getEventItemCalendarResponse.getEvents());
                }
            }
        });

    }

    @Override
    public void cancelLastGetCalendarEvents() {

        SslRequestHandler.getInstance().cancelAllAsyncRequest();
    }

    @Override
    public void addEventToCalendarPresenter(String guidEvent, String time) {
        SslRequestHandler.getInstance().sendRequest(new AddEventToCalendarRequest(PreferencesManager.getInstance().getSessionId(), guidEvent, time), Response.class, new SslRequestHandler.SslRequestListener<Response>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(Response response) {

            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {

            }
        });

//        AsyncAddEvent asyncAddEvent = new AsyncAddEvent(guidEvent,time);
//        asyncAddEvent.execute();
    }

    @Override
    public void deleteEventFromCalendarPresenter(String guidEvent, String time) {

        SslRequestHandler.getInstance()
                .sendRequest(new DeleteEventFromCalendarRequest(PreferencesManager
                        .getInstance().getSessionId(), guidEvent, time)
                        , Response.class, new SslRequestHandler.SslRequestListener<Response>() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(Response response) {

                    }

                    @Override
                    public void onFailure(String errorMsg) {

                    }

                    @Override
                    public void onFinish() {

                    }
                });
//        AsyncDeleteEvent asyncDeleteEvent = new AsyncDeleteEvent(guidEvent,time);
//        asyncDeleteEvent.execute();
    }



    private class AsyncGetCalendarEvents extends AsyncTask<Void, Void, GetEventItemCalendarResponse>{
        boolean isAsyncGetCalendarEventsRunning = true;
        String time;

        public AsyncGetCalendarEvents(String time) {
            this.time = time;
        }

        @Override
        protected GetEventItemCalendarResponse doInBackground(Void... params) {
            GetEventItemCalendarResponse response = null;
            while (isAsyncGetCalendarEventsRunning){
               response = SslRequestHandler.getInstance().sendRequestSync(new GetEventItemCalendarRequest(PreferencesManager.getInstance().getSessionId(), time), GetEventItemCalendarResponse.class);
                isAsyncGetCalendarEventsRunning = false;
            }

            return response;
        }

        @Override
        protected void onPostExecute(GetEventItemCalendarResponse getEventItemCalendarResponse) {
            super.onPostExecute(getEventItemCalendarResponse);
            if (getEventItemCalendarResponse!=null){
                view.showEvents(getEventItemCalendarResponse.getEvents());
            }

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            isAsyncGetCalendarEventsRunning = false;
        }
    }


    private class AsyncAddEvent extends AsyncTask<Void, Void, Response>{

        String guidItem;
        String time;

        public AsyncAddEvent(String guidItem, String time) {
            this.guidItem = guidItem;
            this.time = time;
        }

        @Override
        protected Response doInBackground(Void... params) {
            Response response = SslRequestHandler.getInstance().sendRequestSync(new AddEventToCalendarRequest(PreferencesManager.getInstance().getSessionId(), guidItem, time), Response.class);
            return response;
        }

    }

    private class AsyncDeleteEvent extends AsyncTask<Void, Void, Response>{

        String guidItem;
        String time;

        public AsyncDeleteEvent(String guidItem, String time) {
            this.guidItem = guidItem;
            this.time = time;
        }

        @Override
        protected Response doInBackground(Void... params) {
            Response response = SslRequestHandler.getInstance()
                    .sendRequestSync(new DeleteEventFromCalendarRequest(PreferencesManager
                            .getInstance().getSessionId(), guidItem, time)
                            , Response.class);
            return response;
        }

    }

}
