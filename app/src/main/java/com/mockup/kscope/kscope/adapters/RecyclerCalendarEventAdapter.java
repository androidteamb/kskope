package com.mockup.kscope.kscope.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.entity.ItemEvent;
import com.mockup.kscope.kscope.fragments.communitySections.FragmentCommunitySectionItem;
import com.mockup.kscope.kscope.utils.TimeUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static android.view.View.VISIBLE;

/**
 * Created by Stafiiyevskyi on 27.10.2015.
 */
public class RecyclerCalendarEventAdapter extends RecyclerView.Adapter<RecyclerCalendarEventAdapter.CalendarRecyclerViewHolder> {

    private static final int EVENT_NOT_ADDED = 0;
    private static final int EVENT_ADDED = 1;

    private Activity context;
    private List<ItemEvent> data = new ArrayList<>();
    private RecyclerView recyclerView;
    private EventCalendarAdapterCallback listener;

    public RecyclerCalendarEventAdapter(Activity context, RecyclerView recyclerView) {
        this.context = context;
        this.recyclerView = recyclerView;
    }

    @Override
    public CalendarRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == EVENT_ADDED){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_calendar_swipe_lay_added, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_calendar_swipe_lay_not_added, parent, false);
        }

        CalendarRecyclerViewHolder recyclerViewHolder = new CalendarRecyclerViewHolder(view);
        return recyclerViewHolder;


    }


    @Override
    public void onBindViewHolder(final CalendarRecyclerViewHolder holder, final int position) {

        holder.bindDayText(TimeUtil.getNumberDay(data.get(position).getTime()));
        holder.bindNameDayTime(TimeUtil.getNameDayTime(data.get(position).getTime()));
        holder.bindDescriptionEvent(data.get(position).getTitle());
        ((SwipeLayout) holder.itemView).getChildAt(1).setBackgroundColor(context.getResources().getColor(R.color.more_light_gray));
    }

    @Override
    public int getItemCount() {

        return data.size();

    }

    @Override
    public int getItemViewType(int position) {
        int result = data.get(position).getAdded().equalsIgnoreCase("0") ? EVENT_NOT_ADDED : EVENT_ADDED;
        return result;
    }

    public void setListener(EventCalendarAdapterCallback listener){
        this.listener = listener;
    }


    public void clearData() {
        this.data.clear();
    }


    public void addData(List<ItemEvent> data) {
        this.data.addAll(data);
    }

    public ItemEvent getItemFromAdapterByPosition(int position) {
        return data.get(position);
    }

    public interface EventCalendarAdapterCallback{

        void addEventToCalendar(ItemEvent event, int position);
        void deleteEventFromCalendar(ItemEvent event, int position);

    }

    public class CalendarRecyclerViewHolder extends RecyclerView.ViewHolder  implements OnClickListener{

        @InjectView(R.id.day)
        public TextView day;
        @InjectView(R.id.name_day_time)
        public TextView nameDayTime;
        @InjectView(R.id.description_event)
        public TextView descriptionEvent;
        @InjectView(R.id.swipe_calendar_item_layout)
        public SwipeLayout swipeLayout;
        @InjectView(R.id.delete_event_button)
        public ImageView cancel;
        @InjectView(R.id.confirm_event_button)
        public ImageView check;
        @InjectView(R.id.menu_event_button)
        public ImageView list;
        @InjectView(R.id.top_item_view)
        public LinearLayout topLinearLayout;
        @InjectView(R.id.delete_event_button_linear)
        LinearLayout deleteEvent;
        @InjectView(R.id.confirm_event_button_linear)
        LinearLayout confirmEvent;
        @InjectView(R.id.time_event_button_linear)
        LinearLayout time;
        @InjectView(R.id.menu_event_button_linear)
        LinearLayout menu;


        SwipeLayout.SwipeListener swipeListener = new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {

                topLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.more_light_gray));
                Log.e("On close", " onClose");
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onStartOpen(SwipeLayout layout) {
                topLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.white));
                Log.e("OnStartOpen", " OnStartOpen");
                int first = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                int last = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                for (int i = first; i <= last; i++) {

                    if (i != getAdapterPosition()) {

                        if (recyclerView.getLayoutManager().findViewByPosition(i) != null) {

                            ((SwipeLayout) recyclerView.getLayoutManager().findViewByPosition(i)).close();
                        }

                    }

                }
            }

            @Override
            public void onOpen(SwipeLayout layout) {

                topLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.white));
                Log.e("OnOpen", " onOpen");

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                topLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.more_light_gray));
                Log.e("On Start Close", " onStartClose");
            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        };

        public CalendarRecyclerViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            topLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.more_light_gray));
            ((SwipeLayout) itemView).addSwipeListener(swipeListener);

            deleteEvent.setOnClickListener(this);
            confirmEvent.setOnClickListener(this);
            topLinearLayout.setOnClickListener(this);

        }

        public void bindDayText(String dayText) {
            day.setText(dayText);
        }

        public void bindNameDayTime(String nameDayTimeText) {
            nameDayTime.setText(nameDayTimeText);
        }

        public void bindDescriptionEvent(String descriptionEventText) {
            descriptionEvent.setText(descriptionEventText);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.confirm_event_button_linear:
                    ((SwipeLayout) itemView).close();
                    listener.addEventToCalendar(data.get(getAdapterPosition()), getAdapterPosition());
                    break;
                case R.id.delete_event_button_linear:
                    ((SwipeLayout) itemView).close();
                    listener.deleteEventFromCalendar(data.get(getAdapterPosition()),getAdapterPosition() );
                    break;
                case R.id.top_item_view:
//                    SslRequestHandler.getInstance().cancelAllAsyncRequest();
                    v.setBackgroundResource(R.drawable.list_event_selector);
                    FragmentCommunitySectionItem fragment = new FragmentCommunitySectionItem();
                    Bundle bundle = new Bundle();
                    bundle.putString(MainActivity.ITEM_GUID, data.get(getAdapterPosition()).getGuid_item());
                    fragment.setArguments(bundle);
                    Toolbar toolbar = ButterKnife.findById((MainActivity)context, R.id.toolbar);
                    ButterKnife.findById(toolbar, R.id.menu_add_item_event).setVisibility(View.GONE);
                    ((MainActivity) context).addFragment(fragment, data.get(getAdapterPosition()).getTitle());
                    break;
                default: break;
            }
        }
    }
}
