package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 06.11.2015.
 */

public class GetSubsectionItemRequest extends Request {

    private GetSubsectionItem request_param;

    public GetSubsectionItemRequest(String guid_subsection, String guid_item, String id_user) {
        setRequestCmd("get_item");
        request_param = new GetSubsectionItem();
        request_param.guid_subsection = guid_subsection;
        request_param.guid_item = guid_item;
        request_param.id_user = id_user;
    }

    private class GetSubsectionItem {
        private String guid_subsection;
        private String guid_item;
        private String id_user;
    }

}
