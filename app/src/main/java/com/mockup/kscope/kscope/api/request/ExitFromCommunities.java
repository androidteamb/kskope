package com.mockup.kscope.kscope.api.request;

/**
 * Created by Serhii_Slobodianiuk on 10.11.2015.
 */
public class ExitFromCommunities extends Request {
    private ExitParams request_param;

    public ExitFromCommunities( String sessionId, String guidCommunity) {
        setRequestCmd("exit_from_community");
        setSessionId(sessionId);
        request_param = new ExitParams();
        request_param.guid_community = guidCommunity;
    }


    private class ExitParams {
        @SuppressWarnings("unused")
        private String guid_community;
    }
}
