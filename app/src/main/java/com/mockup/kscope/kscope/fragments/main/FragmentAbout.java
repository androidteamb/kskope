package com.mockup.kscope.kscope.fragments.main;

import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.AboutAdapter;
import com.mockup.kscope.kscope.customView.TypefaceTextView;
import com.mockup.kscope.kscope.entity.ItemAbout;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import java.util.ArrayList;

import butterknife.InjectView;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class FragmentAbout extends BaseFragment {

//    @InjectView(R.id.list_about)
//    ExpandableListView lvAbout;
    @InjectView(R.id.about_tv)
    TypefaceTextView aboutTV;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        AboutAdapter adapter = new AboutAdapter(getActivity(), initDummyData());
//        lvAbout.setAdapter(adapter);
//        lvAbout.setGroupIndicator(null);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_about;
    }

    @Override
    public void updateToolbar() {
        ((MainActivity) getActivity()).setActionBarTitle("About us", true);
    }

//    private ArrayList<ItemAbout> initDummyData() {
//        ArrayList<ItemAbout> about = new ArrayList<>();
//        ItemAbout about1 = new ItemAbout("CPC", "Phasellus sed urna vestibulum, blandit turpis a, eleif end erat. Aenean eget pulvinar tortor. Donec quam tellus, eleifend non dignissim consequat, egestas vellectus. Praesent neque lorem, commodo et accum san eu, pre tium vel lectus. Curabitur rutrum ligula sed magna lacinia consequat.");
//        ItemAbout about2 = new ItemAbout("Testimonials", "Phasellus sed urna vestibulum, blandit turpis a, eleif end erat. Aenean eget pulvinar tortor. Donec quam tellus, eleifend non dignissim consequat, egestas vellectus. Praesent neque lorem, commodo et accum san eu, pre tium vel lectus. Curabitur rutrum ligula sed magna lacinia consequat.");
//        about.add(about1);
//        about.add(about2);
//        return about;
//    }

}