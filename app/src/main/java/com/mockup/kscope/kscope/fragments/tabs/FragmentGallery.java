package com.mockup.kscope.kscope.fragments.tabs;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.GalleryAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.UserGalleryRequest;
import com.mockup.kscope.kscope.api.response.GetGalleryResponse;
import com.mockup.kscope.kscope.customView.CustomItemDecoration;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.fragments.Updatable;
import com.mockup.kscope.kscope.fragments.main.FragmentImagePreview;
import com.mockup.kscope.kscope.utils.PreferencesManager;

import java.util.ArrayList;

import butterknife.InjectView;

import static com.mockup.kscope.kscope.adapters.GalleryAdapter.Callback;
import static com.mockup.kscope.kscope.adapters.ViewPagerAdapter.FragmentLifecycle;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
@SuppressWarnings("ALL")
public class FragmentGallery extends BaseFragment implements FragmentLifecycle, Updatable, Callback {

    private static final String USER_ID = "user_id";

    @InjectView(R.id.gallery)
    RecyclerView mGalleryGrid;
    @InjectView(R.id.empty)
    LinearLayout emptyView;
    @InjectView(R.id.progress_bar)
    ProgressBar mProgressBar;

    private Context mContext;

    private GalleryAdapter mAdapter;
    private GridLayoutManager mLayoutManager;
    private ArrayList<String> images;

    private PreferencesManager preferenceManager = PreferencesManager.getInstance();

    private String userId;

    public static Fragment newInstance(String userId) {
        FragmentGallery fragmentGallery = new FragmentGallery();
        Bundle args = new Bundle();
        args.putString(USER_ID, userId);
        fragmentGallery.setArguments(args);
        return fragmentGallery;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userId = getArguments().getString(USER_ID);
        if (userId.equals("")) {
            userId = preferenceManager.getUserId();
        }
        //mGalleryGrid.setVisibility(View.VISIBLE);
        //emptyView.setVisibility(View.VISIBLE);
        initGrid(new ArrayList<String>());

        sendGalleryRequest();
//        loadItems();
    }

    private void loadItems() {
//        List<ItemGallery> assets = new ArrayList<>();
//        for (int i = 0; i < 20; i++) {
//            ItemGallery itemGallery = new ItemGallery();
//            itemGallery.setPhoto(R.drawable.rectangle);
//            itemGallery.setType(((i % 2) == 0) ? "videos" : "images");
//            assets.add(itemGallery);
//        }
//        mAdapter.addItems(assets);
    }

    private void initGrid(ArrayList<String> list) {
        mGalleryGrid.setVisibility(View.GONE);
        mAdapter = new GalleryAdapter(this, getActivity(), list);
        mGalleryGrid.setHasFixedSize(true);

        mLayoutManager = new GridLayoutManager(getActivity(), 3);
        mGalleryGrid.setLayoutManager(mLayoutManager);
        mGalleryGrid.setBackgroundResource(R.color.light_gray);
//        mGalleryGrid.setOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//
//                int visibleItemCount = mLayoutManager.getChildCount();
//                int totalItemCount = mLayoutManager.getItemCount();
//                int[] firstVisibleItems = new int[4];
//                int[] lastVisibleItems = new int[4];
////                firstVisibleItems = mLayoutManager.findFirstVisibleItemPositions(firstVisibleItems);
////                lastVisibleItems = mLayoutManager.findLastVisibleItemPositions(lastVisibleItems);
//                int firstVisiblePosition = mLayoutManager.findFirstVisibleItemPosition();
//                int lastVisiblePosition = mLayoutManager.findLastVisibleItemPosition();
//                //load new item
////                if (!loading) {
//////                    if ((visibleItemCount + firstVisibleItems[0]) >= totalItemCount) {
////                    if (lastVisibleItems[0] >= mCurrentCount - ITEM_LOAD_COUNT && mCurrentCount > 0) {
////                        loading = true;
////                        Log.d("RESTCOUNTER", "from scroll listener");
////                        loadMoreItems();
////                    }
////                }
//                //mAdapter.scroll(firstVisibleItems[0], lastVisibleItems[0]);
//                mAdapter.scroll(firstVisiblePosition, lastVisiblePosition);
//            }
//        });

        mGalleryGrid.addItemDecoration(new CustomItemDecoration(
                getResources().getDimensionPixelSize(R.dimen.gallery_padding)
        ));
        mGalleryGrid.setAdapter(mAdapter);
    }

    private void sendGalleryRequest() {
        UserGalleryRequest galleryRequest = new UserGalleryRequest(preferenceManager.getSessionId(), userId);
        SslRequestHandler.getInstance().sendRequest(galleryRequest, GetGalleryResponse.class, galleryListener);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_gallery;
    }

    @Override
    public void updateToolbar() {

    }

    @Override
    public void onPauseFragment() {

    }

    @Override
    public void update() {
        sendGalleryRequest();
    }

    private SslRequestHandler.SslRequestListener<GetGalleryResponse> galleryListener = new SslRequestHandler.SslRequestListener<GetGalleryResponse>() {
        public GetGalleryResponse response;

        @Override
        public void onStart() {
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onSuccess(GetGalleryResponse response) {
            this.response = response;
        }

        @Override
        public void onFailure(String errorMsg) {
            Toast.makeText(mContext, errorMsg.toString(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onFinish() {
            if (!isVisible()) {
                return;
            }
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }
            if (response != null) {
                mGalleryGrid.setVisibility(View.VISIBLE);
                mAdapter.setItems(response.getRequestParam().getConvertedImages());
                images = response.getRequestParam().getImages();
                if (response.getRequestParam().getDataCount().equals("0")) {
                    mGalleryGrid.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);
                }
            } else {
                mGalleryGrid.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public void onClick(int itemPosition) {
        if (images != null) {
            ((MainActivity) getActivity()).addFragment(FragmentImagePreview.newInstance(images.get(itemPosition)), "My Gallery");
        }
    }
}
