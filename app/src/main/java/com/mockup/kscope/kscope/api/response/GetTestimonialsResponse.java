package com.mockup.kscope.kscope.api.response;

import com.mockup.kscope.kscope.entity.ItemTestimonial;
import com.mockup.kscope.kscope.entity.ItemTestimonials;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stafiiyevskyi on 09.12.2015.
 */
public class GetTestimonialsResponse extends Response{

    private CommentsAllRequestParam request_param = new CommentsAllRequestParam();

    public List<ItemTestimonials> getAllTestimonials(){
        return request_param.testimonials;
    }

    public CommentsAllRequestParam getRequestParam() {
        return request_param;
    }

    public static class CommentsAllRequestParam extends InvalidParam {

        private String data_count;
        private String data_limit;
        private String data_offset;
        private ArrayList<ItemTestimonials> testimonials;

        public String getDataCount() {
            return data_count;
        }

        public String getDataLimit() {
            return data_limit;
        }

        public String getDataOffset() {
            return data_offset;
        }

        public ArrayList<ItemTestimonials> getComments(){
            return testimonials;
        }
    }
}