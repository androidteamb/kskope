package com.mockup.kscope.kscope.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.mockup.kscope.kscope.utils.PreferencesManager;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Serhii_Slobodianiuk on 30.11.2015.
 */
public class PhotoFragment {
    public static final int PHOTO_TAKEN_ALBUM = 6001;
    public static final int PHOTO_SELECTED_ALBUM = 7001;
    public static final int ADD_NEW_ITEM_PHOTO_TAKEN_ALBUM = 1001;
    public static final int ADD_NEW_ITEM_PHOTO_SELECTED_ALBUM = 1002;

    public static final String JPEG_FILE_PREFIX = "IMG_";
    public static final String JPEG_FILE_SUFFIX = ".jpg";

    public static final String FILE_PREFIX = "file:///";

    private static final String ALBUM_NAME = "KSkope";

    private static final String CAMERA_DIR = "/dcim/";

    public static void dispatchTakePictureIntent(Activity activity, int requestCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                switch (requestCode) {
                    case PHOTO_TAKEN_ALBUM:
                        photoFile = createAlbumImageFile();
                        break;
                    case PHOTO_SELECTED_ALBUM:
                        photoFile = createProfileImageFile();
                        break;
                    case ADD_NEW_ITEM_PHOTO_TAKEN_ALBUM:
                        photoFile = createAlbumImageFile();
                        break;
                    case ADD_NEW_ITEM_PHOTO_SELECTED_ALBUM:
                        photoFile = createProfileImageFile();
                        break;
                    default:
                        break;
                }
//                if (requestCode == PHOTO_TAKEN_ALBUM) {
//                    photoFile = createAlbumImageFile();
//                } else {
//                    photoFile = createProfileImageFile();
//                }
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                Uri uri = Uri.fromFile(photoFile);
                switch (requestCode){
                    case PHOTO_TAKEN_ALBUM:
                        PreferencesManager.getInstance().setPhotoTmp(uri);
                        break;
                    case PHOTO_SELECTED_ALBUM:
                        PreferencesManager.getInstance().setPhotoTmp(uri);
                        break;
                    case ADD_NEW_ITEM_PHOTO_TAKEN_ALBUM:
                        PreferencesManager.getInstance().setPhotoAddNewItem(uri);
                        break;
                    case ADD_NEW_ITEM_PHOTO_SELECTED_ALBUM:
                        PreferencesManager.getInstance().setPhotoAddNewItem(uri);
                        break;
                    default:
                        break;
                }

                Bundle newExtras = new Bundle();
                newExtras.putParcelable(MediaStore.EXTRA_OUTPUT, uri);
                newExtras.putBoolean("return-data", true);
                takePictureIntent.putExtras(newExtras);

                activity.startActivityForResult(takePictureIntent, requestCode);
            }
        }
    }

    public static void dispatchGetGalleryPictureIntent(Activity activity, int requestCode) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(Intent.createChooser(intent, "Select Photo"), requestCode);
    }

    public static File createAlbumImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
        return imageF;
    }

    private static File createProfileImageFile() throws IOException {
        // Create an image file name
        String imageFileName = "/profile";
        File albumF = getAlbumDir();
        //File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
        File imageF = new File(albumF.getAbsolutePath() + imageFileName + JPEG_FILE_SUFFIX);
        imageF.getParentFile().mkdirs();
        imageF.createNewFile();
        return imageF;
    }

    public static File getAlbumStorageDir() {
        return new File(
                Environment.getExternalStorageDirectory()
                        + CAMERA_DIR
                        + ALBUM_NAME
        );
    }

    public static File getAlbumDir() {
        File storageDir = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = getAlbumStorageDir();
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraHelper", "failed to create directory");
                        return null;
                    }
                }
            }
        } else {
            Log.v("CameraHelper", "External storage is not mounted READ/WRITE.");
        }
        return storageDir;
    }

}
