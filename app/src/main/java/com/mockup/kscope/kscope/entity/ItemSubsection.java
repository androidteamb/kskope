package com.mockup.kscope.kscope.entity;

/**
 * Created by Stafiiyevskyi on 06.11.2015.
 */
public class ItemSubsection {
    private String address;
    private String description;
    private String guid_item;
    private String guid_subsection;
    private String phone;
    private String rating;
    private String time;
    private String title;
    private String comments_count;
    private String first_name;
    private String likes;
    private String likes_count;
    private String views;
    private String views_count;
    private String vote;
    private String votes_count;
    private String image_url;
    private String user_login;
    private String converted_image;
    private String owner;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getConverted_image() {
        return converted_image;
    }

    public void setConverted_image(String converted_image) {
        this.converted_image = converted_image;
    }

    public String getUser_login() {
        return user_login;
    }

    public void setUser_login(String user_login) {
        this.user_login = user_login;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getComments_count() {
        return comments_count;
    }

    public void setComments_count(String comments_count) {
        this.comments_count = comments_count;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(String likes_count) {
        this.likes_count = likes_count;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getViews_count() {
        return views_count;
    }

    public void setViews_count(String views_count) {
        this.views_count = views_count;
    }

    public String getVote() {
        return vote;
    }

    public void setVote(String vote) {
        this.vote = vote;
    }

    public String getVotes_count() {
        return votes_count;
    }

    public void setVotes_count(String votes_count) {
        this.votes_count = votes_count;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGuid_item() {
        return guid_item;
    }

    public void setGuid_item(String guid_item) {
        this.guid_item = guid_item;
    }

    public String getGuid_subsection() {
        return guid_subsection;
    }

    public void setGuid_subsection(String guid_subsection) {
        this.guid_subsection = guid_subsection;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
