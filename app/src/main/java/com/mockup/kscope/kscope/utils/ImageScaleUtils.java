package com.mockup.kscope.kscope.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;

/**
 * Created by Stafiiyevskyi on 10.12.2015.
 */
public class ImageScaleUtils {


    public static String scaleImage(String imagePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap resultBitmap;
        FileInputStream fileInputStream;

        Log.i("Input image path ", imagePath);
        try {
            fileInputStream = new FileInputStream(new File(imagePath));
            resultBitmap = BitmapFactory.decodeStream(fileInputStream, null, options);
//            resultBitmap = Bitmap.createScaledBitmap(resultBitmap, width, height, false);
            Log.e("resolution ", resultBitmap.getHeight() + " " + resultBitmap.getWidth());
            fileInputStream.close();
            if (resultBitmap != null) {
                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                boolean resultBool = resultBitmap.compress(Bitmap.CompressFormat.JPEG, 70, bao);
                if (resultBool) {
                    File photo = new File(imagePath);
                    File copyPhoto;
                    if (photo.exists()) {
//                        boolean photoDel = photo.delete();
                        copyPhoto = new File(photo.getParent(), "copy_" + photo.getName());
                        FileOutputStream fos = new FileOutputStream(copyPhoto.getPath());
                        fos.write(bao.toByteArray());
                        fos.close();
                        return copyPhoto.getPath();
                    }
                    Log.e("New photo path ", photo.getPath());
                    FileOutputStream fos = new FileOutputStream(photo.getPath());
                    fos.write(bao.toByteArray());
                    fos.close();
                    return photo.getPath();
                }

            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
