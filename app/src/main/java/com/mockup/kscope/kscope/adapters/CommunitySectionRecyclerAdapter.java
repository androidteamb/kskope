package com.mockup.kscope.kscope.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.mockup.kscope.kscope.utils.DisplayUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;


import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Stafiiyevskyi on 29.10.2015.
 */
public class CommunitySectionRecyclerAdapter extends RecyclerView.Adapter<CommunitySectionRecyclerAdapter.ViewHolderCommunitySection> {

    private Activity context;
    private List<ItemCommunities> data = new ArrayList<>();
    private OnItemClickListener itemClickListener;
    private DisplayImageOptions options;

    public CommunitySectionRecyclerAdapter(Activity context, OnItemClickListener recyclerView) {
        this.context = context;
        this.itemClickListener = recyclerView;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.placeholder)
                .showImageForEmptyUri(R.drawable.placeholder)
                .showImageOnFail(R.drawable.placeholder)
                .cacheInMemory(true)
                .considerExifParams(false)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }


    @Override
    public ViewHolderCommunitySection onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_community_list, parent, false);

        ViewHolderCommunitySection recyclerViewHolder = new ViewHolderCommunitySection(view);
        return recyclerViewHolder;

    }


    @Override
    public void onBindViewHolder(final ViewHolderCommunitySection holder, final int position) {

//        String url = "/pub/pictures/castle.jpg";
//        if (data.get(position).getLogo()==null){
//            if (position%2==0){
//                url = "/pub/pictures/030007b.jpg";
//            }
//            if (position%3==0){
//                url = "/pub/pictures/castle.jpg";
//            }
//            if (position%4==0){
//                url = "/pub/pictures/coffepot.jpg";
//            }
//            if (position%5==0){
//                url = "/pub/pictures/fitness.jpeg";
//            }
//        } else {
//            url = data.get(position).getLogo();
//        }

        int iconRes;
        if (data.get(position).getName().equalsIgnoreCase("Education & Training")){
            iconRes = R.drawable.img_cm_education;
        }else if(data.get(position).getName().equalsIgnoreCase("Professional Organizations")){
            iconRes = R.drawable.img_cm_professional;
        }else if(data.get(position).getName().equalsIgnoreCase("Family Services")){
            iconRes = R.drawable.img_cm_family;
        } else if(data.get(position).getName().equalsIgnoreCase("Business Services")){
            iconRes = R.drawable.img_cm_business;
        }else  if (data.get(position).getName().equalsIgnoreCase("Social Organizations")){
            iconRes = R.drawable.img_cm_social;
        } else if (data.get(position).getName().equalsIgnoreCase("State & Local Organizations")){
            iconRes = R.drawable.img_cm_local;
        } else if(data.get(position).getName().equalsIgnoreCase("Housing Services")){
            iconRes = R.drawable.img_cm_house;
        } else if(data.get(position).getName().equalsIgnoreCase("Arts & Culture")){
            iconRes = R.drawable.img_cm_art;
        }else if (data.get(position).getName().equalsIgnoreCase("Entertainment")){
            iconRes = R.drawable.img_cm_entertainment;
        } else if (data.get(position).getName().equalsIgnoreCase("Dining")){
            iconRes = R.drawable.img_cm_dining;
        } else {
            iconRes = R.drawable.img_cm_transport;
        }

        holder.nameCommunity.setText(data.get(position).getName());
        holder.bannerCommunity.setImageResource(iconRes);
//        ImageLoader.getInstance().displayImage(url, holder.bannerCommunity,options);

    }

    @Override
    public int getItemCount() {
        if (data != null) {
            return data.size();
        } else return 0;


    }

    public ItemCommunities getCommunityByPosition(int position){
        return data.get(position);
    }

    public void setData(List<ItemCommunities> data) {
        this.data = data;
    }

    public void clearData(){
        if (data!=null){
            data.clear();
        }

    }

    public void addData(List<ItemCommunities> data){
        this.data.addAll(data);

    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.itemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view , int position);
    }


    public class ViewHolderCommunitySection extends RecyclerView.ViewHolder implements View.OnClickListener{
        @InjectView(R.id.iv_community_logo)
        public ImageView bannerCommunity;
        @InjectView(R.id.tv_community_name)
        public TextView nameCommunity;

        public ViewHolderCommunitySection(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.inject(this, itemView);
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }
}
