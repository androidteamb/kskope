package com.mockup.kscope.kscope.fragments.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.RecyclerCalendarEventAdapter;
import com.mockup.kscope.kscope.adapters.RecyclerCalendarEventAdapter.EventCalendarAdapterCallback;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.GetEventItemCalendarRequest;
import com.mockup.kscope.kscope.api.response.GetEventItemCalendarResponse;
import com.mockup.kscope.kscope.customView.CalendarItemDecoration;
import com.mockup.kscope.kscope.customView.CustomCalendarView;
import com.mockup.kscope.kscope.customView.TypefaceTextView;
import com.mockup.kscope.kscope.entity.ItemEvent;
import com.mockup.kscope.kscope.events.NewItemAddedEvent;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.presenters.FragmentCalendarPresenterImpl;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.utils.TimeUtil;
import com.mockup.kscope.kscope.viewsPresenter.FragmentCalendarView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

import static android.view.View.VISIBLE;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class FragmentCalendar extends BaseFragment implements CustomCalendarView.ControlHandler, CustomCalendarView.EventHandler, EventCalendarAdapterCallback, FragmentCalendarView {

    @InjectView(R.id.recycler_calendar)
    RecyclerView calendarRecycler;
    @InjectView(R.id.calendarview)
    CustomCalendarView calendarView;
    @InjectView(R.id.empty_text)
    TypefaceTextView emptyText;
    @InjectView(R.id.progress)
    FrameLayout progressBar;
    private RecyclerCalendarEventAdapter recyclerCalendarEventAdapter;
    private String currentDatePressed = "";
    private FragmentCalendarPresenterImpl presenter;
    LinearLayout addEvent;


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents();
        initAdapter();
        initRecycler();
        setDataToAdapterRecycler(null);
        setupToolbarMenu();
        progressBar.setVisibility(View.VISIBLE);
        currentDatePressed = TimeUtil.getDateFromMilliseconds(new Date().getTime(), null);
        presenter.getCalendarEventPresenter(TimeUtil.getDateFromMilliseconds(new Date().getTime(), null));
    }


    @Override
    public void onPause() {
        super.onPause();
        addEvent.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(NewItemAddedEvent event){
        progressBar.setVisibility(VISIBLE);
        presenter.getCalendarEventPresenter(currentDatePressed);
    }

    private void setupToolbarMenu() {
        Toolbar toolbar = ButterKnife.findById((MainActivity)getActivity(), R.id.toolbar);
        addEvent = ButterKnife.findById(toolbar, R.id.menu_add_item_event);
        addEvent.setVisibility(VISIBLE);
        addEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SslRequestHandler.getInstance().cancelAllAsyncRequest();
                Fragment fragment = new FragmentAddNewItem();
                ((MainActivity) getActivity()).addFragment(fragment, getActivity().getResources().getString(R.string.fragment_add_new_item_title));
            }
        });
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_calendar;
    }

    @Override
    public void updateToolbar() {
        EventBus.getDefault().post(new NewItemAddedEvent());
        ((MainActivity) getActivity()).setActionBarTitle("Calendar of events", true);
        setupToolbarMenu();
    }

    private void initComponents() {
        calendarView.setControlHandler(this);
        calendarView.setEventHandler(this);
        presenter = new FragmentCalendarPresenterImpl(this);
    }

    private void initAdapter() {
        recyclerCalendarEventAdapter = new RecyclerCalendarEventAdapter(getActivity(), calendarRecycler);
        recyclerCalendarEventAdapter.setListener(this);
    }

    private void initRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        calendarRecycler.addItemDecoration(new CalendarItemDecoration(getActivity()));
        calendarRecycler.setLayoutManager(layoutManager);
        calendarRecycler.setAdapter(recyclerCalendarEventAdapter);
    }


    private void setDataToAdapterRecycler(List<ItemEvent> eventsData) {

        recyclerCalendarEventAdapter.clearData();
        if (eventsData != null) {
            recyclerCalendarEventAdapter.addData(eventsData);
        }
        recyclerCalendarEventAdapter.notifyDataSetChanged();


    }

    @Override
    public void nextMonthPressed() {

    }

    @Override
    public void previousMonthPressed() {

    }


    @Override
    public void onDayPress(long date) {
        Log.e("Day press", TimeUtil.getDateFromMilliseconds(date, null));
        currentDatePressed = TimeUtil.getDateFromMilliseconds(date, null);
        progressBar.setVisibility(View.VISIBLE);
        presenter.cancelLastGetCalendarEvents();
        presenter.getCalendarEventPresenter(currentDatePressed);
    }

    @Override
    public void addEventToCalendar(ItemEvent event, final int position) {
        presenter.addEventToCalendarPresenter(event.getGuid_item(), event.getTime());
        updateEventItem(position, true);

    }

    @Override
    public void deleteEventFromCalendar(ItemEvent event, final int position) {
        presenter.deleteEventFromCalendarPresenter(event.getGuid_item(), event.getTime());
        updateEventItem(position, false);

    }

    private void updateEventItem(int position, boolean isAddToCalendar) {

        if (!isAddToCalendar) {
            recyclerCalendarEventAdapter.getItemFromAdapterByPosition(position).setAdded("0");
        } else {
            recyclerCalendarEventAdapter.getItemFromAdapterByPosition(position).setAdded("1");
        }
        recyclerCalendarEventAdapter.notifyItemChanged(position);

    }

    @Override
    public void showEvents(List<ItemEvent> events) {
        if (this.isVisible()){
            if (events == null) {
                emptyText.setVisibility(View.VISIBLE);
                setDataToAdapterRecycler(null);
            } else {
                emptyText.setVisibility(View.INVISIBLE);
                setDataToAdapterRecycler(events);
            }

            progressBar.setVisibility(View.GONE);
        }

    }

    @Override
    public void updateEvent() {

    }
}