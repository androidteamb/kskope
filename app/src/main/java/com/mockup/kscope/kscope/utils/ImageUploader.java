package com.mockup.kscope.kscope.utils;

import android.util.Log;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;

/**
 * Created by Stafiiyevskyi on 08.12.2015.
 */
public class ImageUploader {

    public static String FTP_SERVER_HOST = "52.34.106.138";
    public static int FTP_SERVER_PORT = 21;
    public static String FTP_LOGIN = "kscope_user";
    public static String FTP_PASSWORD = "kscope";

    private ImageUploader(){

    }

    private static class Holder {
        private static final ImageUploader INSTANCE = new ImageUploader();
    }

    public static ImageUploader getInstance() {
        return Holder.INSTANCE;
    }

    public boolean uploadImageToFTPServer(String pathImage, String pathImageServer){
        FTPClient con = null;
        boolean isSucceeded = false;
        try
        {
            con = new FTPClient();
            con.connect(FTP_SERVER_HOST);///"speedtest.tele2.net"

            if (con.login(FTP_LOGIN, FTP_PASSWORD))//"anonymous", ""
            {
                con.enterLocalPassiveMode();
                con.setFileType(FTP.BINARY_FILE_TYPE);
                String data = pathImage;

                FileInputStream in = new FileInputStream(new File(data));
                Log.i("From upload imgServ", pathImageServer);
                boolean result = con.storeFile(pathImageServer, in);
                in.close();
                isSucceeded = result;
                if (result){
                    Log.v("upload result", "succeeded");
                } else {
                    Log.v("upload result", "failed");
                }
                con.logout();
                con.disconnect();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return isSucceeded;
    }
}
