package com.mockup.kscope.kscope.api.response;

import com.mockup.kscope.kscope.entity.ItemEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stafiiyevskyi on 25.11.2015.
 */
public class GetEventItemCalendarResponse extends  Response {

    private GetEventItemCalendarRequestParam request_param = new GetEventItemCalendarRequestParam();

    public List<ItemEvent> getEvents(){
        return request_param.calendar;
    }

    public GetEventItemCalendarRequestParam getRequestParam() {
        return request_param;
    }

    public static class GetEventItemCalendarRequestParam extends InvalidParam {

        private String data_count;
        private String data_limit;
        private String data_offset;
        private ArrayList<ItemEvent> calendar;

        public String getDataCount() {
            return data_count;
        }

        public String getDataLimit() {
            return data_limit;
        }

        public String getDataOffset() {
            return data_offset;
        }

        public ArrayList<ItemEvent> getCalendar(){
            return calendar;
        }
    }

}
