package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 05.11.2015.
 */
public class GetSubsectionRequest extends Request{
    private GetSubsection request_param;

    public GetSubsectionRequest(String guidCommunity, String guid_subsection){
        setRequestCmd("get_subsection");
        request_param = new GetSubsection();
        request_param.guid_community = guidCommunity;
        request_param.guid_subsection = guid_subsection;
    }


    private class GetSubsection {
        private String guid_community;
        private String guid_subsection;
    }
}
