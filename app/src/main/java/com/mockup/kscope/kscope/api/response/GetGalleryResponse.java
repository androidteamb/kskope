package com.mockup.kscope.kscope.api.response;

import java.util.ArrayList;

/**
 * Created by Serhii_Slobodianiuk on 03.11.2015.
 */
public class GetGalleryResponse extends Response {

    private GalleryRequestParam request_param = new GalleryRequestParam();

    public GalleryRequestParam getRequestParam() {
        return request_param;
    }

    public static class GalleryRequestParam extends InvalidParam {

        private String data_count;
        private String data_limit;
        private String data_offset;

        private ArrayList<String> image_url;
        private ArrayList<String> converted_image;

        public String getDataCount() {
            return data_count;
        }

        public String getDataLimit() {
            return data_limit;
        }

        public String getDataOffset() {
            return data_offset;
        }

        public ArrayList<String> getImages() {
            return image_url;
        }

        public ArrayList<String> getConvertedImages() {
            return converted_image;
        }
    }
}
