package com.mockup.kscope.kscope.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.customView.TypefaceTextView;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;


import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Stafiiyevskyi on 29.10.2015.
 */
public class CommunityCheckListAdapter extends RecyclerView.Adapter<CommunityCheckListAdapter.ViewHolderCommunitySection> {

    private static final int ITEM_ADDED = 1;
    private static final int ITEM_NOT_ADDED = 0;

    private Activity context;


    private List<ItemCommunities> data = new ArrayList<>();
    private OnItemClickListener itemClickListener;
    private DisplayImageOptions options;

    public CommunityCheckListAdapter(Activity context, OnItemClickListener recyclerView) {
        this.context = context;
        this.itemClickListener = recyclerView;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.placeholder)
                .showImageForEmptyUri(R.drawable.placeholder)
                .showImageOnFail(R.drawable.placeholder)
                .cacheInMemory(true)
                .considerExifParams(false)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }


    @Override
    public ViewHolderCommunitySection onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        if (viewType == ITEM_ADDED) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_community_list_checked, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_community_list_check, parent, false);
        }

        ViewHolderCommunitySection recyclerViewHolder = new ViewHolderCommunitySection(view);
        return recyclerViewHolder;

    }


    @Override
    public void onBindViewHolder(final ViewHolderCommunitySection holder, final int position) {
        // fictive image urls

//        String url = "/pub/pictures/castle.jpg";
//
//        if (data.get(position).getLogo() == null) {
//            if (position % 2 == 0) {
//                url = "/pub/pictures/030007b.jpg";
//            }
//            if (position % 3 == 0) {
//                url = "/pub/pictures/castle.jpg";
//            }
//            if (position % 4 == 0) {
//                url = "/pub/pictures/coffepot.jpg";
//            }
//            if (position % 5 == 0) {
//                url = "/pub/pictures/fitness.jpeg";
//            }
//        } else {
//            url = data.get(position).getLogo();
//        }

        int iconRes = R.drawable.bg_calendar_selected;
        if (data.get(position).getName().equalsIgnoreCase("Education & Training")){
            iconRes = R.drawable.img_cm_education;
        }else if(data.get(position).getName().equalsIgnoreCase("Professional Organizations")){
            iconRes = R.drawable.img_cm_professional;
        }else if(data.get(position).getName().equalsIgnoreCase("Family Services")){
            iconRes = R.drawable.img_cm_family;
        } else if(data.get(position).getName().equalsIgnoreCase("Business Services")){
            iconRes = R.drawable.img_cm_business;
        }else  if (data.get(position).getName().equalsIgnoreCase("Social Organizations")){
            iconRes = R.drawable.img_cm_social;
        } else if (data.get(position).getName().equalsIgnoreCase("State & Local Organizations")){
            iconRes = R.drawable.img_cm_local;
        } else if(data.get(position).getName().equalsIgnoreCase("Housing Services")){
            iconRes = R.drawable.img_cm_house;
        } else if(data.get(position).getName().equalsIgnoreCase("Arts & Culture")){
            iconRes = R.drawable.img_cm_art;
        }else if (data.get(position).getName().equalsIgnoreCase("Entertainment")){
            iconRes = R.drawable.img_cm_entertainment;
        } else if (data.get(position).getName().equalsIgnoreCase("Dining")){
            iconRes = R.drawable.img_cm_dining;
        } else {
            iconRes = R.drawable.img_cm_transport;
        }

        holder.bannerCommunity.setImageResource(iconRes);
        holder.nameCommunity.setText(data.get(position).getName());
//        ImageLoader.getInstance().displayImage(url, holder.bannerCommunity, options);

    }

    @Override
    public int getItemCount() {
        if (data != null) {
            return data.size();
        } else return 0;

    }

    @Override
    public int getItemViewType(int position) {

        if (data.get(position).getAdded() == null) {
            data.get(position).setAdded("0");
        }

        if (data.get(position).getAdded().equalsIgnoreCase("0")) {
            return ITEM_NOT_ADDED;
        } else {
            return ITEM_ADDED;
        }

    }

    public ItemCommunities getCommunityByPosition(int position) {
        return data.get(position);
    }


    public void clearData() {
        if (data != null) {
            data.clear();
        }

    }

    public void addData(List<ItemCommunities> data) {
        this.data.addAll(data);

    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.itemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        public void onItemClick(int position);
    }


    public class ViewHolderCommunitySection extends RecyclerView.ViewHolder implements View.OnClickListener {
        @InjectView(R.id.iv_community_logo)
        public ImageView bannerCommunity;
        @InjectView(R.id.tv_community_name)
        public TypefaceTextView nameCommunity;

        public ViewHolderCommunitySection(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.inject(this, itemView);
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(getAdapterPosition());
            }
        }
    }
}
