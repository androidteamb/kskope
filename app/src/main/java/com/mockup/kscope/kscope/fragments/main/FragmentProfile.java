package com.mockup.kscope.kscope.fragments.main;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.ViewPagerAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.model.User;
import com.mockup.kscope.kscope.api.request.TabsCountRequest;
import com.mockup.kscope.kscope.api.request.UserDataRequest;
import com.mockup.kscope.kscope.api.response.TabsCountResponse;
import com.mockup.kscope.kscope.api.response.UserDataResponse;
import com.mockup.kscope.kscope.constants.TimeConstants;
import com.mockup.kscope.kscope.customView.TypefaceTextView;
import com.mockup.kscope.kscope.events.CommunitiesFilterEvent;
import com.mockup.kscope.kscope.events.FragmentStateEvent;
import com.mockup.kscope.kscope.events.FriendsFilterEvent;
import com.mockup.kscope.kscope.events.HideMenuEvent;
import com.mockup.kscope.kscope.events.LikesFilterEvent;
import com.mockup.kscope.kscope.events.MessagesFilterEvent;
import com.mockup.kscope.kscope.events.RefreshProfileFragmentEvent;
import com.mockup.kscope.kscope.events.ShowProgressEvent;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.fragments.Updatable;
import com.mockup.kscope.kscope.fragments.tabs.FragmentFriends;
import com.mockup.kscope.kscope.fragments.tabs.FragmentGallery;
import com.mockup.kscope.kscope.fragments.tabs.FragmentLikes;
import com.mockup.kscope.kscope.fragments.tabs.FragmentMessages;
import com.mockup.kscope.kscope.fragments.tabs.FragmentTabsCommunities;
import com.mockup.kscope.kscope.fragments.tabs.FragmentTabsCommunities.OnCommunitiesExit;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.GONE;
import static android.view.View.OnClickListener;
import static android.view.View.VISIBLE;
import static com.mockup.kscope.kscope.adapters.ViewPagerAdapter.FragmentLifecycle;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

@SuppressWarnings("deprecation")
public class FragmentProfile extends BaseFragment implements OnCommunitiesExit, Updatable {

    @InjectView(R.id.tabs)
    TabLayout mTabLayout;
    @InjectView(R.id.pager)
    ViewPager mViewPager;
    // Profile header
    @InjectView(R.id.photo)
    CircleImageView photo;

    @InjectView(R.id.username)
    TextView tvUserName;
    @InjectView(R.id.working)
    TextView tvAbout;
    @InjectView(R.id.city)
    TextView tvAddress;
    @InjectView(R.id.progress_bar)
    ProgressBar progressBar;

    private DisplayImageOptions options;

    private int currentPosition = 0;

    private Context mContext;
    private Resources mResources;

    public static SearchView searchView;
    private LinearLayout menuEditProfile;
    private LinearLayout menuTabs;
    private ImageView menuTabIcon;

    private PreferencesManager preferenceManager = PreferencesManager.getInstance();

    private UserDataResponse userData;
    private TabsCountResponse counts;
    private TypefaceTextView toolbarTitle;
    private LinearLayout sendMessage;

    private String imageUrl;
    private String firstName;
    private String lastName;

    private boolean[] fragmentsState = new boolean[5];

    //updating
    private boolean isRunning;
    private final Handler mHandler = new Handler();

    public static Fragment newInstance() {
        FragmentProfile fragmentProfile = new FragmentProfile();
        Bundle args = new Bundle();
        fragmentProfile.setArguments(args);
        return fragmentProfile;
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        showMenuItems();
        ((MainActivity) getActivity()).setActionBarTitle(R.string.my_profile, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mResources = mContext.getResources();
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .considerExifParams(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        showPhotoProfile();

        setupViewPager(mViewPager);
        if (mViewPager != null) {
            mTabLayout.setupWithViewPager(mViewPager);
            setupCustomTab(mResources.getDrawable(R.drawable.tab_friends_selector),
                    "", mResources.getString(R.string.my_friends), 0);
            setupCustomTab(mResources.getDrawable(R.drawable.tab_messages_selector),
                    "", mResources.getString(R.string.my_messages), 1);
            setupCustomTab(mResources.getDrawable(R.drawable.tab_gallery_selector),
                    "", mResources.getString(R.string.my_gallery), 2);
            setupCustomTab(mResources.getDrawable(R.drawable.tab_likes_selector),
                    "", mResources.getString(R.string.my_likes), 3);
            setupCustomTab(mResources.getDrawable(R.drawable.tab_communities_selector),
                    "", mResources.getString(R.string.my_communities), 4);
        }

        if (preferenceManager.getUsername() != null) {
            String userName = preferenceManager.getUsername();
            String about = preferenceManager.getPersonalInfo();
            String address = preferenceManager.getAddress();
            setUserInfo(userName, about, address);
        }

        sendUpdateRequest();
        setupToolbarMenu();
        updateToolbar();
        isRunning = true;
        mHandler.postDelayed(updateRunnable, TimeConstants.MINUTE);
    }

    private void updateTabCount() {
        if (counts != null) {
            TextView friends_count = (TextView) mTabLayout.getTabAt(0).getCustomView().findViewById(R.id.count_tab);
            friends_count.setText(counts.getRequestParam().getCountFriends());
            TextView messages_count = (TextView) mTabLayout.getTabAt(1).getCustomView().findViewById(R.id.count_tab);
            messages_count.setText(counts.getRequestParam().getCountMessages());
            TextView gallery_count = (TextView) mTabLayout.getTabAt(2).getCustomView().findViewById(R.id.count_tab);
            gallery_count.setText(counts.getRequestParam().getCountGallery());
            TextView likes_count = (TextView) mTabLayout.getTabAt(3).getCustomView().findViewById(R.id.count_tab);
            likes_count.setText(counts.getRequestParam().getCountLikes());
            TextView community_count = (TextView) mTabLayout.getTabAt(4).getCustomView().findViewById(R.id.count_tab);
            community_count.setText(counts.getRequestParam().getCountCommunities());
        }
    }

    private void setUserInfo(String userName, String about, String address) {
        tvUserName.setText(userName);
        tvAbout.setText(about);
        tvAddress.setText(address);
    }

    private void sendUpdateRequest() {
        if (!preferenceManager.getPhotoTmp().equals("")) {
            photo.setVisibility(View.INVISIBLE);
            showPhotoProfile();
        }
        if (preferenceManager.getUsername() == null) {
            UserDataRequest userDataRequest = new UserDataRequest(preferenceManager.getSessionId(), null);
            SslRequestHandler.getInstance().sendRequest(userDataRequest, UserDataResponse.class, profileRequestListener);
        }
        TabsCountRequest tabsCountRequest = new TabsCountRequest(preferenceManager.getSessionId(), null);
        SslRequestHandler.getInstance().sendRequest(tabsCountRequest, TabsCountResponse.class, countTabsListener);
    }

    private void setupToolbarMenu() {
        Toolbar toolbar = ButterKnife.findById(getActivity(), R.id.toolbar);
        toolbarTitle = ButterKnife.findById(toolbar, R.id.toolbarText);

        sendMessage = ButterKnife.findById(toolbar, R.id.menu_messages);

        searchView = ButterKnife.findById(toolbar, R.id.searchView);
        int searchImgId = getResources().getIdentifier("android:id/search_button", null, null);
        final ImageView searchIcon = ButterKnife.findById(searchView, searchImgId);
        searchIcon.setImageResource(R.drawable.ic_ab_search);

        searchView.setOnSearchClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarTitle.setVisibility(GONE);
                if (!fragmentsState[currentPosition]) {
                    if (!searchView.isIconified()) {
                        searchView.setIconified(true);
                    }
                    Toast.makeText(mContext, "Please, wait for loading data...", Toast.LENGTH_SHORT).show();

                } else {
                   // searchView.setIconified(false);
                }
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                toolbarTitle.setVisibility(VISIBLE);
                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                switch (currentPosition) {
                    case 0:
                        EventBus.getDefault().post(new FriendsFilterEvent(newText));
                        break;
                    case 1:
                        EventBus.getDefault().post(new MessagesFilterEvent(newText));
                        break;
                    case 3:
                        EventBus.getDefault().post(new LikesFilterEvent(newText));
                        break;
                    case 4:
                        EventBus.getDefault().post(new CommunitiesFilterEvent(newText));
                        break;
                }
                return true;
            }
        });

        showMenuItem(searchView);

        menuEditProfile = ButterKnife.findById(toolbar, R.id.menu_edit_profile);

        showMenuItem(menuEditProfile);

        menuEditProfile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = FragmentEditProfile.newInstance();
                if (fragment != null) {
                    hideMenuItems();
                    ((MainActivity) getActivity()).addFragment(fragment, mResources.getString(R.string.edit_profile_title));
                }
            }
        });

        menuTabs = ButterKnife.findById(toolbar, R.id.menu_tab);
        showMenuItem(menuTabs);
        updateToolbar();
    }

    private void setupCustomTab(Drawable icon, String count, String title, final int index) {
        final LinearLayout tabView = (LinearLayout) inflateCustomTabLayout();
        ImageView iconView = ButterKnife.findById(tabView, R.id.icon_tab);
        iconView.setImageDrawable(icon);

        TextView countView = ButterKnife.findById(tabView, R.id.count_tab);
        countView.setText(count);

        TextView titleView = ButterKnife.findById(tabView, R.id.title_tab);
        titleView.setText(title);

        if (index == 0) {
            tabView.setSelected(true);
        }
        mTabLayout.getTabAt(index).setCustomView(tabView);
    }

    private void setupViewPager(ViewPager viewPager) {
        if (viewPager != null) {
            final ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
            adapter.addFragment(new FragmentFriends(), mResources.getString(R.string.my_friends));
            adapter.addFragment(new FragmentMessages(), mResources.getString(R.string.my_messages));
            adapter.addFragment(FragmentGallery.newInstance(""), mResources.getString(R.string.my_gallery));
            adapter.addFragment(FragmentLikes.newInstance(""), mResources.getString(R.string.my_likes));
            adapter.addFragment(FragmentTabsCommunities.newInstance(""), mResources.getString(R.string.my_communities));
            viewPager.setAdapter(adapter);
            viewPager.setOffscreenPageLimit(4);
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    FragmentLifecycle fragmentToHide = (FragmentLifecycle) adapter.getItem(currentPosition);
                    fragmentToHide.onPauseFragment();
                    currentPosition = position;
                    updateToolbar(currentPosition);
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }
    }

    private void showMenuItem(LinearLayout menuView) {
        menuView.setVisibility(VISIBLE);
    }

    private void showMenuItems() {
        searchView.setVisibility(VISIBLE);
        menuEditProfile.setVisibility(VISIBLE);
        menuTabs.setVisibility(VISIBLE);
    }

    private void hideMenuItems() {
        searchView.setVisibility(GONE);
        menuEditProfile.setVisibility(GONE);
        menuTabs.setVisibility(GONE);
    }

    private void showMenuItem(SearchView menuView) {
        menuView.setVisibility(VISIBLE);
    }

    @Override
    public void updateToolbar() {
        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.my_profile), true);
        showMenuItems();
        updateToolbar(currentPosition);
        sendMessage.setVisibility(GONE);
    }

    private void updateToolbar(int position) {
        menuTabIcon = ButterKnife.findById(menuTabs, R.id.menu_tab_icon);
        switch (position) {
            case 0:
                searchView.setVisibility(VISIBLE);
                menuEditProfile.setVisibility(VISIBLE);
                menuTabs.setVisibility(VISIBLE);
                menuTabIcon.setImageDrawable(mResources.getDrawable(R.drawable.ic_add_friends));
                menuTabs.setOnClickListener(initOnMenuTabClickListeners(0));
                break;
            case 1:
                menuEditProfile.setVisibility(GONE);
                searchView.setVisibility(VISIBLE);
                menuTabs.setVisibility(GONE);
                break;
            case 2:
                menuEditProfile.setVisibility(GONE);
                searchView.setVisibility(GONE);
                menuTabs.setVisibility(GONE);
                break;
            case 3:
                menuEditProfile.setVisibility(GONE);
                searchView.setVisibility(VISIBLE);
                menuTabs.setVisibility(GONE);
                break;
            case 4:
                menuEditProfile.setVisibility(GONE);
                searchView.setVisibility(VISIBLE);
                menuTabs.setVisibility(GONE);
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        searchView.setQuery(null, false);
        searchView.setIconified(true);
        hideMenuItems();
        //mTabLayout.getTabAt(mViewPager.getCurrentItem()).getCustomView().setSelected(false);
        toolbarTitle.setVisibility(VISIBLE);
        Log.e("Profile fragment", "pause");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        stopUpdating();
        super.onDestroy();
    }

    public void onEvent(HideMenuEvent event) {
        hideMenuItems();
    }

    public void onEvent(RefreshProfileFragmentEvent event) {
        sendUpdateRequest();
    }

    public void onEvent(ShowProgressEvent event) {
        progressBar.setVisibility(VISIBLE);
    }

    public void onEvent(FragmentStateEvent event) {
        fragmentsState[event.getPosition()] = event.getFragmentsState()[event.getPosition()];

        for (int i =0; i < fragmentsState.length; i++){
            Log.e("loading ", i + " " + fragmentsState[i]);
        }
    }

    private void showPhotoProfile() {
        if (!TextUtils.isEmpty(preferenceManager.getPhotoUrl())) {
            Log.e("From uri ", preferenceManager.getPhotoUrl());
//            Picasso.with(mContext)
//                    .load(preferenceManager.getPhotoUrl())
//                    .memoryPolicy(MemoryPolicy.NO_STORE)
//                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                    .resize(86, 86)
//                    .placeholder(R.drawable.ic_photo_placeholder)
//                    .into(photo);
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .cacheInMemory(false)
                    .cacheOnDisk(false)
                    .build();
            ImageLoader.getInstance().displayImage(preferenceManager.getPhotoUrl(),photo, options);
        } else {
            photo.setImageResource(R.drawable.ic_photo_placeholder);
        }
        photo.setVisibility(VISIBLE);
    }

    private View inflateCustomTabLayout() {
        return LayoutInflater.from(mContext).inflate(R.layout.layout_custom_tab, null);
    }

    @Override
    public void onCommunityExit() {
        TabsCountRequest tabsCountRequest = new TabsCountRequest(preferenceManager.getSessionId(), null);
        SslRequestHandler.getInstance().sendRequest(tabsCountRequest, TabsCountResponse.class, countTabsListener);
    }

    public OnClickListener initOnMenuTabClickListeners(int menuTabId) {

        OnClickListener onClickListener = null;

        switch (menuTabId) {
            case 0:
                onClickListener = new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((MainActivity) getActivity()).addFragment(FragmentUsers.newInstance(), "Users");
                    }
                };
                return onClickListener;
            case 1:
                onClickListener = new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("Tag", "msg");
                    }
                };
                return onClickListener;
            case 2:
                onClickListener = new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("Tag", "gallery");
                    }
                };
                return onClickListener;
            case 3:
                onClickListener = new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("Tag", "likes");
                    }
                };
                return onClickListener;
            case 4:
                onClickListener = new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("Tag", "comm");
                    }
                };
                return onClickListener;
        }
        return onClickListener;
    }

    private void stopUpdating() {
        isRunning = false;
        mHandler.removeCallbacksAndMessages(null);
    }

    private Runnable updateRunnable = new Runnable() {
        @Override
        public void run() {
            if (isRunning) {
                update();
                for (int i = 0; i < mViewPager.getAdapter().getCount(); i++) {
                    Fragment f = ((ViewPagerAdapter) mViewPager.getAdapter()).getItem(i);
                    if (f instanceof Updatable) {
                        ((Updatable) f).update();
                    }
                }
                mHandler.postDelayed(updateRunnable, TimeConstants.MINUTE);
            }
        }
    };

    private SslRequestHandler.SslRequestListener<TabsCountResponse> countTabsListener = new SslRequestHandler.SslRequestListener<TabsCountResponse>() {

        @Override
        public void onStart() {

        }

        @Override
        public void onSuccess(TabsCountResponse response) {
            counts = response;
        }

        @Override
        public void onFailure(String errorMsg) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFinish() {
            if (!isVisible()) {
                return;
            }
            showPhotoProfile();

            progressBar.setVisibility(GONE);
            updateTabCount();
        }
    };
    private SslRequestHandler.SslRequestListener<UserDataResponse> profileRequestListener = new SslRequestHandler.SslRequestListener<UserDataResponse>() {

        @Override
        public void onStart() {

        }

        @Override
        public void onSuccess(UserDataResponse response) {
            userData = response;
        }

        @Override
        public void onFailure(String errorMsg) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFinish() {
            if (userData == null) {
                sendUpdateRequest();
            }
            User user = userData.getUserData().get(0);
            if (user != null) {
                if (TextUtils.isEmpty(user.getFirst_name())) {
                    tvUserName.setText(user.getUser_login());
                } else {
                    firstName = user.getFirst_name();
                    lastName = user.getSecond_name();

                    String userName = firstName + " " + lastName;
                    String about = user.getUser_profile();
                    String email = user.getUser_email();
                    String address = user.getStreet_address1();

                    imageUrl = user.getUser_image();
                    preferenceManager.setPhotoUrl(imageUrl);
                    setUserInfo(userName, about, address);
                    showPhotoProfile();

                    preferenceManager.setUsername(userName);
                    preferenceManager.setFirstName(firstName);
                    preferenceManager.setLastName(lastName);
                    preferenceManager.setAddress(about);
                    preferenceManager.setUserEmail(email);
                    preferenceManager.setPersonalInfo(address);
                }
            }
        }
    };

    @Override
    public void update() {
        sendUpdateRequest();
    }
}