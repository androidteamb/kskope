package com.mockup.kscope.kscope.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mockup.kscope.kscope.database.models.DbFriend;

import java.util.List;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "kscope.db";
    private static final int VERSION = 15;

    private static DatabaseHelper mInstance = null;

    static {
        cupboard().register(DbFriend.class);
    }

    public static DatabaseHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return mInstance;
    }

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        cupboard().withDatabase(db).createTables();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        cupboard().withDatabase(db).upgradeTables();
    }

    public List<DbFriend> getFriends(String owner) {
        return cupboard().withDatabase(getReadableDatabase()).query(DbFriend.class)
                .withSelection("owner = ?", owner)
                .list();
    }

    public void addFriend(DbFriend friend) {
        cupboard().withDatabase(getWritableDatabase()).put(friend);
    }

    public void deleteFriend(DbFriend friend) {
        if (friend != null) {
            cupboard().withDatabase(getReadableDatabase()).delete(DbFriend.class);
        }
    }

    public void clearFriends(String owner) {
        cupboard().withDatabase(getReadableDatabase()).delete(DbFriend.class, "owner = ?", owner);
    }

    public void updateFriends(List<DbFriend> friends, String owner) {
        clearFriends(owner);
        cupboard().withDatabase(getWritableDatabase()).put(friends);
    }


}
