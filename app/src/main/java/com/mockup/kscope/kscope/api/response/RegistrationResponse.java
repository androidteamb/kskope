package com.mockup.kscope.kscope.api.response;

/**
 * Created by Alexander Rubanskiy on 30.10.2015.
 */

public class RegistrationResponse extends Response {

    private RegistrationRequestParam request_param = new RegistrationRequestParam();

    public String getIdUser() {
        return request_param.id_user;
    }

    public String getIdUserRole() {
        return request_param.id_user_role;
    }

    public String getUserLogin() {
        return request_param.user_login;
    }

    public RegistrationRequestParam getRequestParam() {
        return request_param;
    }

    public static class RegistrationRequestParam extends InvalidParam {

        private String id_user;
        private String id_user_role;
        private String user_login;

        public String getIdUser() {
            return id_user;
        }

        public String getIdUserRole() {
            return id_user_role;
        }

        public String getUserLogin() {
            return user_login;
        }
    }

}