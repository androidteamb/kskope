package com.mockup.kscope.kscope.database.models;

import com.mockup.kscope.kscope.entity.ItemFriend;

/**
 * Created by mpodolsky on 04.12.2015.
 */
public class DbFriend {

    public Long _id;
    public ItemFriend friend;
    public String owner;

    public DbFriend(ItemFriend friend, String owner) {
        this.friend = friend;
        this.owner = owner;
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public ItemFriend getFriend() {
        return friend;
    }

    public void setFriend(ItemFriend friend) {
        this.friend = friend;
    }
}
