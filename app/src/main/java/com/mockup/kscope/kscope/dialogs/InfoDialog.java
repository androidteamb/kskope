package com.mockup.kscope.kscope.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.mockup.kscope.kscope.R;

public class InfoDialog extends DialogFragment {

    private static final String ARG_TITLE = "ARG_TITLE";
    private static final String ARG_MESSAGE = "ARG_MESSAGE";

    private DialogCallback callback;

    public InfoDialog() {
    }

    public static InfoDialog newInstance(int title, String message) {
        InfoDialog dialog = new InfoDialog();
        Bundle args = new Bundle();
        args.putInt(ARG_TITLE, title);
        args.putString(ARG_MESSAGE, message);
        dialog.setArguments(args);
        return dialog;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        int title = args.getInt(ARG_TITLE);
        String message = args.getString(ARG_MESSAGE);
        int confirm = R.string.dialog_confirm;


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setNeutralButton(confirm,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (callback != null) {
                            callback.confirm();
                        }
                        dismiss();
                    }
                });
        return builder.create();
    }

    public DialogCallback getCallback() {
        return callback;
    }

    public void setCallback(DialogCallback callback) {
        this.callback = callback;
    }
}
