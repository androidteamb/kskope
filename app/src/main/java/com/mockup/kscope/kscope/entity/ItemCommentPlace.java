package com.mockup.kscope.kscope.entity;

/**
 * Created by Stafiiyevskyi on 04.11.2015.
 */
public class ItemCommentPlace {

    private String first_name;
    private String second_name;
    private String comments;
    private String time_added;
    private String guid_comment;
    private String id_user;
    private String user_login;
    private String user_image;

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_login() {
        return user_login;
    }

    public void setUser_login(String user_login) {
        this.user_login = user_login;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getGuid_comment() {
        return guid_comment;
    }

    public void setGuid_comment(String guid_comment) {
        this.guid_comment = guid_comment;
    }

    public String getUsername() {
        return first_name;
    }

    public void setUsername(String username) {
        this.first_name = username;
    }

    public String getComment() {
        return comments;
    }

    public void setComment(String comment) {
        this.comments = comment;
    }

    public String getTime() {
        return time_added;
    }

    public void setTime(String time) {
        this.time_added = time;
    }


    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSecond_name() {
        return second_name;
    }

    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }
}
