package com.mockup.kscope.kscope.api.response;

/**
 * Created by Stafiiyevskyi on 23.11.2015.
 */
public class GetLikeItemResponse extends Response{

    private GetLikeItemRequestParam request_param = new GetLikeItemRequestParam();

    public GetLikeItemRequestParam getRequestParam() {
        return request_param;
    }
    public String getLikesCount(){
        return request_param.getDataCount();
    }

    public static class GetLikeItemRequestParam extends InvalidParam {

        private String data_count;
        private String data_limit;
        private String data_offset;

        public String getDataCount() {
            return data_count;
        }

        public String getDataLimit() {
            return data_limit;
        }

        public String getDataOffset() {
            return data_offset;
        }

    }
}
