package com.mockup.kscope.kscope.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.fragments.signup.SignupFirstFragment;
import com.mockup.kscope.kscope.fragments.signup.SignupFourthFragment;
import com.mockup.kscope.kscope.fragments.signup.SignupLastFragment;
import com.mockup.kscope.kscope.fragments.signup.SignupSecondFragment;
import com.mockup.kscope.kscope.fragments.signup.SignupThirdFragment;

public class SignupContainerActivity extends BaseActivity {

    public static final int FIRST_FRAGMENT_ID = 1;
    public static final int SECOND_FRAGMENT_ID = 2;
    public static final int THIRD_FRAGMENT_ID = 3;
    public static final int FOURTH_FRAGMENT_ID = 4;
    public static final int LAST_FRAGMENT_ID = 5;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(FIRST_FRAGMENT_ID);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_signup_container;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 2) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    public void setFragment(int fragmentID){
        Fragment fragment = null;
        switch (fragmentID) {
            case FIRST_FRAGMENT_ID:
                fragment = SignupFirstFragment.newInstance();
                break;
            case SECOND_FRAGMENT_ID:
                fragment = SignupSecondFragment.newInstance();
                break;
            case THIRD_FRAGMENT_ID:
                fragment = SignupThirdFragment.newInstance();
                break;
            case FOURTH_FRAGMENT_ID:
                fragment = SignupFourthFragment.newInstance();
                break;
            case LAST_FRAGMENT_ID:
                fragment = SignupLastFragment.newInstance();
                break;
            default:
                fragment = SignupFirstFragment.newInstance();
                break;
        }
        if (fragment != null) {
            if (fragmentID == FIRST_FRAGMENT_ID){
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragment_container, fragment, null)
                        .commit();
            }else {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, fragment, null)
                        .addToBackStack(String.valueOf(fragmentID))
                        .commit();
            }

        }
    }

    public void setTitle(String title){
        setActionBarTitle(title, true);
    }

    public void saveAndClose(){
        SignInActivity.launch(this);
    }

    public void saveAndLogin(){
        MainActivity.launch(this);
    }
}
