package com.mockup.kscope.kscope.adapters;

/**
 * Created by Stafiiyevskyi on 06.11.2015.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.entity.ItemSubsection;

import java.util.List;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Stafiiyevskyi on 30.10.2015.
 */
public class SubSectionItemsAdapter extends ArrayAdapter<ItemSubsection> {

    private final static int RESOURCE = R.layout.item_community_list;

    private LayoutInflater inflater;
    private List<ItemSubsection> items;
    private Context context;


    public SubSectionItemsAdapter(Context context, List<ItemSubsection> items) {
        super(context, RESOURCE, items);
        this.context = context;
        this.items = items;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (items != null) {
            return items.size();
        } else return 0;

    }

    @Override
    public ItemSubsection getItem(int position) {
        return items.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        final ViewHolder holder;
        View item = convertView;

        if (item == null) {

            item = inflater.inflate(RESOURCE, parent, false);
            holder = new ViewHolder();
            holder.tvName = (TextView) item.findViewById(R.id.tv_community_name);
            holder.logoItem = (CircleImageView) item.findViewById(R.id.iv_community_logo);
            item.setTag(holder);
        } else {
            holder = (ViewHolder) item.getTag();
        }


        final ItemSubsection menuItem = getItem(position);

        holder.tvName.setText(menuItem.getTitle());
        String urlImage = menuItem.getConverted_image();


        if (urlImage != null) {
            if (!urlImage.isEmpty()) {
                if (urlImage.startsWith("http://") || urlImage.startsWith("https://")) {
                    Picasso.with(context).load(urlImage).placeholder(R.drawable.bg_calendar_selected).into(holder.logoItem);

                } else if (menuItem.getConverted_image().startsWith("/")) {
                    Picasso.with(context).load(SslRequestHandler.IMAGE_URL_PREFIX + urlImage).placeholder(R.drawable.bg_calendar_selected).into(holder.logoItem);

                } else {
                    Picasso.with(context).load(R.drawable.bg_calendar_selected).into(holder.logoItem);
                }
            } else {
                Picasso.with(context).load(R.drawable.bg_calendar_selected).into(holder.logoItem);
            }

        } else {
            if (!menuItem.getImage_url().isEmpty()) {
                urlImage = menuItem.getImage_url();
                if (urlImage.startsWith("http://") || urlImage.startsWith("https://")) {
                    Picasso.with(context).load(urlImage).placeholder(R.drawable.bg_calendar_selected).into(holder.logoItem);

                } else if (menuItem.getImage_url().startsWith("/")) {
                    Picasso.with(context).load(SslRequestHandler.IMAGE_URL_PREFIX + urlImage).placeholder(R.drawable.bg_calendar_selected).into(holder.logoItem);

                } else {
                    Picasso.with(context).load(R.drawable.bg_calendar_selected).into(holder.logoItem);
                }
            } else {
                Picasso.with(context).load(R.drawable.bg_calendar_selected).into(holder.logoItem);
            }
        }

        return item;
    }

    static class ViewHolder {
        private TextView tvName;
        private CircleImageView logoItem;
    }

}