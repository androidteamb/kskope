package com.mockup.kscope.kscope.utils;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by Serhii_Slobodianiuk on 12.11.2015.
 */
public class Utils {
    public Utils() {
    }

    public static boolean isNetworkConnected(Context mContext) {
        if (mContext != null) {
            ConnectivityManager manager = (ConnectivityManager) mContext
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (manager.getActiveNetworkInfo() != null) {
                return (manager.getActiveNetworkInfo().isAvailable() && manager
                        .getActiveNetworkInfo().isConnected());
            }
        }
        return false;
    }
}
