package com.mockup.kscope.kscope.fragments.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.TabsCommunitiesAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.ExitFromCommunities;
import com.mockup.kscope.kscope.api.request.GetUserCommunityRequest;
import com.mockup.kscope.kscope.api.response.GetCommunityResponse;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.dialogs.ConfirmDialog;
import com.mockup.kscope.kscope.dialogs.DialogCallback;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.mockup.kscope.kscope.entity.ItemSubCommunities;
import com.mockup.kscope.kscope.events.CommunitiesFilterEvent;
import com.mockup.kscope.kscope.events.FragmentStateEvent;
import com.mockup.kscope.kscope.events.HideMenuEvent;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.fragments.Updatable;
import com.mockup.kscope.kscope.fragments.communitySections.FragmentCommunitySubSectionItems;
import com.mockup.kscope.kscope.fragments.main.FragmentProfile;
import com.mockup.kscope.kscope.utils.PreferencesManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;

import static com.mockup.kscope.kscope.adapters.ViewPagerAdapter.FragmentLifecycle;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
public class FragmentTabsCommunities extends BaseFragment implements FragmentLifecycle, Updatable {
    private static final String USER_ID = "user_id";

    @InjectView(R.id.list_communities)
    ExpandableListView lvCommunities;
    @InjectView(R.id.empty)
    LinearLayout emptyView;
    @InjectView(R.id.progress_bar)
    ProgressBar progressBar;

    private List<ItemCommunities> communities;
    private SslRequestHandler.SslRequestListener requestCommunitiesListener;
    private SslRequestHandler.SslRequestListener requestSubCommListener;
    private SslRequestHandler.SslRequestListener requestExitFromCommunities;
    private TabsCommunitiesAdapter adapter;
    private int expandedPosition;
    private boolean expanded = false;
    private PreferencesManager preferencesManager = PreferencesManager.getInstance();

    private EventBus eventBus = EventBus.getDefault();

    private String userId;

    public static Fragment newInstance(String userId) {
        FragmentTabsCommunities fragment = new FragmentTabsCommunities();
        Bundle args = new Bundle();
        args.putString(USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emptyView.setVisibility(View.GONE);
        userId = getArguments().getString(USER_ID);
        if (userId.equals("")) {
            userId = preferencesManager.getUserId();
        }
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }
        initRequestListeners();
        adapter = new TabsCommunitiesAdapter(getActivity(), new ArrayList<ItemCommunities>());

        adapter.setCallback(new TabsCommunitiesAdapter.OnItemClick() {
            @Override
            public void onSubSectionItemClick(ItemSubCommunities communities) {
                eventBus.post(new HideMenuEvent());
                Fragment fragment = FragmentCommunitySubSectionItems.newInstance(communities.getTitle(), communities.getGuidSubsection());
                ((MainActivity) getActivity()).addFragment(fragment, communities.getTitle());
            }
        });

        lvCommunities.setAdapter(adapter);
        lvCommunities.setTextFilterEnabled(false);
        lvCommunities.setGroupIndicator(null);
//        lvCommunities.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//            @Override
//            public boolean onGroupClick(ExpandableListView parent, View v, final int groupPosition, long id) {
//                if (expandedPosition != groupPosition) {
//                    expanded = false;
//                }
//                if (!expanded) {
//                    expanded = true;
//                    expandedPosition = groupPosition;
//                    GetSubsectionRequest request = new GetSubsectionRequest(adapter.getGroups().get(groupPosition).getGuid_community(), null);
//                    SslRequestHandler.getInstance().sendRequest(request, GetSubsectionResponse.class, requestSubCommListener);
//                }
//                return false;
//            }
//        });
        lvCommunities.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (userId == preferencesManager.getUserId()) {
                    final int groupPosition = ExpandableListView.getPackedPositionGroup(id);
                    ConfirmDialog dialog = ConfirmDialog.newInstance(R.string.dialog_exit_from_communities,
                            R.string.dialog_exit_messages,
                            R.string.dialog_confirm,
                            R.string.dialog_cancel);
                    dialog.setCallback(new DialogCallback.EmptyCallback() {
                        @Override
                        public void confirm() {
                            ExitFromCommunities exitFromCommunitiesRequest = new ExitFromCommunities(preferencesManager.getSessionId(), communities.get(groupPosition).getGuid_community());
                            SslRequestHandler.getInstance().sendRequest(exitFromCommunitiesRequest, Response.class, requestExitFromCommunities);
                        }
                    });
                    dialog.show(getFragmentManager(), "dialog");
                }
                return false;
            }
        });
        sendCommunitiesRequest();
    }

    private void initRequestListeners() {
        requestCommunitiesListener = new SslRequestHandler.SslRequestListener<GetCommunityResponse>() {

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(GetCommunityResponse response) {
                communities = response.getCommunity();
            }

            @Override
            public void onFailure(String errorMsg) {
                Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFinish() {
                if (!FragmentTabsCommunities.this.isVisible()) {
                    return;
                }
                if (progressBar != null) {
                    progressBar.setVisibility(View.GONE);
                    if (communities == null) {
                        lvCommunities.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                    } else {
                        lvCommunities.setAdapter(adapter);
                        adapter.setGroupItems(communities);
                        EventBus.getDefault().post(new FragmentStateEvent(4));
                    }
                }
            }
        };

        requestExitFromCommunities = new SslRequestHandler.SslRequestListener<Response>() {

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(Response response) {
            }

            @Override
            public void onFailure(String errorMsg) {
                Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFinish() {
                OnCommunitiesExit fragment = (OnCommunitiesExit) getParentFragment();
                fragment.onCommunityExit();
                sendCommunitiesRequest();
            }
        };
    }

    private void sendCommunitiesRequest() {
        GetUserCommunityRequest getCommunityRequest = new GetUserCommunityRequest(null, userId);
        SslRequestHandler.getInstance().sendRequest(getCommunityRequest, GetCommunityResponse.class, requestCommunitiesListener);
    }

    public void onEvent(CommunitiesFilterEvent event) {
        adapter.getFilter().filter(event.getQueryText());
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_my_communities;
    }

    @Override
    public void updateToolbar() {

    }

    @Override
    public void onPauseFragment() {
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void update() {
        if (FragmentProfile.searchView.isIconified()) {
            sendCommunitiesRequest();
        }
    }

    public interface OnCommunitiesExit {
        void onCommunityExit();
    }
}