package com.mockup.kscope.kscope.fragments.communitySections;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.RecyclerCommentPlaceAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.customView.TypefaceTextView;
import com.mockup.kscope.kscope.dialogs.DialogFragmentSendComment;
import com.mockup.kscope.kscope.dialogs.DialogFragmentRating;
import com.mockup.kscope.kscope.dialogs.DialogSendCommentCallback;
import com.mockup.kscope.kscope.dialogs.DialogRatingCallback;
import com.mockup.kscope.kscope.entity.ItemCommentPlace;
import com.mockup.kscope.kscope.entity.ItemSubsection;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.fragments.main.FragmentFriendProfile;
import com.mockup.kscope.kscope.fragments.main.FragmentProfile;
import com.mockup.kscope.kscope.presenters.FragmentCommunitySectionItemPresenterImpl;
import com.mockup.kscope.kscope.utils.DisplayUtil;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.utils.TimeUtil;
import com.mockup.kscope.kscope.utils.TypefaceManager;
import com.mockup.kscope.kscope.viewsPresenter.FragmentCommunitySectionItemView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Stafiiyevskyi on 30.10.2015.
 */
public class FragmentCommunitySectionItem extends BaseFragment implements RecyclerCommentPlaceAdapter.OnItemClickListener, FragmentCommunitySectionItemView {

    private final int DATA_LIMIT = 30;

    @InjectView(R.id.like_button)
    ImageView likeButton;
    @InjectView(R.id.place_count_likes)
    TypefaceTextView likesCount;
    @InjectView(R.id.place_image)
    ImageView placePhoto;
    @InjectView(R.id.name_place)
    TypefaceTextView namePlace;
    @InjectView(R.id.place_description)
    TypefaceTextView descriptionPlace;
    @InjectView(R.id.date_adding_description)
    TypefaceTextView dateAddingDescription;
    @InjectView(R.id.username)
    TypefaceTextView userNameAdded;
    @InjectView(R.id.average_rating)
    TypefaceTextView ratingText;
    @InjectView(R.id.ratingBar)
    RatingBar ratingBar;
    @InjectView(R.id.place_number_watchers)
    TypefaceTextView countViewers;
    @InjectView(R.id.ratted_count)
    TypefaceTextView countRatted;
    @InjectView(R.id.place_count_comment)
    TypefaceTextView countComments;
    @InjectView(R.id.tv_no_comments)
    TypefaceTextView noDataTextView;
    //    @InjectView(R.id.load_more)
//    Button loadMoreComments;
    @InjectView(R.id.place_comment_recycler)
    RecyclerView commentRecycler;
    @InjectView(R.id.container_first_two_comment)
    LinearLayout containerFirstTwoComments;
    @InjectView(R.id.progress)
    ProgressBar progressBar;
    @InjectView(R.id.progress_all)
    ProgressBar progressBarAll;
    @InjectView(R.id.rl_all_view_container)
    RelativeLayout allViewContainer;
    @InjectView(R.id.add_comment)
    ImageView addComment;

    private boolean isLoadMoreCliked = false;
    private boolean isAddCommentClick = false;

    private boolean isLiked = false;

    private String title;
    private String itemGuid;
    private String currentRating;
    private String userLogin;
    private String owner;
    private RecyclerCommentPlaceAdapter adapter;
    private int currentOffset = 0;

    private DialogFragmentRating dialogFragmentRating;
    private DialogFragmentSendComment dialogSendComment;
    DisplayImageOptions options;


    private FragmentCommunitySectionItemPresenterImpl presenter;

    private final Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            if (FragmentCommunitySectionItem.this.isVisible()) {
                double width = bitmap.getWidth();
                double height = bitmap.getHeight();
                double screenWidth = DisplayUtil.getScreenWidth(getActivity());
                double proportion = screenWidth / width;
                double proportionHeight = height * proportion;
                placePhoto.setLayoutParams(new RelativeLayout.LayoutParams((int) screenWidth, (int) proportionHeight));
                placePhoto.setImageBitmap(bitmap);
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };


    @Override
    public int getLayoutResource() {
        return R.layout.fragment_item_section_place;
    }

    @Override
    public void updateToolbar() {
        Toolbar toolbar = ButterKnife.findById(getActivity(), R.id.toolbar);
        LinearLayout message = ButterKnife.findById(toolbar, R.id.menu_messages);
        message.setVisibility(View.GONE);
        ((MainActivity) getActivity()).setActionBarTitle(title, true);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ratingBar.setOnTouchListener(null);
        presenter = new FragmentCommunitySectionItemPresenterImpl();
        presenter.setView(this);
        setupUIElements();
        presenter.getAllItemsData(itemGuid, null);
        presenter.getComments(itemGuid, String.valueOf(DATA_LIMIT), String.valueOf(currentOffset));
        updateToolbar();

    }

    private void setupUIElements() {
        userNameAdded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (owner!=null){
                    if (owner.equals(PreferencesManager.getInstance().getUserId())) {
                        ((MainActivity)getActivity()).addFragment(FragmentProfile.newInstance(), "My profile");

                    } else if(userLogin!=null){
                        ((MainActivity)getActivity()).addFragment(FragmentFriendProfile.newInstance(userLogin)
                                , userLogin);
                    }
                }
            }
        });

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .considerExifParams(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        allViewContainer.setVisibility(View.INVISIBLE);
        progressBarAll.setVisibility(View.VISIBLE);
        commentRecycler.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                        //Allow ScrollView to intercept touch events once again.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                // Handle RecyclerView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });


        initRecycler(new ArrayList<ItemCommentPlace>());
        title = getArguments().getString(MainActivity.TITLE_KEY);
        itemGuid = getArguments().getString(MainActivity.ITEM_GUID);
        ((MainActivity) getActivity()).setActionBarTitle(title, true);
    }


    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        ((MainActivity) getActivity()).setActionBarTitle(title, true);

    }


    @OnClick(R.id.like_button)
    void onLikeButtonClick() {

        if (isLiked) {
            isLiked = false;
            likeButton.setImageResource(R.drawable.ic_like);
            likesCount.setText("");
        } else {
            isLiked = true;
            likeButton.setImageResource(R.drawable.ic_like_selected);
            likesCount.setText("");
        }
        Date date = new Date();
        long time = date.getTime();
        presenter.cancelLastSendLike();
        presenter.sendLike(itemGuid, null, TimeUtil.getDateFromMilliseconds(time, "yyyy-MM-dd HH:mm:ss"));
    }

    @OnClick(R.id.add_comment)
    void addCommentClick() {
        if (!isAddCommentClick) {
            isAddCommentClick = true;
            dialogSendComment = DialogFragmentSendComment.newInstance();
            dialogSendComment.setCallback(new DialogSendCommentCallback() {
                @Override
                public void confirm(String inputText, final String imagePath) {
                    presenter.sendComment(itemGuid, inputText, imagePath);
                }

                @Override
                public void cancel() {
                    dialogSendComment.dismiss();
                    isAddCommentClick = false;
                }

                @Override
                public void dismiss() {
                    isAddCommentClick = false;
                }
            });

            dialogSendComment.show(getActivity().getFragmentManager(), "dlg");
        }
    }


    private void initRecycler(List<ItemCommentPlace> data) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        commentRecycler.setLayoutManager(layoutManager);
        adapter = new RecyclerCommentPlaceAdapter(getActivity(), data);
        adapter.setOnItemClickListener(this);
        commentRecycler.setAdapter(adapter);
    }


    private void addCommentsData(List<ItemCommentPlace> data) {
        if (data != null) {
            adapter.addData(data);
        }
    }

    private void updateCommentsData(List<ItemCommentPlace> data) {
        if (data != null) {
            adapter.cleanData();
            adapter.addData(data);
        }

    }

    @Override
    public void onItemCommentClick(View view, int position) {
        Log.i("Like position ", String.valueOf(position));
    }

    @Override
    public void showAllItemsData(final ItemSubsection item) {
        if (this.isVisible()) {
            title = item.getTitle();
            userLogin = item.getUser_login();
            owner = item.getOwner();
            progressBarAll.setVisibility(View.INVISIBLE);
            allViewContainer.setVisibility(View.VISIBLE);
            namePlace.setText(item.getAddress());
            descriptionPlace.setText(item.getDescription());
            dateAddingDescription.setText(TimeUtil.getDateAddedFormatItemCommunity(item.getTime()) + " By ");
            if (item.getFirst_name() != null) {
                userNameAdded.setText(item.getFirst_name().isEmpty() ? item.getUser_login() : item.getFirst_name());
            } else {
                userNameAdded.setText("");
            }

            ratingText.setText(item.getRating());
            currentRating = item.getRating();
            countRatted.setText(item.getVotes_count());
            ((MainActivity) getActivity()).setActionBarTitle(item.getTitle(), true);

            isLiked = item.getLikes().equalsIgnoreCase("1") ? true : false;
            if (isLiked) {
                likeButton.setImageResource(R.drawable.ic_like_selected);
            } else {
                likeButton.setImageResource(R.drawable.ic_like);
            }
            String userLikesCount = item.getLikes_count().isEmpty() ? "0" : item.getLikes_count();

            if (item.getLikes().equalsIgnoreCase("1")) {
                if (Integer.parseInt(userLikesCount) > 1) {

                    String likesCountStr = item.getLikes().equalsIgnoreCase("1") ?
                            "You and " + (Integer.parseInt(item.getLikes_count()) - 1) + " users likes this" :
                            userLikesCount + " users likes this";
                    likesCount.setText(likesCountStr);
                } else {
                    String likesCountStr = "You  like this";
                    likesCount.setText(likesCountStr);
                }
            } else {
                if (Integer.parseInt(userLikesCount) > 0) {
                    if (Integer.parseInt(userLikesCount) == 1) {
                        String likesCountStr = userLikesCount + " users like this";
                        likesCount.setText(likesCountStr);
                    } else {
                        String likesCountStr = userLikesCount + " users likes this";
                        likesCount.setText(likesCountStr);
                    }

                } else {
                    likesCount.setText("0");
                }
            }


            ratingBar.setRating(item.getRating().equalsIgnoreCase("") ? 0 : Float.valueOf(item.getRating()));
            countViewers.setText(item.getViews_count());
            countComments.setText(item.getComments_count());

            if (!item.getImage_url().isEmpty()) {
                if (item.getImage_url().startsWith("http://") || item.getImage_url().startsWith("https://")) {

                    Picasso.with(getActivity()).load(item.getImage_url())
                            .into(/*placePhoto*/target);
                } else if (item.getImage_url().startsWith("/")) {

                    Picasso.with(getActivity()).load(SslRequestHandler.IMAGE_URL_PREFIX + item.getImage_url())
                            .into(/*placePhoto*/target);
                }

            }

            if (item.getVote().equalsIgnoreCase("0") || item.getVote().equalsIgnoreCase("")) {
                ratingBar.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            dialogFragmentRating = DialogFragmentRating.newInstance();
                            dialogFragmentRating.setCallback(new DialogRatingCallback() {
                                @Override
                                public void confirm(float rating) {
                                    presenter.sendRating(itemGuid, null, String.valueOf((int) rating));
                                    ratingBar.setOnTouchListener(null);
                                }

                                @Override
                                public void cancel() {
                                    dialogFragmentRating.dismiss();
                                }
                            });
                            dialogFragmentRating.show(getActivity().getFragmentManager(), "dlg");
                        }
                        return true;
                    }
                });
            }


        }


    }

    @Override
    public void showComments(List<ItemCommentPlace> comments) {
        if (this.isVisible()) {
            if (comments != null) {
                if (comments.size() > 0) {

                    currentOffset = currentOffset + comments.size();
                    commentRecycler.setVisibility(View.VISIBLE);

                    if (isLoadMoreCliked) {

                        isLoadMoreCliked = false;
                        FragmentCommunitySectionItem.this.addCommentsData(comments);

                    } else {

                        FragmentCommunitySectionItem.this.updateCommentsData(comments);
                    }

                    noDataTextView.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);

                } else {

                    noDataTextView.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }
        }

    }

    @Override
    public void showCommentsCount(String commentCount) {

    }

    @Override
    public void showRating(String itemRating) {

    }

    @Override
    public void showLikesCount(String itemsLikesCount) {

    }

    @Override
    public void updateAllData(Response response) {
        if (this.isVisible()) {
            if (response != null) {
                if (response.getRequestResult().equalsIgnoreCase("1")) {
                    dialogFragmentRating.dismiss();
                    Toast.makeText(getActivity(), "You rated this item", Toast.LENGTH_SHORT).show();
                    presenter.getAllItemsData(itemGuid, null);
                } else {
                    Toast.makeText(getActivity(), "Please, try again", Toast.LENGTH_SHORT).show();
                }

            }
            presenter.getAllItemsData(itemGuid, PreferencesManager.getInstance().getUserId());
        }
    }

    @Override
    public void updateDataAfterSendComment() {
        if (this.isVisible()) {
            isAddCommentClick = false;
            dialogSendComment.dismiss();
            presenter.getAllItemsData(itemGuid, PreferencesManager.getInstance().getUserId());
            currentOffset = 0;
            presenter.getComments(itemGuid, String.valueOf(DATA_LIMIT), String.valueOf(currentOffset));
        }
    }

}
