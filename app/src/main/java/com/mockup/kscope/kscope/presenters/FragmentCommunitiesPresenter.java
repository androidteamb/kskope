package com.mockup.kscope.kscope.presenters;

/**
 * Created by Stafiiyevskyi on 04.12.2015.
 */
public interface FragmentCommunitiesPresenter {

    void entranceToCommunity(String guidCommunity);
    void exitFromCommunity(String guidCommunity);
    void getCommunities();

}
