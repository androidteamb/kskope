package com.mockup.kscope.kscope.api.response;

/**
 * Created by Alexander Rubanskiy on 30.10.2015.
 */

public class InvalidParam {

    private String detail_err_msg;
    private String err_msg;
    private String id_err_msg;

    public String getDetailErrorMessage() {
        return detail_err_msg;
    }

    public void setDetailErrorMessage(String detail_err_msg) {
        this.detail_err_msg = detail_err_msg;
    }

    public String getErrorMessage() {
        return err_msg;
    }

    public void setErrorMessage(String err_msg) {
        this.err_msg = err_msg;
    }

    public String getIdErrorMessage() {
        return id_err_msg;
    }

    public void setIdErrorMessage(String id_err_msg) {
        this.id_err_msg = id_err_msg;
    }

}