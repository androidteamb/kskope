package com.mockup.kscope.kscope.api.response;

/**
 * Created by Stafiiyevskyi on 09.12.2015.
 */
public class UpdateUserDataResponse extends Response {

    private UserRequestParam request_param = new UserRequestParam();
    public String getUserImage(){
        return request_param.getUser_image();
    }

    public UserRequestParam getRequestParam(){
        return request_param;
    }

    public static class UserRequestParam extends InvalidParam {
        private String user_image;

        public String getUser_image() {
            return user_image;
        }
    }
}