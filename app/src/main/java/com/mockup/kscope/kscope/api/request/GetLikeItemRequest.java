package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 23.11.2015.
 */
public class GetLikeItemRequest extends Request {

    private GetLike request_param;

    public GetLikeItemRequest(String guid_item, String sessionId){
        setRequestCmd("get_like");
        setSessionId(sessionId);
        request_param = new GetLike();
        request_param.guid_item = guid_item;
        request_param.id_user = null;
    }

    public GetLikeItemRequest(String guid_item, String sessionId, String userId){
        setRequestCmd("get_like");
        setSessionId(sessionId);
        request_param = new GetLike();
        request_param.guid_item = guid_item;
        request_param.id_user = userId;
    }

    private class GetLike {
        @SuppressWarnings("unused")
        private String guid_item;
        @SuppressWarnings("unused")
        private String id_user;
    }

}