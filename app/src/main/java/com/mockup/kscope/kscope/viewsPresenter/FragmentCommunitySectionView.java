package com.mockup.kscope.kscope.viewsPresenter;

import com.mockup.kscope.kscope.entity.ItemCommunities;

import java.util.List;

/**
 * Created by Stafiiyevskyi on 02.12.2015.
 */
public interface FragmentCommunitySectionView {

    void showCommunitySections(List<ItemCommunities> sections);
}
