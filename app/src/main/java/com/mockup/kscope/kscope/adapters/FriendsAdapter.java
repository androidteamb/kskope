package com.mockup.kscope.kscope.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.entity.ItemFriend;
import com.mockup.kscope.kscope.utils.CircleTransform;
import com.mockup.kscope.kscope.utils.NameUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */

public class FriendsAdapter extends BaseAdapter implements Filterable {

    private List<ItemFriend> friendsList;
    private List<ItemFriend> orig;
    private Context mContext;
    private OnStateButton callback;
    private OnFriendsClick friendsClickCallback;

    public FriendsAdapter(OnFriendsClick friendsClickCallback, Context mContext, List<ItemFriend> friendsList) {
        this.friendsList = friendsList;
        this.friendsClickCallback = friendsClickCallback;
        this.mContext = mContext;
    }

    public void setCallback(OnStateButton callback) {
        this.callback = callback;
    }

    @Override
    public int getCount() {
        return friendsList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItems(List<ItemFriend> friends) {
        this.friendsList = friends;
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_friends, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final ItemFriend itemFriend = friendsList.get(position);
        String user_photo = itemFriend.getUser_photo();
        if (!TextUtils.isEmpty(user_photo)) {
            Picasso.with(mContext)
                    .load(user_photo)
                    .placeholder(R.drawable.ic_photo_placeholder)
                    .fit()
                    .centerCrop()
                    .transform(new CircleTransform())
                    .into(holder.ivFriendsPhoto);
        } else {
            Picasso.with(mContext)
                    .load(R.drawable.ic_photo_placeholder)
                            //.load(friendsList.get(position).getPhoto())
                    .placeholder(R.drawable.ic_photo_placeholder)
                    .into(holder.ivFriendsPhoto);
        }

        if (itemFriend.isConfirm()) {
            showStateButtons(holder, itemFriend.getUser_login());
            holder.tvFriendsName.setTextColor(mContext.getResources().getColor(R.color.holo_red_light));
            holder.tvFriendsFollowers.setText("123" + " Following");
        } else {
            holder.tvFriendsName.setTextColor(mContext.getResources().getColor(R.color.typeface_black));
            hideStateButtons(holder);
            holder.tvFriendsFollowers.setText("123" + " Following");
            //holder.tvFriendsFollowers.setText(friendsList.get(position).getFollowers() + " Following");
        }
        holder.tvFriendsName.setText(NameUtil.getName(itemFriend.getFirst_name(),
                itemFriend.getSecond_name(), itemFriend.getUser_login()));
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                friendsClickCallback.onFriendsItemClick(itemFriend.getUser_login());
            }
        });

        return view;
    }

    private void hideStateButtons(ViewHolder holder) {
        holder.confirmFriend.setVisibility(View.GONE);
        holder.cancelFriend.setVisibility(View.GONE);
    }

    private void showStateButtons(ViewHolder holder, final String userLogin) {
        holder.confirmFriend.setVisibility(View.VISIBLE);
        holder.confirmFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onConfirm(userLogin);
            }
        });
        holder.cancelFriend.setVisibility(View.VISIBLE);
        holder.cancelFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onCancel(userLogin);
            }
        });
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults filter = new FilterResults();
                final List<ItemFriend> results = new ArrayList<>();
                if (orig == null)
                    orig = friendsList;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final ItemFriend item : orig) {
                            if (item.getUser_login().toLowerCase()
                                    .contains(constraint.toString()))
                                results.add(item);
                        }
                    }
                    filter.values = results;
                }
                return filter;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                friendsList = (List<ItemFriend>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface OnFriendsClick{
        public void onFriendsItemClick(String userLogin);
    }

    public interface OnStateButton {

        public void onConfirm(String userLogin);

        public void onCancel(String userLogin);
    }

    static class ViewHolder {

        @InjectView(R.id.friends_name)
        TextView tvFriendsName;
        @InjectView(R.id.friends_followers)
        TextView tvFriendsFollowers;
        @InjectView(R.id.image)
        ImageView ivFriendsPhoto;
        @InjectView(R.id.confirm)
        LinearLayout confirmFriend;
        @InjectView(R.id.cancel)
        LinearLayout cancelFriend;
        @InjectView(R.id.icon_confirm)
        ImageView iconConfirm;
        @InjectView(R.id.icon_cancel)
        ImageView iconCancel;
        @InjectView(R.id.root)
        RelativeLayout root;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

}
