package com.mockup.kscope.kscope.fragments.main;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import butterknife.InjectView;
import it.sephiroth.android.library.imagezoom.ImageViewTouch;

/**
 * Created by Serhii_Slobodyanuk on 11.12.2015.
 */
public class FragmentImagePreview extends BaseFragment {

    private static final String IMAGE_URL = "image_url";

    @InjectView(R.id.image_preview)
    ImageViewTouch imagePreview;
    @InjectView(R.id.progress_bar)
    ProgressBar progressBar;

    private String imageUrl;
    private DisplayImageOptions options;

    public static Fragment newInstance(String imageUrl) {
        FragmentImagePreview fragment = new FragmentImagePreview();
        Bundle args = new Bundle();
        args.putString(IMAGE_URL, imageUrl);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageUrl = getArguments().getString(IMAGE_URL);

        if (!imageUrl.isEmpty()) {
            if (imageUrl.startsWith("http://") || imageUrl.startsWith("https://")) {
                Picasso.with(getActivity()).load(imageUrl).noPlaceholder().into(imagePreview);
            } else if (imageUrl.startsWith("/")) {
                Picasso.with(getActivity()).load(SslRequestHandler.IMAGE_URL_PREFIX + imageUrl).noPlaceholder().into(target);
            }
        }
    }

    private void showProgressBar() {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        ImageLoader.getInstance().cancelDisplayTask(imagePreview);
    }

    private void hideProgressBar() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_image_preview;
    }

    @Override
    public void updateToolbar() {
        ((MainActivity) getActivity()).setActionBarTitle(getActivity().getResources().getString(R.string.my_gallery), true);
    }

    private com.squareup.picasso.Target target = new com.squareup.picasso.Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            if (FragmentImagePreview.this.isVisible()) {
                imagePreview.setImageBitmap(bitmap);
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };


}
