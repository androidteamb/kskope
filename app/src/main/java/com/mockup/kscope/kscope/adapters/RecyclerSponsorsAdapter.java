package com.mockup.kscope.kscope.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.entity.ItemSponsor;
import com.mockup.kscope.kscope.utils.DisplayUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Stafiiyevskyi on 28.10.2015.
 */
public class RecyclerSponsorsAdapter extends RecyclerView.Adapter<RecyclerSponsorsAdapter.SponsorsRecyclerViewHolder> {

    private List<ItemSponsor> data = new ArrayList<>();
    private Activity context;
    private OnItemClickListener onItemClickListener;
    private DisplayImageOptions options;


    public RecyclerSponsorsAdapter() {
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.placeholder)
                .showImageForEmptyUri(R.drawable.placeholder)
                .showImageOnFail(R.drawable.placeholder)
                .cacheInMemory(true)
                .considerExifParams(false)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    @Override
    public SponsorsRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_community_list, parent, false);
        SponsorsRecyclerViewHolder sponsorsRecyclerViewHolder = new SponsorsRecyclerViewHolder(view);

        return sponsorsRecyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final SponsorsRecyclerViewHolder holder, final int position) {


        String url = data.get(position).getLink_picture();

        holder.sponsorDescription.setText(data.get(position).getTitle());

        if (url.isEmpty()) {

            Picasso.with(context).load(R.drawable.bg_calendar_selected).into(holder.sponsorBanner);

        } else {

            if (url.startsWith("http://") || url.startsWith("https://")) {

                Picasso.with(context).load(url).placeholder(R.drawable.bg_calendar_selected).into(holder.sponsorBanner);

            } else if (url.startsWith("/")) {

                Picasso.with(context).load(SslRequestHandler.IMAGE_URL_PREFIX + url).placeholder(R.drawable.bg_calendar_selected).into(holder.sponsorBanner);

            }

        }


    }

    @Override
    public int getItemCount() {
        if (data != null) {

            return data.size();

        } else return 0;
    }

    public void clearData() {
        if (data != null) {
            data.clear();
        }

    }

    public void addData(List<ItemSponsor> data) {
        this.data.addAll(data);

    }

    public void setContext(Activity context) {
        this.context = context;
    }

    public ItemSponsor getItemSponsorByPosition(int position) {
        return data.get(position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public interface OnItemClickListener {
        void onItemClick(View v, int position);
    }

    public class SponsorsRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @InjectView(R.id.iv_community_logo)
        public ImageView sponsorBanner;
        @InjectView(R.id.tv_community_name)
        public TextView sponsorDescription;


        public SponsorsRecyclerViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.inject(this, itemView);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

}
