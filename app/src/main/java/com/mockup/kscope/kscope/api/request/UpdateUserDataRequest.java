package com.mockup.kscope.kscope.api.request;

import com.mockup.kscope.kscope.api.model.User;

import java.util.ArrayList;

public class UpdateUserDataRequest extends Request {

    private User request_param;
    public UpdateUserDataRequest(String sessionId, User user) {
        setRequestCmd("update_user_data");
        setSessionId(sessionId);
        request_param = new User();
        request_param = user;
    }

//    private class UpdateUser{
//        @SuppressWarnings("unused")
//        String user_data;
//    }
}
