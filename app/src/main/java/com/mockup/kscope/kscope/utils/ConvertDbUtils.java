package com.mockup.kscope.kscope.utils;

import com.mockup.kscope.kscope.database.models.DbFriend;
import com.mockup.kscope.kscope.entity.ItemFriend;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mpodolsky on 04.12.2015.
 */
public class ConvertDbUtils {

    public static List<ItemFriend> getFriends(List<DbFriend> dbFriends) {
        List<ItemFriend> friends = new ArrayList<>();
        if (dbFriends != null && dbFriends.size() > 0) {
            for (DbFriend dbFriend :
                    dbFriends) {
                friends.add(dbFriend.getFriend());
            }
        }
        return friends;
    }

}
