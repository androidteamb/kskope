package com.mockup.kscope.kscope.fragments.main;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.activities.StartUpActivity;
import com.mockup.kscope.kscope.adapters.MainMenuListAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.LogOutRequest;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.dialogs.ConfirmDialog;
import com.mockup.kscope.kscope.dialogs.DialogCallback;
import com.mockup.kscope.kscope.entity.ItemMainMenu;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.fragments.communitySections.FragmentCommunitySection;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.utils.Utils;
import com.twitter.sdk.android.Twitter;

import java.util.ArrayList;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class FragmentMainMenu extends BaseFragment {

    @InjectView(R.id.main_menu_list)
    ListView lvMainMenu;
    @InjectView(R.id.progress)
    FrameLayout progressBar;
    EventBus eventBus = EventBus.getDefault();
    private PreferencesManager preferencesManager = PreferencesManager.getInstance();
    private Context mContext;
    private Resources mResources;
    private String[] ArrayMainList;
    private String title;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity)getActivity()).setActionBarTitle("Main menu", true);
        ((MainActivity)getActivity()).getToolbar().setNavigationIcon(null);
        mContext = getActivity();
        mResources = getResources();

        ArrayMainList = mResources.getStringArray(R.array.main_menu_array);
        MainMenuListAdapter adapter = new MainMenuListAdapter(mContext, initList());
        adapter.setCallback(new MainMenuListAdapter.Callback() {
            @Override
            public void onMenuClick(int position) {
                addFragment(position);
            }
        });
        lvMainMenu.setAdapter(adapter);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_main;
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    private ArrayList<ItemMainMenu> initList() {
        ArrayList<ItemMainMenu> items = new ArrayList<>();
        for (int i = 0; i < ArrayMainList.length; i++) {
            ItemMainMenu itemMainMenu = new ItemMainMenu(ArrayMainList[i]);
            int iconRes = R.drawable.bg_calendar_selected;
            if (i == 0) {
                iconRes = R.drawable.ic_mm_profile;
            } else if (i == 1) {
                iconRes = R.drawable.ic_mm_communities;
            } else if (i == 2) {
                iconRes = R.drawable.ic_mm_calendar;
            } else if (i == 3) {
                iconRes = R.drawable.ic_mm_about_us;
            } else if (i == 4) {
                iconRes = R.drawable.ic_mm_sponsors;
            } else if (i == 5) {
                iconRes = R.drawable.ic_mm_testimonials;
            } else if (i == 6) {
                iconRes = R.drawable.ic_mm_community_section;
            }
            itemMainMenu.setIconResource(iconRes);
            items.add(itemMainMenu);
        }
        return items;
    }

    private void addFragment(int position) {
        if (getFragment(position) != null) {
            if (Utils.isNetworkConnected(mContext)) {
                Fragment fragment = getFragment(position);
                ((MainActivity) mContext).addFragment(fragment, title);
            } else {
                Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private Fragment getFragment(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = FragmentProfile.newInstance();
                title = ArrayMainList[0];
                break;
            case 1:
                fragment = new FragmentCommunities();
                title = ArrayMainList[1];
                break;
            case 2:
                fragment = new FragmentCalendar();
                title = ArrayMainList[2];
                break;
            case 3:
                fragment = new FragmentAbout();
                title = ArrayMainList[3];
                break;
            case 4:
                fragment = new FragmentSponsors();
                title = ArrayMainList[4];
                break;
            case 5:
                fragment = new FragmentTestimonials();
                title = ArrayMainList[5];
                break;
            case 6:
                fragment = new FragmentCommunitySection();
                title = ArrayMainList[6];
                break;
            case 7:
                //lvMainMenu.setVisibility(View.VISIBLE);

                    ConfirmDialog dialog = ConfirmDialog.newInstance(R.string.dialog_logout_title,
                            R.string.dialog_logout_messages,
                            R.string.dialog_confirm,
                            R.string.dialog_cancel);
                    dialog.setCallback(new DialogCallback.EmptyCallback() {
                        @Override
                        public void confirm() {
                            if (Utils.isNetworkConnected(mContext)) {
                                logout();
                            } else {
                                Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                    dialog.show(getFragmentManager(), "dialog");


                return null;
            default:
                break;
        }
        return fragment;
    }

    private void logout() {
        // facebook logout
        LoginManager.getInstance().logOut();
        // twitter logout
        Twitter.getInstance().logOut();
        // server logout
        SslRequestHandler.SslRequestListener logoutListener = new SslRequestHandler.SslRequestListener() {
            @Override
            public void onStart() {
                PreferenceManager.getDefaultSharedPreferences(mContext).edit().clear().apply();
                PreferencesManager.getInstance().clear();
                if (FragmentMainMenu.this.isVisible()){
                    progressBar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSuccess(Object response) {
            }

            @Override
            public void onFailure(String errorMsg) {
                if (FragmentMainMenu.this.isVisible()){
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFinish() {

                startActivity(new Intent(mContext, StartUpActivity.class));
                if (FragmentMainMenu.this.isVisible()){
                    progressBar.setVisibility(View.GONE);
                }
                getActivity().finish();
            }
        };
        LogOutRequest logOutRequest = new LogOutRequest(preferencesManager.getSessionId());
        SslRequestHandler.getInstance().sendRequest(logOutRequest, Response.class, logoutListener);
    }

    @Override
    public void updateToolbar() {
        ((MainActivity) mContext).getToolbar().setNavigationIcon(null);
        ((MainActivity) mContext).setActionBarTitle("Main menu", true);
    }

    @Override
    public void onResume() {
        super.onResume();
        //lvMainMenu.setVisibility(View.VISIBLE);

//        ((MainActivity) mContext).getToolbar().setNavigationIcon(null);
//        ((MainActivity) mContext).setActionBarTitle("Main menu", true);
    }

}