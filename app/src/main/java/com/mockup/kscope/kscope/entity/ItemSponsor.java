package com.mockup.kscope.kscope.entity;

/**
 * Created by Stafiiyevskyi on 28.10.2015.
 */
public class ItemSponsor {

    private String link_picture;
    private String title;
    private String link_homepage;
    private String guid_sponsor;

    public String getLink_homepage() {
        return link_homepage;
    }

    public void setLink_homepage(String link_homepage) {
        this.link_homepage = link_homepage;
    }

    public String getGuid_sponsor() {
        return guid_sponsor;
    }

    public void setGuid_sponsor(String guid_sponsor) {
        this.guid_sponsor = guid_sponsor;
    }

    public String getLink_picture() {
        return link_picture;
    }

    public void setLink_picture(String link_picture) {
        this.link_picture = link_picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
