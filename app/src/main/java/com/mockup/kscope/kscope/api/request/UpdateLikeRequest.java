package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 23.11.2015.
 */
public class UpdateLikeRequest extends Request {

    private UpdateLike request_param;

    public UpdateLikeRequest(String guid_item, String sessionId, String like_time){
        setRequestCmd("update_like");
        setSessionId(sessionId);
        request_param = new UpdateLike();
        request_param.guid_item = guid_item;
        request_param.like_time = like_time;
    }

    private class UpdateLike {
        @SuppressWarnings("unused")
        private String guid_item;
        @SuppressWarnings("unused")
        private String like_time;
    }

}