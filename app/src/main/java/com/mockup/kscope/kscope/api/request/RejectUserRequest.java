package com.mockup.kscope.kscope.api.request;

import java.util.ArrayList;

/**
 * Created by Alexander Rubanskiy on 16.12.2015.
 */

public class RejectUserRequest extends Request {

    private UserData request_param;

    public RejectUserRequest(String sessionId, String userLogin) {
        setRequestCmd("reject_contact");
        setSessionId(sessionId);
        request_param = new UserData();
        request_param.user_data.add(new UserLogin(userLogin));
    }

    private class UserData {
        @SuppressWarnings("unused")
        private ArrayList<UserLogin> user_data = new ArrayList<>();
    }

    private class UserLogin{
        @SuppressWarnings("unused")
        private String user_login;

        public UserLogin(String userLogin) {
            user_login = userLogin;
        }
    }

}