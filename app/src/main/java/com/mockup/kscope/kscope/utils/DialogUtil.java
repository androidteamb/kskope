package com.mockup.kscope.kscope.utils;

import com.mockup.kscope.kscope.entity.ItemMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mpodolsky on 16.12.2015.
 */
public class DialogUtil {

    public static List<ItemMessage> getUniqueMessages(List<ItemMessage> list) {
        List<ItemMessage> messages = new ArrayList<>();
        for (ItemMessage message : list) {
            if (!containsId(messages, message.getIdSender())) {
                messages.add(message);
            }
        }
        return messages;
    }

    private static boolean containsId(List<ItemMessage> list, String id) {
        boolean contains = false;
        for (ItemMessage message : list) {
            if (message.getIdSender().equals(id)) {
                contains = true;
            }
        }
        return contains;
    }

}
