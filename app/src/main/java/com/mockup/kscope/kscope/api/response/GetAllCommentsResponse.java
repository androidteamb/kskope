package com.mockup.kscope.kscope.api.response;

import com.mockup.kscope.kscope.entity.ItemCommentPlace;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stafiiyevskyi on 17.11.2015.
 */
public class GetAllCommentsResponse extends Response{

    private CommentsAllRequestParam request_param = new CommentsAllRequestParam();

    public List<ItemCommentPlace> getAllComments(){
        return request_param.user_item;
    }

    public CommentsAllRequestParam getRequestParam() {
        return request_param;
    }

    public static class CommentsAllRequestParam extends InvalidParam {

        private String data_count;
        private String data_limit;
        private String data_offset;
        private ArrayList<ItemCommentPlace> user_item;

        public String getDataCount() {
            return data_count;
        }

        public String getDataLimit() {
            return data_limit;
        }

        public String getDataOffset() {
            return data_offset;
        }

        public ArrayList<ItemCommentPlace> getComments(){
            return user_item;
        }
    }
}
