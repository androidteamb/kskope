package com.mockup.kscope.kscope.events;

/**
 * Created by Serhii_Slobodianiuk on 26.11.2015.
 */
public class LikesFilterEvent {

    private String queryText;

    public LikesFilterEvent(String newText) {
        this.queryText = newText;
    }

    public String getQueryText() {
        return queryText;
    }
}
