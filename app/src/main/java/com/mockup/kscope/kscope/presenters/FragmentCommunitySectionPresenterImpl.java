package com.mockup.kscope.kscope.presenters;

import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.GetCommunityRequest;
import com.mockup.kscope.kscope.api.response.GetCommunityResponse;
import com.mockup.kscope.kscope.viewsPresenter.FragmentCommunitySectionView;

/**
 * Created by Stafiiyevskyi on 02.12.2015.
 */
public class FragmentCommunitySectionPresenterImpl implements FragmentCommunitySectionPresenter{

    private FragmentCommunitySectionView view;

    public FragmentCommunitySectionPresenterImpl(FragmentCommunitySectionView view) {
        this.view = view;
    }

    @Override
    public void getCommunitySections() {
        GetCommunityRequest getCommunityRequest = new GetCommunityRequest();
        SslRequestHandler.getInstance().sendRequest(getCommunityRequest, GetCommunityResponse.class, new SslRequestHandler.SslRequestListener<GetCommunityResponse>() {
            GetCommunityResponse getCommunityResponse;

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(GetCommunityResponse response) {
                getCommunityResponse = response;
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                if (getCommunityResponse!=null){
                    if (getCommunityResponse.getCommunity()!=null){
                        view.showCommunitySections(getCommunityResponse.getCommunity());
                    }
                }
            }
        });


    }


}
