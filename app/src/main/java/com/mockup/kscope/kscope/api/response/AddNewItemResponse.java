package com.mockup.kscope.kscope.api.response;

import com.mockup.kscope.kscope.entity.ItemSubsection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stafiiyevskyi on 09.12.2015.
 */
public class AddNewItemResponse extends Response{

    private AddNewItemRequestParam request_param = new AddNewItemRequestParam();

    public List<ItemSubsection> getItem(){
        return request_param.item;
    }

    public AddNewItemRequestParam getRequestParam() {
        return request_param;
    }

    public static class AddNewItemRequestParam extends InvalidParam {

        private ArrayList<ItemSubsection> item;


        public ArrayList<ItemSubsection> getItem(){
            return item;
        }
    }
}
