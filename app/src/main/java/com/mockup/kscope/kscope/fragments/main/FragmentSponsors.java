package com.mockup.kscope.kscope.fragments.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.RecyclerSponsorsAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.GetSponsorRequest;
import com.mockup.kscope.kscope.api.response.GetSponsorResponse;
import com.mockup.kscope.kscope.customView.DividerItemDecoration;
import com.mockup.kscope.kscope.entity.ItemSponsor;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;

/**
 * Created by Stafiiyevskyi on 28.10.2015.
 */
public class FragmentSponsors extends BaseFragment implements RecyclerSponsorsAdapter.OnItemClickListener {

    @InjectView(R.id.recycler_sponsors)
    RecyclerView sponsorsRecycler;
    @InjectView(R.id.progress)
    ProgressBar progressBar;
    @InjectView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private RecyclerSponsorsAdapter recyclerSponsorsAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        prepareAdapter();
        prepareRecyclerView();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Utils.isNetworkConnected(getActivity())) {
                    swipeRefreshLayout.setRefreshing(true);
                    getSponsors();
                } else {
                    Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
                }
            }
        });

        if (Utils.isNetworkConnected(getActivity())) {
            getSponsors();
        } else {
            Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
        }
        return view;
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_sponsors;
    }

    @Override
    public void updateToolbar() {
        ((MainActivity) getActivity()).setActionBarTitle("Sponsors", true);
    }

    private void showInformation(List<ItemSponsor> sponsors){
        if (sponsors != null) {
            recyclerSponsorsAdapter.clearData();
            recyclerSponsorsAdapter.addData(sponsors);
            recyclerSponsorsAdapter.notifyDataSetChanged();
        }
    }

    private void prepareAdapter() {
        recyclerSponsorsAdapter = new RecyclerSponsorsAdapter();
        recyclerSponsorsAdapter.setContext(getActivity());
        recyclerSponsorsAdapter.setOnItemClickListener(this);
    }


    private void prepareRecyclerView() {
//        StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        sponsorsRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        sponsorsRecycler.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        sponsorsRecycler.setItemAnimator(new DefaultItemAnimator());
        sponsorsRecycler.setAdapter(recyclerSponsorsAdapter);
    }

    private void getSponsors() {
        GetSponsorRequest getSponsorRequest = new GetSponsorRequest();
        SslRequestHandler.getInstance().sendRequest(getSponsorRequest, GetSponsorResponse.class, requestListener);
    }

    @Override
    public void onItemClick(View v, int position) {
        String url = recyclerSponsorsAdapter.getItemSponsorByPosition(position).getLink_homepage();
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        getActivity().startActivity(browserIntent);
    }

    private SslRequestHandler.SslRequestListener requestListener = new SslRequestHandler.SslRequestListener<GetSponsorResponse>() {
        List<ItemSponsor> sponsorsResult = null;

        @Override
        public void onStart() {
            if (!swipeRefreshLayout.isRefreshing()) {
                progressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onSuccess(GetSponsorResponse response) {
            ArrayList<ItemSponsor> sponsors = response.getRequestParam().getSponsor();
            sponsorsResult = sponsors;
            Log.i("Sponsor response ", response.toString());

        }

        @Override
        public void onFailure(String errorMsg) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFinish() {
            if (FragmentSponsors.this.isVisible()) {
                swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.INVISIBLE);
                showInformation(sponsorsResult);
            }

        }
    };

}