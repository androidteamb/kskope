package com.mockup.kscope.kscope.entity;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class ItemAbout {

    private String name;
    private String value;

    public ItemAbout(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
