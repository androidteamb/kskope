package com.mockup.kscope.kscope.api.response;

import com.mockup.kscope.kscope.entity.ItemSubsection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stafiiyevskyi on 06.11.2015.
 */
public class GetSubsectionItemsResponse extends Response{

    private GetSubsectionItemsParam request_param = new GetSubsectionItemsParam();

    public List<ItemSubsection> getSubsectionItems(){
        return request_param.item;
    }

    public GetSubsectionItemsParam getRequestParam() {
        return request_param;
    }

    public static class GetSubsectionItemsParam extends InvalidParam {

        private String data_count;
        private String data_limit;
        private String data_offset;
        private ArrayList<ItemSubsection> item;

        public String getDataCount() {
            return data_count;
        }

        public String getDataLimit() {
            return data_limit;
        }

        public String getDataOffset() {
            return data_offset;
        }

        public ArrayList<ItemSubsection> getItem(){
            return item;
        }
    }
}
