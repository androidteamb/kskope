package com.mockup.kscope.kscope.presenters;

/**
 * Created by Stafiiyevskyi on 02.12.2015.
 */
public interface FragmentAddNewItemPresenter {

    void getCommunitySections();
    void getCommunitySubsections(String guidSections);
    void addNewItem(String title, String description, String time, String address, String phone, String guid_subsection);

}
