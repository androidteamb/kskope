package com.mockup.kscope.kscope.entity;

import java.util.ArrayList;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class ItemCommunities {

    private String title;
    private String guid_community;
    private String guid_subsection;
    private String image_url;
    private String value;
    private String entrance_to_community;
    private ArrayList<ItemSubCommunities> sub_section;

    public ArrayList<ItemSubCommunities> getSubCommunities() {
        return sub_section;
    }

    public ItemCommunities(String name, String guid_community) {
        this.title = name;
        this.guid_community = guid_community;
    }

    public ItemCommunities(String name) {
        this.title = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getAdded() {
        return entrance_to_community;
    }

    public void setAdded(String added) {
        this.entrance_to_community = added;
    }

    public String getGuid_subsection() {
        return guid_subsection;
    }

    public void setGuid_subsection(String guid_subsection) {
        this.guid_subsection = guid_subsection;
    }

    public String getGuid_community() {
        return guid_community;
    }

    public void setGuid_community(String guid_community) {
        this.guid_community = guid_community;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return title;
    }

    public String getLogo() {
        return image_url;
    }

    public void setLogo(String logo) {
        this.image_url = logo;
    }
}
