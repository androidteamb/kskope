package com.mockup.kscope.kscope.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.customView.TypefaceTextView;
import com.mockup.kscope.kscope.entity.ItemCommunities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class CommunitiesAdapter extends ArrayAdapter<ItemCommunities> {

    private final static int RESOURCE = R.layout.item_community_list_check;

    private final Context context;
    private LayoutInflater inflater;
    private List<ItemCommunities> items;
    private Callback callback;

    public CommunitiesAdapter(Context context, List<ItemCommunities> items, Callback listener) {
        super(context, RESOURCE, items);
        this.context = context;
        this.items = items;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setCallback(listener);
    }

    @Override
    public ItemCommunities getItem(int position) {
        return items.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        View item = convertView;
//        if (item == null) {

            item = inflater.inflate(RESOURCE, parent, false);
            holder = new ViewHolder();
            holder.tvName = (TextView) item.findViewById(R.id.tv_community_name);
            holder.itemContainer = (RelativeLayout) item.findViewById(R.id.item_container);
            holder.checkBox = (CheckBox) item.findViewById(R.id.checkbox);
            item.setTag(holder);
//        }
//        else {
//            holder = (ViewHolder) item.getTag();
//        }

        final ItemCommunities menuItem = getItem(position);

        holder.tvName.setText(menuItem.getName());
        holder.itemContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.checkBox.isChecked()) {
                    holder.checkBox.setChecked(false);
                } else {
                    holder.checkBox.setChecked(true);
                }
                callback.onItemClick(menuItem, position);
            }
        });

        return item;
    }

    static class ViewHolder {
        private RelativeLayout itemContainer;
        private TextView tvName;
        private CheckBox checkBox;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public interface Callback {
        void onItemClick(ItemCommunities community, int position);
    }
}