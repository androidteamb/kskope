package com.mockup.kscope.kscope.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Serhii_Slobodianiuk on 28.10.2015.
 **/
public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private List<String> data;
    private Context context;
    private Callback mCallback;
    private DisplayImageOptions options;


    public GalleryAdapter(Callback mCallback, Context context, List<String> data) {
        this.context = context;
        this.data = data;
        this.mCallback = mCallback;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public void addItems(List<String> newItems) {
        if (newItems != null) {
            data.addAll(newItems);
            notifyItemRangeInserted(getItemCount(), newItems.size());
        }
    }

    public List<String> getItems() {
        return data;
    }

    public void setItems(List<String> newItems) {
        data.clear();
        if (newItems != null) {
            data.addAll(newItems);
            notifyDataSetChanged();
        } else {
            notifyDataSetChanged();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gallery_grid, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String item = data.get(position);
        holder.image.setTag(R.string.tag, item);
        holder.image.setImageResource(android.R.color.transparent);

//        if (!TextUtils.isEmpty(assetType) && assetType.equals("videos")) {
//            holder.play.setImageResource(R.drawable.ic_gallery_video);
//        } else {
//            holder.play.setImageResource(R.drawable.ic_gallery);
//        }

        holder.play.setImageResource(R.drawable.ic_gallery);
        if (!item.isEmpty()) {
            if (item.startsWith("http://") || item.startsWith("https://")) {
                Picasso.with(context).load(item).noPlaceholder().into(holder.image);
            } else if (item.startsWith("/")) {
                Picasso.with(context).load(SslRequestHandler.IMAGE_URL_PREFIX + item).noPlaceholder().into(holder.image);
            }
        }

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface Callback {
        void onClick(int position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.title)
        TextView title;
        @InjectView(R.id.image)
        ImageView image;
        @InjectView(R.id.published)
        TextView published;
        @InjectView(R.id.play)
        ImageView play;
        @InjectView(R.id.root)
        RelativeLayout mRoot;
        @InjectView(R.id.thumbnail_container)
        RelativeLayout mImageContainer;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}