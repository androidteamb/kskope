package com.mockup.kscope.kscope.dialogs;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Button;
import android.widget.DatePicker;


import com.mockup.kscope.kscope.R;

import java.util.Calendar;

/**
 * Created by Stafiiyevskyi on 03.12.2015.
 */
public class DialogFragmentDatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {


    private DatePickerListener listener;

    public static DialogFragmentDatePicker newInstance(DatePickerListener listener) {
        DialogFragmentDatePicker dialog = new DialogFragmentDatePicker();
        dialog.listener = listener;
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        Dialog picker = new DatePickerDialog(getActivity(), this,
                year, month, day);

        return picker;
    }

    @Override
    public void onStart() {
        super.onStart();

        Button nButton = ((AlertDialog) getDialog())
                .getButton(DialogInterface.BUTTON_POSITIVE);
        nButton.setText(getResources().getString(R.string.date_picker_confirm));

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, android.R.style.Theme_Light_Panel);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year,int month, int day) {

        int montNormal = month + 1;
        listener.setDate(year, montNormal, day);

    }


    public interface DatePickerListener {
        void setDate(int year, int month, int day);
    }
}