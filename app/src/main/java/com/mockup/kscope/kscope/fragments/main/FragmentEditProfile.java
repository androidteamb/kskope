package com.mockup.kscope.kscope.fragments.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.model.User;
import com.mockup.kscope.kscope.api.request.ConvertImageRequest;
import com.mockup.kscope.kscope.api.request.UpdateUserDataRequest;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.api.response.UpdateUserDataResponse;
import com.mockup.kscope.kscope.dialogs.PhotoDialog;
import com.mockup.kscope.kscope.events.AvatarChangedEvent;
import com.mockup.kscope.kscope.events.RefreshProfileFragmentEvent;
import com.mockup.kscope.kscope.events.SelectedProfilePhotoEvent;
import com.mockup.kscope.kscope.events.TakenProfilePhotoEvent;
import com.mockup.kscope.kscope.events.UploadSuccessEvent;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.fragments.PhotoFragment;
import com.mockup.kscope.kscope.utils.KeyboardUtils;
import com.mockup.kscope.kscope.utils.PathUtil;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.utils.UploadUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Serhii_Slobodianiuk on 02.11.2015.
 */
public class FragmentEditProfile extends BaseFragment {

    // Profile header
    @InjectView(R.id.photo)
    CircleImageView photo;
    @InjectView(R.id.username)
    TextView username;
    @InjectView(R.id.working)
    TextView about;
    @InjectView(R.id.city)
    TextView address;

    @InjectView(R.id.first_name)
    EditText etFirstName;
    @InjectView(R.id.last_name)
    EditText etLastName;
    @InjectView(R.id.email)
    EditText etEmail;
    @InjectView(R.id.info)
    EditText etInfo;
    @InjectView(R.id.address)
    EditText etAddress;
    @InjectView(R.id.confirm)
    Button btnConfirm;
    @InjectView(R.id.progress)
    ProgressBar progressBar;

    private UpdateUserDataResponse updateUserResponse;

    private PreferencesManager preferenceManager = PreferencesManager.getInstance();
    private Context mContext;
    private boolean isAvatarChanged;
    private Toast toast;

    public static Fragment newInstance() {
        FragmentEditProfile fragmentEditProfile = new FragmentEditProfile();
        Bundle args = new Bundle();
        fragmentEditProfile.setArguments(args);
        return fragmentEditProfile;
    }

    @Nullable
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_edit_profile;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        showPhotoProfile();

        toast = Toast.makeText(getActivity(), "For updating your profile, please try to change some information", Toast.LENGTH_LONG);

        if (preferenceManager.getUsername() != null) {
            username.setText(preferenceManager.getUsername());
            about.setText(preferenceManager.getPersonalInfo());
            address.setText(preferenceManager.getAddress());
            etFirstName.setText(preferenceManager.getFirstName());
            etLastName.setText(preferenceManager.getLastName());
            etEmail.setText(preferenceManager.getUserEmail());
            etAddress.setText(preferenceManager.getAddress());
            etInfo.setText(preferenceManager.getPersonalInfo());
        } else {
            username.setText("Your Name");
            about.setText("Write something about");
            address.setText("yourself");
        }
        setupNextFocused();
    }

    private void setupNextFocused() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(etFirstName, InputMethodManager.SHOW_IMPLICIT);
        etFirstName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    etLastName.requestFocus();
                }
                return true;
            }
        });
        etLastName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    etEmail.requestFocus();
                }
                return true;
            }
        });
        etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    etInfo.requestFocus();
                }
                return true;
            }
        });
        etInfo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    etAddress.requestFocus();
                }
                return true;
            }
        });
    }

    @OnClick(R.id.confirm)
    void onConfirmUpdate() {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String email = etEmail.getText().toString();
        String info = etInfo.getText().toString();
        String address = etAddress.getText().toString();

        if (isFieldsChanged(firstName, lastName, email, info, address) || isAvatarChanged()) {
            preferenceManager.setUsername(firstName + " " + lastName);
            preferenceManager.setFirstName(firstName);
            preferenceManager.setLastName(lastName);
            preferenceManager.setUserEmail(email);
            preferenceManager.setPersonalInfo(info);
            preferenceManager.setAddress(address);

            btnConfirm.setEnabled(false);

            User user = new User(firstName, lastName, email, info, address, "avatar.png");
            UpdateUserDataRequest updateUserDataRequest = new UpdateUserDataRequest(preferenceManager.getSessionId(), user);
            SslRequestHandler.getInstance().sendRequest(updateUserDataRequest, UpdateUserDataResponse.class, listener);
        } else {
           if (!toast.getView().isShown()) {
               toast.show();
           }
        }
    }

    private boolean isFieldsChanged(String firstName, String lastName, String email, String info, String address) {
        if (!firstName.equals(preferenceManager.getFirstName())) {
            return true;
        }
        if (!lastName.equals(preferenceManager.getLastName())) {
            return true;
        }
        if (!email.equals(preferenceManager.getUserEmail())) {
            return true;
        }
        if (!info.equals(preferenceManager.getPersonalInfo())) {
            return true;
        }
        if (!address.equals(preferenceManager.getAddress())) {
            return true;
        }
        return false;
    }

    @OnClick(R.id.change_photo)
    public void changePhoto() {
        DialogFragment dialog = PhotoDialog.newInstance(PhotoFragment.PHOTO_TAKEN_ALBUM,
                PhotoFragment.PHOTO_SELECTED_ALBUM);
        dialog.show(getFragmentManager(), null);
    }

    @Override
    public void onPause() {
        super.onPause();
        KeyboardUtils.hideKeyboard(etFirstName);
    }

    public void onEventMainThread(TakenProfilePhotoEvent event) {
//        preferenceManager.setPhotoTmp(event.getPhotoUri());
        showPhotoProfilePreview();
    }

    public void onEventMainThread(SelectedProfilePhotoEvent event) {
        preferenceManager.setPhotoTmp(event.getPhotoUri());
        showPhotoProfilePreview();
    }

    public void onEventMainThread(AvatarChangedEvent event) {
        setAvatarChanged(true);
    }

    public void onEventMainThread(UploadSuccessEvent event) {

        String imageUrl = updateUserResponse.getUserImage().substring(7);
        Log.e("Sub link image ", imageUrl);
        SslRequestHandler.getInstance().sendRequest(new ConvertImageRequest(imageUrl), Response.class, new SslRequestHandler.SslRequestListener<Response>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(Response response) {
                Log.e("Response convert result ", response.getRequestResult());
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                if (FragmentEditProfile.this.isVisible()) {
                    btnConfirm.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                    ImageLoader.getInstance().clearMemoryCache();
                    preferenceManager.setPhotoUrl(String.valueOf(preferenceManager.getPhotoTmp()));
                    getActivity().onBackPressed();
                    EventBus.getDefault().post(new RefreshProfileFragmentEvent());
                }
            }
        });

    }

    private boolean isAvatarChanged() {
        return isAvatarChanged;
    }

    private void setAvatarChanged(boolean isChanged) {
        isAvatarChanged = isChanged;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private void showPhotoProfile() {
        if (!TextUtils.isEmpty(preferenceManager.getPhotoUrl())) {
            Picasso.with(mContext)
                    .load(preferenceManager.getPhotoUrl())
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .resize(86, 86)
                    .placeholder(R.drawable.ic_photo_placeholder)
                    .into(photo);
//            Picasso.with(mContext)
//                    .load(preferenceManager.getPhotoUrl())
//                    .resize(86, 86)
//                    .placeholder(R.drawable.ic_photo_placeholder)
//                    .into(photo);
        } else {
            photo.setImageResource(R.drawable.ic_photo_placeholder);
        }
    }

    private void showPhotoProfilePreview() {
//        Picasso.with(mContext)
//                .load(preferenceManager.getPhotoTmp())
//                .resize(86, 86)
//                .placeholder(R.drawable.ic_photo_placeholder)
//                .into(photo);
        Picasso.with(mContext)
                .load(preferenceManager.getPhotoTmp())
                .resize(86, 86)
                .placeholder(R.drawable.ic_photo_placeholder)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(photo);
    }

    @Override
    public void updateToolbar() {
        ((MainActivity) getActivity()).setActionBarTitle("Edit profile", true);
    }

    private SslRequestHandler.SslRequestListener listener = new SslRequestHandler.SslRequestListener<UpdateUserDataResponse>() {

        @Override
        public void onStart() {
            if (FragmentEditProfile.this.isVisible()) {
                progressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onSuccess(UpdateUserDataResponse response) {
            updateUserResponse = response;
        }

        @Override
        public void onFailure(String errorMsg) {
            Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFinish() {
            if (updateUserResponse != null) {
                String urlToUpload = UploadUtils.BASE_URL + updateUserResponse.getUserImage();
                String filePath = String.valueOf(PathUtil.getPath(getActivity(), preferenceManager.getPhotoTmp()));
//                preferenceManager.setPhotoUrl(String.valueOf(preferenceManager.getPhotoTmp()));
                UploadUtils.uploadImage(urlToUpload, filePath);
            }
        }
    };
}
