package com.mockup.kscope.kscope.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;


public class PreferencesManager {

    private static final String PREF_NAME = "settings";

    private static final String TOKEN = "token";
    private static final String TOKEN_TYPE = "token_type";
    private static final String SESSION_ID = "session";
    private static final String USERNAME = "username";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String USER_LOGIN = "user_login";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_ID = "user_id";
    private static final String PERSONAL_INFO = "persona_info";
    private static final String USER_ADDRESS = "user_address";
    private static final String USER_PHOTO = "user_photo";
    private static final String USER_PHOTO_URL = "user_photo_url";
    private static final String LAST_FRIENDS_UPDATE_TIME = "last_friends_update_time";
    private static final String LAST_MESSAGES_UPDATE_TIME = "last_messages_update_time";
    private static final String LAST_LIKES_UPDATE_TIME = "last_likes_update_time";
    private static final String LAST_COMMUNITIES_UPDATE_TIME = "last_communities_update_time";
    private static final String PHOTO_ADD_NEW_ITEM = "photo_add_new_item";
    private static PreferencesManager sInstance;
    private final SharedPreferences mPref;


    private PreferencesManager(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PreferencesManager(context);
        }
    }

    public static synchronized PreferencesManager getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(PreferencesManager.class.getSimpleName() +
                    " is not initialized, call initializeTaskInstance(..) method first.");
        }
        return sInstance;
    }

    public SharedPreferences getPreferences() {
        return mPref;
    }

    public void remove(String key) {
        mPref.edit()
                .remove(key)
                .commit();
    }

    public boolean clear() {
        return mPref.edit()
                .clear()
                .commit();
    }

    public String getSessionId() {
        return mPref.getString(SESSION_ID, null);
    }

    public void setSessionId(String sessionId) {
        mPref.edit()
                .putString(SESSION_ID, sessionId)
                .commit();
    }

    public String getUsername() {
        return mPref.getString(USERNAME, null);
    }

    public String getUserLogin() {
        return mPref.getString(USER_LOGIN, null);
    }

    public void setUserLogin(String userLogin) {
        mPref.edit()
                .putString(USER_LOGIN, userLogin)
                .commit();
    }

    public void setUserEmail(String email) {
        mPref.edit()
                .putString(USER_EMAIL, email)
                .commit();
    }

    public String getUserEmail() {
        return mPref.getString(USER_EMAIL, null);
    }

    public void setFirstName(String firstName) {
        mPref.edit()
                .putString(FIRST_NAME, firstName)
                .commit();
    }

    public String getFirstName() {
        return mPref.getString(FIRST_NAME, null);
    }

    public void setLastName(String lastName) {
        mPref.edit()
                .putString(LAST_NAME, lastName)
                .commit();
    }

    public String getLastName() {
        return mPref.getString(LAST_NAME, null);
    }

    public void setUsername(String username) {
        mPref.edit()
                .putString(USERNAME, username)
                .commit();
    }

    public String getPersonalInfo() {
        return mPref.getString(PERSONAL_INFO, null);
    }

    public void setPersonalInfo(String username) {
        mPref.edit()
                .putString(PERSONAL_INFO, username)
                .commit();
    }

    public String getAddress() {
        return mPref.getString(USER_ADDRESS, null);
    }

    public void setAddress(String username) {
        mPref.edit()
                .putString(USER_ADDRESS, username)
                .commit();
    }


    public String getUserId() {
        return mPref.getString(USER_ID, null);
    }

    public void setUserId(String userId) {
        mPref.edit()
                .putString(USER_ID, userId)
                .commit();
    }

    public Uri getPhotoTmp() {
        return Uri.parse(mPref.getString(USER_PHOTO, ""));
    }

    public void setPhotoUrl(String photoUrl) {
        mPref.edit()
                .putString(USER_PHOTO_URL, photoUrl.toString())
                .commit();
    }

    public String getPhotoUrl() {
        return mPref.getString(USER_PHOTO_URL, "");
    }

    public void setPhotoTmp(Uri photoTmp) {
        mPref.edit()
                .putString(USER_PHOTO, photoTmp.toString())
                .commit();
    }

    public long getLastFriendsUpdateTime() {
        return mPref.getLong(LAST_FRIENDS_UPDATE_TIME, 0);
    }

    public void setLastFriendsUpdateTime(long lastUpdateTime) {
        mPref.edit()
                .putLong(LAST_FRIENDS_UPDATE_TIME, lastUpdateTime)
                .commit();
    }

    public void setPhotoAddNewItem(Uri photoTmp){
        mPref.edit()
                .putString(PHOTO_ADD_NEW_ITEM, photoTmp.toString())
                .commit();
    }

    public Uri getAddNewItemPhotoTmp() {
        return Uri.parse(mPref.getString(PHOTO_ADD_NEW_ITEM, ""));
    }


}
