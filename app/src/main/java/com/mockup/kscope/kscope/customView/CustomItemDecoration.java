package com.mockup.kscope.kscope.customView;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class CustomItemDecoration extends RecyclerView.ItemDecoration {

    private int mSizeGridSpacingPx;
    private int mGridSize = 3;

    public CustomItemDecoration(int gridSpacingPx) {
        mSizeGridSpacingPx = gridSpacingPx;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int singleSpace;
        singleSpace = mSizeGridSpacingPx % 2 == 0 ? mSizeGridSpacingPx / 2 : (mSizeGridSpacingPx + 1) / 2;
        int itemPosition = ((RecyclerView.LayoutParams) view.getLayoutParams()).getViewPosition();
        if (itemPosition < mGridSize) {
            outRect.top = 0;
        } else {
            outRect.top = singleSpace * 2;
        }
        outRect.right = singleSpace;
        outRect.left = singleSpace;
        outRect.bottom = 0;
    }
}
