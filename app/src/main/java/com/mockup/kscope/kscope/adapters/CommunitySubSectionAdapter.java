package com.mockup.kscope.kscope.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Stafiiyevskyi on 30.10.2015.
 */
public class CommunitySubSectionAdapter extends ArrayAdapter<ItemCommunities> {

    private final static int RESOURCE = R.layout.item_community_list;

    private LayoutInflater inflater;
    private List<ItemCommunities> items;
    private Context context;
    public CommunitySubSectionAdapter(Context context, List<ItemCommunities> items) {
        super(context, RESOURCE, items);
        this.items = items;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (items!=null){
            return items.size();
        } else return 0;

    }

    @Override
    public ItemCommunities getItem(int position) {
        return items.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        View item = convertView;

        if (item == null) {

            item = inflater.inflate(RESOURCE, parent, false);
            holder = new ViewHolder();
            holder.tvName = (TextView) item.findViewById(R.id.tv_community_name);
            holder.image = (CircleImageView)item.findViewById(R.id.iv_community_logo);
            item.setTag(holder);
        } else {
            holder = (ViewHolder) item.getTag();
        }

        final ItemCommunities menuItem = getItem(position);


        holder.tvName.setText(menuItem.getName());
        String urlImage = menuItem.getLogo();
        if (urlImage != null) {
            if (!urlImage.isEmpty()) {
                if (urlImage.startsWith("http://") || urlImage.startsWith("https://")) {
                    Picasso.with(context).load(urlImage).into(holder.image);

                } else if (urlImage.startsWith("/")) {
                    Picasso.with(context).load(SslRequestHandler.IMAGE_URL_PREFIX + urlImage).into(holder.image);

                } else {
                    Picasso.with(context).load(R.drawable.bg_calendar_selected).into(holder.image);
                }
            } else {
                Picasso.with(context).load(R.drawable.bg_calendar_selected).into(holder.image);
            }

        }

        return item;
    }

    static class ViewHolder {
        private TextView tvName;
        public CircleImageView image;
    }

}