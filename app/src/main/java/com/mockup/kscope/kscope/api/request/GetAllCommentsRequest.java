package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 10.11.2015.
 */
public class GetAllCommentsRequest extends Request{
    private GetCommunity request_param;

    public GetAllCommentsRequest(String guidCommunity){
        setRequestCmd("get_comment");
        request_param = new GetCommunity();
        request_param.guid_item = guidCommunity;
    }

    public GetAllCommentsRequest(String guidCommunity, String dataLimit, String dataOffset){
        setRequestCmd("get_comment");
        request_param = new GetCommunity();
        request_param.guid_item = guidCommunity;
        request_param.data_limit = dataLimit;
        request_param.data_offset = dataOffset;
    }


    private class GetCommunity {
        @SuppressWarnings("unused")
        private String guid_item;
        @SuppressWarnings("unused")
        private String data_limit;
        @SuppressWarnings("unused")
        private String data_offset;
    }
}
