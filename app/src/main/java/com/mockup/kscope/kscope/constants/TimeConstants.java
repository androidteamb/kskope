package com.mockup.kscope.kscope.constants;

/**
 * Created by mpodolsky on 04.12.2015.
 */
public interface TimeConstants {

    public static final long SECOND = 1000;
    public static final long TEN_SECONDS = 10 * 1000;
    public static final long TWENTY_SECONDS = 20 * 1000;
    public static final long MINUTE = 60 * 1000;
    public static final long TEN_MINUTES = 10 * 60 * 1000;
    public static final long HOUR = 60 * 60 * 1000;

}
