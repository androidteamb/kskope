package com.mockup.kscope.kscope.api.response;

/**
 * Created by Alexander Rubanskiy on 30.10.2015.
 */

public class Response {

    public String request_cmd;
    public String request_result;
    public String session_id;

    public int getDataCount() {
        return -1;
    }

    public int getDataLimit() {
        return -1;
    }

    public int getDataOffset() {
        return -1;
    }

    public String getRequestCmd() {
        return request_cmd;
    }

    public void setRequestCmd(String request_cmd) {
        this.request_cmd = request_cmd;
    }

    public String getRequestResult() {
        return request_result;
    }

    public void setRequestResult(String request_result) {
        this.request_result = request_result;
    }

    public boolean isValid() {
        return getRequestResult().equals("1");
    }

    public String getSessionId() {
        return session_id;
    }

    public void setSessionId(String session_id) {
        this.session_id = session_id;
    }

}
