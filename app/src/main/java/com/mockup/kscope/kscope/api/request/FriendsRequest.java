package com.mockup.kscope.kscope.api.request;

/**
 * Created by Serhii_Slobodianiuk on 02.11.2015.
 */
public class FriendsRequest extends Request {

    private Friends request_param;

    public FriendsRequest(String sessionId, String type, String notFull) {
        setRequestCmd("get_contact");
        setSessionId(sessionId);
        request_param = new Friends();
        request_param.sort_type = type;
        request_param.not_full = notFull;
    }

     private class Friends {
        @SuppressWarnings("unused")
        private String sort_type;
        private String not_full;
    }
}
