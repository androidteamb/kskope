package com.mockup.kscope.kscope.api.request;

/**
 * Created by Serhii_Slobodianiuk on 11.11.2015.
 */
public class LogOutRequest extends Request {
    public LogOutRequest(String sessionId) {
        setRequestCmd("user_logout");
        setSessionId(sessionId);
    }
}
