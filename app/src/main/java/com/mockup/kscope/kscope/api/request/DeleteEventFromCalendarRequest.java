package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 26.11.2015.
 */
public class DeleteEventFromCalendarRequest extends Request {

    private DeleteEventFromCalendar request_param;

    public DeleteEventFromCalendarRequest(String sessionId, String guid_item, String time){
        setRequestCmd("del_item_from_calendar");
        setSessionId(sessionId);
        request_param = new DeleteEventFromCalendar();
        request_param.guid_item = guid_item;
        request_param.time = time;
    }

    private class DeleteEventFromCalendar {
        @SuppressWarnings("unused")
        private String guid_item;
        private String time;
    }

}