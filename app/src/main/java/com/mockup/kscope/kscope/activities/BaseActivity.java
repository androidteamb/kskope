package com.mockup.kscope.kscope.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.customView.TypefaceTextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;

/**
 * Created by MPODOLSKY on 12.06.2015.
 */

abstract class BaseActivity extends AppCompatActivity {

    @Optional
    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        ButterKnife.inject(this);
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    protected void setActionBarIcon(int iconRes) {
        toolbar.setNavigationIcon(iconRes);
    }

    protected void setActionBarTitle(int titleRes) {
        toolbar.setTitle(titleRes);
    }

    public void setActionBarTitle(String title) {
        toolbar.setTitle(title);
    }

    public void setActionBarTitle(String title, boolean flag) {
        ((TypefaceTextView) toolbar.findViewById(R.id.toolbarText)).setText(title);
    }

    public void setActionBarTitle(int idRes, boolean flag) {
        ((TypefaceTextView) toolbar.findViewById(R.id.toolbarText)).setText(idRes);
    }

    protected abstract int getLayoutResource();

}
