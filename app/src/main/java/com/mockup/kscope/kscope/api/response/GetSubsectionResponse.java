package com.mockup.kscope.kscope.api.response;

import com.mockup.kscope.kscope.entity.ItemCommunities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stafiiyevskyi on 05.11.2015.
 */
public class GetSubsectionResponse extends Response {

    private SubsectionRequestParam request_param = new SubsectionRequestParam();

    public List<ItemCommunities> getSub_section(){
        return request_param.sub_section;
    }

    public SubsectionRequestParam getRequestParam() {
        return request_param;
    }

    public static class SubsectionRequestParam extends InvalidParam {

        private String data_count;
        private String data_limit;
        private String data_offset;
        private ArrayList<ItemCommunities> sub_section;

        public String getDataCount() {
            return data_count;
        }

        public String getDataLimit() {
            return data_limit;
        }

        public String getDataOffset() {
            return data_offset;
        }

        public ArrayList<ItemCommunities> getSub_section(){
            return sub_section;
        }
    }

}