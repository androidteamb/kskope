package com.mockup.kscope.kscope.entity;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */

public class ItemMessage {

    public String first_name;
    public String second_name;
    public String user_login;
    public String id_reciever;
    public String message_status;
    public String message_text;
    public String message_time;
    public String guid_message;
    public String id_sender;
    public String user_image;

    public String getFirstName() {
        return first_name;
    }

    public String getSecondName() {
        return second_name;
    }

    public String getUserLogin() {
        return user_login;
    }

    public String getIdReceiver() {
        return id_reciever;
    }

    public String getMessageStatus() {
        return message_status;
    }

    public String getMessageText() {
        return message_text;
    }

    public String getMessageTime() {
        return message_time;
    }

    public String getGuidMessage() {
        return guid_message;
    }

    public String getIdSender() {
        return id_sender;
    }

    public String getUserImage() {
        return user_image;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }

    public void setUser_login(String user_login) {
        this.user_login = user_login;
    }

    public void setId_reciever(String id_reciever) {
        this.id_reciever = id_reciever;
    }

    public void setMessage_status(String message_status) {
        this.message_status = message_status;
    }

    public void setMessage_text(String message_text) {
        this.message_text = message_text;
    }

    public void setMessage_time(String message_time) {
        this.message_time = message_time;
    }

    public void setGuid_message(String guid_message) {
        this.guid_message = guid_message;
    }

    public void setId_sender(String id_sender) {
        this.id_sender = id_sender;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }
}
