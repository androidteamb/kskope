package com.mockup.kscope.kscope.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.mockup.kscope.kscope.R;

public class NetworkStateDialog extends DialogFragment {

    private static final String ARG_TITLE = "ARG_TITLE";
    private static final String ARG_MESSAGE = "ARG_MESSAGE";
    private static final String ARG_CONFIRM = "ARG_CONFIRM";

    private DialogCallback callback;

    public static NetworkStateDialog newInstance() {
        NetworkStateDialog dialog = new NetworkStateDialog();
//        Bundle args = new Bundle();
//        args.putInt(ARG_TITLE, title);
//        args.putInt(ARG_MESSAGE, message);
//        args.putInt(ARG_CONFIRM, confirm);
//        dialog.setArguments(args);
        return dialog;
    }

    public NetworkStateDialog() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        int title = R.string.dialog_network_state;
        int message = R.string.dialog_network_state_message;
        int confirm = R.string.dialog_info_confirm;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setNeutralButton(confirm,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (callback != null) {
                            callback.confirm();
                        }
                        dismiss();
                    }
                });
        return builder.create();
    }

    public DialogCallback getCallback() {
        return callback;
    }

    public void setCallback(DialogCallback callback) {
        this.callback = callback;
    }
}
