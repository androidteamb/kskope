package com.mockup.kscope.kscope.constants;

/**
 * Created by mpodolsky on 09.12.2015.
 */
public interface StatusConstants {

    public static final String MESSAGE_READ = "1";
    public static final String MESSAGE_UNREAD = "0";

}
