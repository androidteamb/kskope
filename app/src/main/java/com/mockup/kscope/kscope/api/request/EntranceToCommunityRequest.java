package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 06.11.2015.
 */
public class EntranceToCommunityRequest extends Request{
    private EntranceToCommunity request_param;

    public EntranceToCommunityRequest(String guidCommunity, String sessionId){
        setRequestCmd("entrance_to_community");
        setSessionId(sessionId);
        request_param = new EntranceToCommunity();
        request_param.guid_community = guidCommunity;
    }


    private class EntranceToCommunity {
        @SuppressWarnings("unused")
        private String guid_community;
    }
}
