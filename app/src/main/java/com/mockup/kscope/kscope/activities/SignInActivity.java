package com.mockup.kscope.kscope.activities;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.PlusShare;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.LoginRequest;
import com.mockup.kscope.kscope.api.response.LoginResponse;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.utils.TypefaceManager;
import com.mockup.kscope.kscope.utils.Utils;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

public class SignInActivity extends BaseActivity implements Validator.ValidationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int PICK_MEDIA_REQUEST_CODE = 8;
    private static final int SHARE_MEDIA_REQUEST_CODE = 9;
    private static final int SIGN_IN_REQUEST_CODE = 10;
    private static final int ERROR_DIALOG_REQUEST_CODE = 11;
    private static final int TWITTER_REQUEST_CODE = 140;

    @NotEmpty
    @InjectView(R.id.username)
    EditText username;
    @NotEmpty
    @InjectView(R.id.password)
    EditText password;
    @InjectView(R.id.signin)
    Button signin;
    @InjectView(R.id.gplus)
    ImageView gplus;
    Validator validator;
    private GoogleApiClient mGoogleApiClient;
    private boolean mSignInClicked;
    private boolean mIntentInProgress;
    private ConnectionResult mConnectionResult;
    private boolean isFromStop = false;
    //    @InjectView(R.id.facebook)
    private LoginButton facebookLogin;

    private PreferencesManager preferencesManager = PreferencesManager.getInstance();
    private CallbackManager callbackManager;
    private TwitterAuthClient mTwitterAuthClient;
    private ProgressDialog progressDialog;

    public static void launch(Context context) {
        Intent i = new Intent(context, SignInActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        validator = new Validator(this);
        validator.setValidationListener(this);

        setTypefaceElements();
        setupFacebookLogin();

        mGoogleApiClient = buildGoogleAPIClient();
        processSignOut();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // make sure to initiate connection
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // disconnect api if it is connected
        isFromStop = true;
        if (mGoogleApiClient.isConnected()) {
            processSignOut();
        }
        stopProgressDialog();
    }


    @OnClick(R.id.gplus)
    public void signInGPlus() {
        if (!mGoogleApiClient.isConnected()) {
            startProgressDialog();
            processSignIn();
        } else {
            processSignOut();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopProgressDialog();
    }

    private void startProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please wait.");
        progressDialog.show();
    }

    private void stopProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private GoogleApiClient buildGoogleAPIClient() {
        return new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
    }

    private void setupTwitterLogin() {
        mTwitterAuthClient = new TwitterAuthClient();
        mTwitterAuthClient.authorize(this, new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> twitterSessionResult) {
                Log.d("Twitter", "Logged with twitter");
                TwitterSession session = twitterSessionResult.data;
                Log.d("Twitter", session.getUserName());

            }

            @Override
            public void failure(com.twitter.sdk.android.core.TwitterException e) {
                Log.e("Twitter", "Failed login with twitter");
                e.printStackTrace();
            }
        });
    }

    private void setupFacebookLogin() {
        facebookLogin = new LoginButton(this);
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {

            }
        });

    }
    // 64206

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 64206) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == 140) {
            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == SIGN_IN_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else if (requestCode == PICK_MEDIA_REQUEST_CODE) {
            // If picking media is success, create share post using
            // PlusShare.Builder
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                ContentResolver cr = this.getContentResolver();
                String mime = cr.getType(selectedImage);

                PlusShare.Builder share = new PlusShare.Builder(this);
                share.setText("Hello from AndroidSRC.net");
                share.addStream(selectedImage);
                share.setType(mime);
                startActivityForResult(share.getIntent(),
                        SHARE_MEDIA_REQUEST_CODE);
            }
        }

    }

    private void processRevokeRequest() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                    .setResultCallback(new ResultCallback() {
                        @Override
                        public void onResult(com.google.android.gms.common.api.Result result) {
                            Toast.makeText(getApplicationContext(),
                                    "User permissions revoked",
                                    Toast.LENGTH_LONG).show();
                            mGoogleApiClient = buildGoogleAPIClient();
                            mGoogleApiClient.connect();
                        }

                    });
        }

    }

    private void processShareMedia() {
        Intent photoPicker = new Intent(Intent.ACTION_PICK);
        photoPicker.setType("video/*, image/*");
        startActivityForResult(photoPicker, PICK_MEDIA_REQUEST_CODE);
    }

    /**
     * API to process post share request Use PlusShare.Builder to create share
     * post.
     */
    private void processSharePost() {
        // Launch the Google+ share dialog with attribution to your app.
        Intent shareIntent = new PlusShare.Builder(this).setType("text/plain")
                .setText("Google+ Demo http://androidsrc.net")
                .setContentUrl(Uri.parse("http://androidsrc.net")).getIntent();

        startActivityForResult(shareIntent, 0);
    }

    /**
     * API to handle sign out of user
     */
    private void processSignOut() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            if (!isFromStop) {
                Toast.makeText(getApplicationContext(), "Signed Out Successfully",
                        Toast.LENGTH_LONG).show();
            }

        }
    }

    /**
     * API to handler sign in of user If error occurs while connecting process
     * it in processSignInError() api
     */
    private void processSignIn() {

        if (!mGoogleApiClient.isConnecting()) {
            processSignInError();
            mSignInClicked = true;
        }

    }

    /**
     * API to process sign in error Handle error based on ConnectionResult
     */
    private void processSignInError() {
        if (mConnectionResult != null && mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this,
                        SIGN_IN_REQUEST_CODE);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    ERROR_DIALOG_REQUEST_CODE).show();
            return;
        }
        if (!mIntentInProgress) {
            mConnectionResult = result;
            if (mSignInClicked) {
                processSignInError();
            }
        }
    }

    /**
     * Callback for GoogleApiClient connection success
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        mSignInClicked = false;
        stopProgressDialog();
        Toast.makeText(getApplicationContext(), "Signed In Successfully",
                Toast.LENGTH_LONG).show();
    }

    /**
     * Callback for suspension of current connection
     */
    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
    }

    private void setTypefaceElements() {
        username.setTypeface(TypefaceManager.obtainTypeface(this, TypefaceManager.RALEWAY_REGULAR));
        password.setTypeface(TypefaceManager.obtainTypeface(this, TypefaceManager.RALEWAY_REGULAR));
        signin.setTypeface(TypefaceManager.obtainTypeface(this, TypefaceManager.RALEWAY_BOLD));

//        username.setText("userlogin1");
//        password.setText("password");
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_signin;
    }

    @OnClick(R.id.twitter)
    public void twitter() {
        setupTwitterLogin();
    }

    @OnClick(R.id.facebook)
    public void facebook() {
        facebookLogin.performClick();
    }

    @OnClick(R.id.signin)
    public void login() {
        if (Utils.isNetworkConnected(this)) {
            validator.validate();
        } else {
            Toast.makeText(this, "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onValidationSucceeded() {

        LoginRequest loginRequest = new LoginRequest(username.getText().toString(), password.getText().toString());
        SslRequestHandler.getInstance().sendRequest(loginRequest, LoginResponse.class, new SslRequestHandler.SslRequestListener<LoginResponse>() {

            @Override
            public void onStart() {
                startProgressDialog();
            }

            @Override
            public void onSuccess(final LoginResponse response) {
                Log.i("Response login ", response.toString());

                if (Integer.parseInt(response.getRequestResult()) != 0) {
                    preferencesManager.setSessionId(response.getSessionId());
                    preferencesManager.setUserId(response.getIdUser());
                    preferencesManager.setUserLogin(response.getUserLogin());

                    MainActivity.launch(SignInActivity.this);
                } else {

                    SignInActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (response.getRequestParam().getDetailErrorMessage()!=null){
                                Toast.makeText(SignInActivity.this,
                                        response.getRequestParam().getDetailErrorMessage(),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(SignInActivity.this,
                                        response.getRequestParam().getErrorMessage(),
                                        Toast.LENGTH_LONG).show();
                            }

                        }
                    });
                }
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                stopProgressDialog();
            }
        });

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

}
