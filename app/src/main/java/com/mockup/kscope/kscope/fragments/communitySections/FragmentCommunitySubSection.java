package com.mockup.kscope.kscope.fragments.communitySections;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.CommunitySubSectionAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.GetSubsectionRequest;
import com.mockup.kscope.kscope.api.response.GetSubsectionResponse;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Stafiiyevskyi on 30.10.2015.
 */
public class FragmentCommunitySubSection extends BaseFragment implements AdapterView.OnItemClickListener {
    @InjectView(R.id.communities_list)
    ListView lvCommunities;
    @InjectView(R.id.progress)
    ProgressBar progressBar;
    @InjectView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @InjectView(R.id.empty)
    LinearLayout emptyView;
    private CommunitySubSectionAdapter adapter;
    private String title;
    private String guidSection;
    private SslRequestHandler.SslRequestListener listener;



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateToolbar();
        title = getArguments().getString(MainActivity.TITLE_KEY);
        guidSection = getArguments().getString(MainActivity.SECTION_GUID);
        ((MainActivity)getActivity()).setActionBarTitle(title, true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getData();
            }
        });

        initListener();
        getData();

    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        ((MainActivity)getActivity()).setActionBarTitle(title, true);
    }

    private void initListOfSubsection(List<ItemCommunities> data){
        lvCommunities.setOnItemClickListener(this);
        adapter = new CommunitySubSectionAdapter(getActivity(),data);
        lvCommunities.setAdapter(adapter);
    }

    private void initListener(){
        listener = new SslRequestHandler.SslRequestListener<GetSubsectionResponse>() {
            List<ItemCommunities> data;
            @Override
            public void onStart() {
                if (!swipeRefreshLayout.isRefreshing()){
                    progressBar.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onSuccess(GetSubsectionResponse response) {
                data = response.getSub_section();
                Log.i("SubSectionRequest", response.getRequestParam().getSub_section().toString());
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                if (FragmentCommunitySubSection.this.isVisible()){
                    swipeRefreshLayout.setRefreshing(false);
                    progressBar.setVisibility(View.INVISIBLE);
                    if (data == null) {
                        emptyView.setVisibility(View.VISIBLE);
                    } else {
                        initListOfSubsection(data);
                        emptyView.setVisibility(View.INVISIBLE);
                    }
                }

            }
        };
    }


    @Override
    public int getLayoutResource() {
        return R.layout.fragment_communities;
    }

    @Override
    public void updateToolbar() {
        ((MainActivity)getActivity()).setActionBarTitle(title, true);
        Toolbar toolbar = ButterKnife.findById(getActivity(), R.id.toolbar);
        LinearLayout message = ButterKnife.findById(toolbar, R.id.menu_messages);
        message.setVisibility(View.GONE);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Fragment fragment = new FragmentCommunitySubSectionItems();
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.TITLE_KEY,((ItemCommunities) parent.getItemAtPosition(position)).getName());
        bundle.putString(MainActivity.SUBSECTION_GUID,((ItemCommunities) parent.getItemAtPosition(position)).getGuid_subsection());
        fragment.setArguments(bundle);
        ((MainActivity) getActivity()).addFragment(fragment, ((ItemCommunities) parent.getItemAtPosition(position)).getName());
    }

    private void getData(){
        GetSubsectionRequest request = new GetSubsectionRequest(guidSection,null);
        SslRequestHandler.getInstance().sendRequest(request, GetSubsectionResponse.class, listener);
    }

}
