package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 25.11.2015.
 */
public class AddCommentRequest extends Request {

    private AddComment request_param;

    public AddCommentRequest(String sessionId, String guid_item, String comments, String time_added){
        setRequestCmd("add_comment");
        setSessionId(sessionId);
        request_param = new AddComment();
        request_param.guid_item = guid_item;
        request_param.comments = comments;
        request_param.time_added = time_added;
    }

    private class AddComment {
        @SuppressWarnings("unused")
        private String guid_item;
        private String time_added;
        private String comments;
    }

}
