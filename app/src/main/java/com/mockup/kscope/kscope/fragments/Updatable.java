package com.mockup.kscope.kscope.fragments;

/**
 * Created by mpodolsky on 09.12.2015.
 */
public interface Updatable {

    void update();

}
