package com.mockup.kscope.kscope.events;

/**
 * Created by Serhii_Slobodianiuk on 26.11.2015.
 */
public class FriendsFilterEvent {

    private String queryText;

    public FriendsFilterEvent(String newText) {
        this.queryText = newText;
    }

    public String getQueryText() {
        return queryText;
    }
}
