package com.mockup.kscope.kscope.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.customView.TypefaceTextView;
import com.mockup.kscope.kscope.entity.ItemCommunities;

import java.util.ArrayList;

/**
 * Created by Stafiiyevskyi on 02.12.2015.
 */
public class FragmentAddNewItemSpinnerAdapter extends ArrayAdapter<String> {
    private Activity activity;
    private ArrayList data;

    ItemCommunities tempValues = null;
    LayoutInflater inflater;

    public FragmentAddNewItemSpinnerAdapter(Activity activitySpinner, int textViewResourceId, ArrayList objects) {
        super(activitySpinner, textViewResourceId, objects);
        activity = activitySpinner;
        data = objects;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }


    public View getCustomView(int position, View convertView, ViewGroup parent) {

        View row = inflater.inflate(R.layout.item_community_spinner, parent, false);

        tempValues = null;
        tempValues = (ItemCommunities) data.get(position);

        TypefaceTextView communityName = (TypefaceTextView) row.findViewById(R.id.tv_community_name);
        communityName.setText(tempValues.getName());

        if (position == 0){
            communityName.setTextColor(getContext().getResources().getColor(R.color.gray));
        }

        return row;
    }
}


