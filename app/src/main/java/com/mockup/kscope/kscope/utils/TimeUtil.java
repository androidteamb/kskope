package com.mockup.kscope.kscope.utils;

import android.support.annotation.NonNull;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by Stafiiyevskyi on 17.11.2015.
 */
public class TimeUtil {

    private TimeUtil() {

    }

    public static String getTimeAddedForComment(String timeAdded) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        String res = "";
        if (!timeAdded.equalsIgnoreCase("")) {
            try {

                long currentTime = Calendar.getInstance().getTimeInMillis();
                long addedTime = dateFormat.parse(timeAdded).getTime();
                long t3 = currentTime - addedTime;

                long msec = t3 % 1000;
                t3 = t3 / 1000;
                long sec = t3 % 60;
                t3 = t3 / 60;
                long min = t3 % 60;
                t3 = t3 / 60;
                long hour = t3 % 24;
                t3 = t3 / 24;
                long day = t3;

                if (day < 1) {

                    if (hour < 1) {

                        return Long.toString(min) + " minutes ago";

                    } else {

                        return Long.toString(hour) + " hour ago";
                    }

                } else {
                    if (day < 5) {

                        if (hour < 5) {

                            return Long.toString(day) + " day ago";

                        } else {

                            return Long.toString(++day) + " day ago";

                        }

                    } else {

                        dateFormat = new SimpleDateFormat("MMM dd yyyy", Locale.ENGLISH);
                        Date date = new Date(addedTime);
                        return dateFormat.format(date);

                    }
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return res;
    }


    public static String getDateFromMilliseconds(long timeAdded, String format) {
        String dateFormatString = "yyyy-MM-dd"; //default
        if (format != null) {
            dateFormatString = format;
        }
        long addedTime = timeAdded;
        SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatString, Locale.ENGLISH);
        Date date = new Date(addedTime);

        return dateFormat.format(date);
    }

    public static String getDateAddedFormatItemCommunity(String dateAdded) {
        if (!dateAdded.equalsIgnoreCase("")) {
            SimpleDateFormat dateInputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            SimpleDateFormat dateOutputFormat = new SimpleDateFormat("HH:mm - dd/MM/yyyy", Locale.ENGLISH);
            try {
                Date inputDate = dateInputFormat.parse(dateAdded);
                return dateOutputFormat.format(inputDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return "";
    }

    public static String getAddCommentTimeFromDateObject(Date date) {
        SimpleDateFormat dateOutFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

        return dateOutFormat.format(date);
    }

    public static String getNameDayTime(String dateStr) {
        SimpleDateFormat dateOutFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date date;
        try {
            date = dateOutFormat.parse(dateStr);
            dateOutFormat = new SimpleDateFormat("EEEE h:mm a", Locale.ENGLISH);
            return dateOutFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getNumberDay(String dateStr) {
        SimpleDateFormat dateOutFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date date;
        try {
            date = dateOutFormat.parse(dateStr);
            dateOutFormat = new SimpleDateFormat("d", Locale.ENGLISH);
            return dateOutFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * @param dateAdded        string date
     * @param inputDateFormat  pattern of dateAdded
     * @param outputDateFormat pattern of output date
     * @param locale
     */
    public static String getFormattedDate(@NonNull String dateAdded, @NonNull String inputDateFormat, @NonNull String outputDateFormat, @NonNull Locale locale) {
        if (!dateAdded.equalsIgnoreCase("")) {
            SimpleDateFormat dateInputFormat = new SimpleDateFormat(inputDateFormat, locale);
            SimpleDateFormat dateOutputFormat = new SimpleDateFormat(outputDateFormat, locale);
            try {
                Date inputDate = dateInputFormat.parse(dateAdded);
                return dateOutputFormat.format(inputDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return "";
    }
}
