package com.mockup.kscope.kscope.fragments.main;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.ViewPagerAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.model.User;
import com.mockup.kscope.kscope.api.request.TabsCountRequest;
import com.mockup.kscope.kscope.api.request.UserDataRequest;
import com.mockup.kscope.kscope.api.response.FriendsTabsCountResponse;
import com.mockup.kscope.kscope.api.response.UserDataResponse;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.fragments.tabs.FragmentGallery;
import com.mockup.kscope.kscope.fragments.tabs.FragmentLikes;
import com.mockup.kscope.kscope.fragments.tabs.FragmentTabsCommunities;
import com.mockup.kscope.kscope.utils.NameUtil;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.utils.Utils;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Serhii_Slobodianiuk on 23.11.2015.
 */
public class FragmentFriendProfile extends BaseFragment {

    private static String TITLE_KEY = "title";

    @InjectView(R.id.tabs)
    TabLayout mTabLayout;
    @InjectView(R.id.pager)
    ViewPager mViewPager;

    // Profile header
    @InjectView(R.id.photo)
    CircleImageView photo;
    @InjectView(R.id.username)
    TextView username;
    @InjectView(R.id.working)
    TextView about;
    @InjectView(R.id.city)
    TextView address;

    @InjectView(R.id.progress_bar)
    ProgressBar progressBar;

    private LinearLayout sendMessage;

    private String userLogin;

    private Context mContext;
    private Resources mResources;
    private UserDataResponse userData;
    private FriendsTabsCountResponse counts;
    private PreferencesManager preferenceManager = PreferencesManager.getInstance();
    private SslRequestHandler requestHandler = SslRequestHandler.getInstance();

    public static Fragment newInstance(String userLogin) {
        FragmentFriendProfile fragmentProfile = new FragmentFriendProfile();
        Bundle args = new Bundle();
        args.putString(TITLE_KEY, userLogin);
        fragmentProfile.setArguments(args);
        return fragmentProfile;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mResources = mContext.getResources();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userLogin = getArguments().getString(TITLE_KEY);

        sendRequest();
        setupToolbarMenu();
        updateToolbar();
    }

    private void setupViewPager(ViewPager viewPager) {
        if (viewPager != null) {
            final ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
            adapter.addFragment(FragmentGallery.newInstance(userData.getUserData().get(0).getId_user()), mResources.getString(R.string.gallery));
            adapter.addFragment(FragmentLikes.newInstance(userData.getUserData().get(0).getId_user()), mResources.getString(R.string.likes));
            adapter.addFragment(FragmentTabsCommunities.newInstance(userData.getUserData().get(0).getId_user()), mResources.getString(R.string.communities));
            viewPager.setAdapter(adapter);
            viewPager.setOffscreenPageLimit(2);
        }
    }

    private void setupCustomTab(Drawable icon, String count, String title, final int index) {
        final LinearLayout tabView = (LinearLayout) inflateCustomTabLayout();
        ImageView iconView = (ImageView) tabView.findViewById(R.id.icon_tab);
        iconView.setImageDrawable(icon);

        TextView countView = (TextView) tabView.findViewById(R.id.count_tab);
        countView.setText(count);

        TextView titleView = (TextView) tabView.findViewById(R.id.title_tab);
        titleView.setText(title);

        if (index == 0) {
            tabView.setSelected(true);
        }
        mTabLayout.getTabAt(index).setCustomView(tabView);
    }

    private void sendRequest() {
        if (Utils.isNetworkConnected(mContext)) {
            UserDataRequest userDataRequest = new UserDataRequest(preferenceManager.getSessionId(), userLogin);
            requestHandler.sendRequest(userDataRequest, UserDataResponse.class, profileRequestListener);
        } else {
            Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
        }
    }

    private void updateTabCount() {
        if (counts != null) {
            TextView gallery_count = (TextView) mTabLayout.getTabAt(0).getCustomView().findViewById(R.id.count_tab);
            gallery_count.setText(counts.getRequestParam().getCountGallery());
            TextView likes_count = (TextView) mTabLayout.getTabAt(1).getCustomView().findViewById(R.id.count_tab);
            likes_count.setText(counts.getRequestParam().getCountLikes());
            TextView community_count = (TextView) mTabLayout.getTabAt(2).getCustomView().findViewById(R.id.count_tab);
            community_count.setText(counts.getRequestParam().getCountCommunities());
        }
    }

    private void showPhotoProfile(String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(mContext)
                    .load(imageUrl)
                    .error(R.drawable.ic_photo_placeholder)
                    .resize(86, 86)
                    .placeholder(R.drawable.ic_photo_placeholder)
                    .into(photo);
        } else {
            photo.setImageResource(R.drawable.ic_photo_placeholder);
        }
        photo.setVisibility(VISIBLE);
    }

    private void setupToolbarMenu() {
        Toolbar toolbar = ButterKnife.findById(getActivity(), R.id.toolbar);
        LinearLayout message = ButterKnife.findById(toolbar, R.id.menu_messages);
        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userData != null) {
                    User user = userData.getUserData().get(0);
                    String receiverID = user.getId_user();
                    ((MainActivity) getActivity()).addFragment(
                            FragmentSendMessage.newInstance(
                                    NameUtil.getName(user.getFirst_name(), user.getSecond_name(), userLogin),
                                    receiverID),
                            NameUtil.getName(user.getFirst_name(), user.getSecond_name(), user.getUser_login()) + "Message");
                } else {
                    Toast.makeText(getActivity(), "Please, wait for loading user data...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_friends_profile;
    }

    private View inflateCustomTabLayout() {
        return LayoutInflater.from(mContext).inflate(R.layout.layout_custom_tab, null);
    }

    @Override
    public void updateToolbar() {
        ((MainActivity) getActivity()).setActionBarTitle(userLogin, true);
        Toolbar toolbar = ButterKnife.findById(getActivity(), R.id.toolbar);
        LinearLayout message = ButterKnife.findById(toolbar, R.id.menu_messages);
        message.setVisibility(VISIBLE);
    }

    private SslRequestHandler.SslRequestListener profileRequestListener = new SslRequestHandler.SslRequestListener<UserDataResponse>() {

        @Override
        public void onStart() {
            if (progressBar != null) {
                progressBar.setVisibility(VISIBLE);
            }
        }

        @Override
        public void onSuccess(UserDataResponse response) {
            userData = response;

        }

        @Override
        public void onFailure(String errorMsg) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFinish() {
            if (FragmentFriendProfile.this.isVisible()){
                if (progressBar != null) {
                    progressBar.setVisibility(GONE);
                }
                if (userData != null) {
                    showPhotoProfile(userData.getUserData().get(0).getUser_image());
                    setupViewPager(mViewPager);
                    if (mViewPager != null) {
                        mTabLayout.setupWithViewPager(mViewPager);
                        setupCustomTab(mResources.getDrawable(R.drawable.tab_gallery_selector),
                                "", mResources.getString(R.string.gallery), 0);
                        setupCustomTab(mResources.getDrawable(R.drawable.tab_likes_selector),
                                "", mResources.getString(R.string.likes), 1);
                        setupCustomTab(mResources.getDrawable(R.drawable.tab_communities_selector),
                                "", mResources.getString(R.string.communities), 2);
                    }
                    if (Utils.isNetworkConnected(mContext)) {
                        TabsCountRequest tabsCountRequest = new TabsCountRequest(preferenceManager.getSessionId(), userLogin);
                        requestHandler.sendRequest(tabsCountRequest, FriendsTabsCountResponse.class, countTabsListener);
                    } else {
                        Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }
                if (userData.getRequestResult().equalsIgnoreCase("1")){
                    User user = userData.getUserData().get(0);
                    username.setText(NameUtil.getName(user.getFirst_name(), user.getSecond_name(), userLogin));
                    about.setText(user.getUser_profile());
                    address.setText(user.getStreet_address1());
                }
            }

        }
    };

    private SslRequestHandler.SslRequestListener countTabsListener = new SslRequestHandler.SslRequestListener<FriendsTabsCountResponse>() {

        @Override
        public void onStart() {
        }

        @Override
        public void onSuccess(FriendsTabsCountResponse response) {
            counts = response;
        }

        @Override
        public void onFailure(String errorMsg) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFinish() {
            if (!FragmentFriendProfile.this.isVisible()) {
                return;
            }
            updateTabCount();
        }
    };
}
