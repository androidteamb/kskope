package com.mockup.kscope.kscope.events;

/**
 * Created by Serhii_Slobodianiuk on 26.11.2015.
 */
public class MessagesFilterEvent {

    private String queryText;

    public MessagesFilterEvent(String newText) {
        this.queryText = newText;
    }

    public String getQueryText() {
        return queryText;
    }
}
