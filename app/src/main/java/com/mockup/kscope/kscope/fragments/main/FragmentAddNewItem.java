package com.mockup.kscope.kscope.fragments.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.FragmentAddNewItemSpinnerAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.ConvertImageRequest;
import com.mockup.kscope.kscope.api.response.AddNewItemResponse;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.customView.TypefaceTextView;
import com.mockup.kscope.kscope.dialogs.DialogFragmentDatePicker;
import com.mockup.kscope.kscope.dialogs.DialogFragmentDatePicker.DatePickerListener;
import com.mockup.kscope.kscope.dialogs.DialogFragmentTimePicker;
import com.mockup.kscope.kscope.dialogs.PhotoDialog;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.mockup.kscope.kscope.events.NewItemPhotoSelectedEvent;
import com.mockup.kscope.kscope.events.NewItemTakePhotoEvent;
import com.mockup.kscope.kscope.events.RefreshProfileFragmentEvent;
import com.mockup.kscope.kscope.events.UploadSuccessEvent;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.fragments.PhotoFragment;
import com.mockup.kscope.kscope.presenters.FragmentAddNewItemPresenterImpl;
import com.mockup.kscope.kscope.utils.ImageScaleUtils;
import com.mockup.kscope.kscope.utils.KeyboardUtils;
import com.mockup.kscope.kscope.utils.PathUtil;
import com.mockup.kscope.kscope.utils.RealPathUtil;
import com.mockup.kscope.kscope.utils.TimeUtil;
import com.mockup.kscope.kscope.utils.UploadUtils;
import com.mockup.kscope.kscope.viewsPresenter.FragmentAddNewItemView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import static android.view.View.VISIBLE;

/**
 * Created by Stafiiyevskyi on 02.12.2015.
 */
public class FragmentAddNewItem extends BaseFragment implements FragmentAddNewItemView,
        com.mobsandgeeks.saripaar.Validator.ValidationListener, DatePickerListener, DialogFragmentTimePicker.TimePickerListener {

    @NotEmpty
    @InjectView(R.id.item_title_et)
    EditText titleEt;
    @InjectView(R.id.item_date_et)
    TypefaceTextView dateEt;
    @InjectView(R.id.item_time_tv)
    TypefaceTextView timeTV;
    @NotEmpty
    @InjectView(R.id.description_et)
    EditText fullDescriptionEt;
    @InjectView(R.id.mobile_phone_et)
    EditText phoneEt;
    @InjectView(R.id.address_et)
    EditText addressEt;
    @InjectView(R.id.symbol_count_description)
    TypefaceTextView countSymbol;
    @InjectView(R.id.attached_image_iv)
    ImageView attachedImage;
    @InjectView(R.id.attach_image_tv)
    TypefaceTextView attachImageTV;
    @InjectView(R.id.spinner_community_section)
    Spinner communitySectionSpinner;
    @InjectView(R.id.spinner_community_subsection)
    Spinner communitySubsectionSpinner;
    @InjectView(R.id.scrollViewContainer)
    ScrollView scrollView;
    @InjectView(R.id.add_btn)
    Button addBtn;
    @InjectView(R.id.date_container)
    FrameLayout dateContainer;
    @InjectView(R.id.progress)
    ProgressBar progressBar;

    private FragmentAddNewItemPresenterImpl presenter;
    private Validator editTextValidator;

    private ArrayList<ItemCommunities> arrayListSubcommunity = new ArrayList<>();
    private ArrayList<ItemCommunities> arrayListCommunity = new ArrayList<>();
    private FragmentAddNewItemSpinnerAdapter adapterCommunity;
    private FragmentAddNewItemSpinnerAdapter adapterSubCommunity;

    private String selectedGuidCommunity;
    private String selectedGuidSubCommunity;

    private String titleEvent;
    private String descriptionEvent;
    private String dateEvent;
    private String timeEvent;
    private String imagePath;
    private String imagePathServer;
    private String addressEvent;
    private String phoneEvent;


    @Override
    public int getLayoutResource() {
        return R.layout.fragment_add_new_item;
    }

    @Override
    public void updateToolbar() {
        ((MainActivity) getActivity()).setActionBarTitle(R.string.fragment_add_new_item_title, true);
        Toolbar toolbar = ButterKnife.findById((MainActivity) getActivity(), R.id.toolbar);
        LinearLayout addEvent = ButterKnife.findById(toolbar, R.id.menu_add_item_event);
        addEvent.setVisibility(View.GONE);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateToolbar();
        fullDescriptionEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                countSymbol.setVisibility(View.VISIBLE);
                countSymbol.setText(fullDescriptionEt.getText().toString().length() + " / 256");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                countSymbol.setText(fullDescriptionEt.getText().toString().length() + " / 256");
            }

            @Override
            public void afterTextChanged(Editable s) {
                countSymbol.setText(fullDescriptionEt.getText().toString().length() + " / 256");
            }
        });
        setupNextFocused();
        prepareSpinners();
        editTextValidator = new Validator(this);
        editTextValidator.setValidationListener(this);
        presenter = new FragmentAddNewItemPresenterImpl(this);
        progressBar.setVisibility(VISIBLE);
        presenter.getCommunitySections();
    }

    @Override
    public void onPause() {
        super.onPause();
        View view = this.getActivity().getCurrentFocus();
        if (view != null) {
            KeyboardUtils.hideKeyboard(view);
        }


    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
//        EventBus.getDefault().unregister(this);
    }

    private void prepareSpinners() {
        ItemCommunities itemCommunities = new ItemCommunities("Select community");
        arrayListCommunity.add(itemCommunities);

        ItemCommunities itemSubCommunity = new ItemCommunities("Select sub community");
        arrayListSubcommunity.add(itemSubCommunity);

        adapterCommunity = new FragmentAddNewItemSpinnerAdapter(getActivity(), R.layout.item_community_spinner, arrayListCommunity);
        communitySectionSpinner.setAdapter(adapterCommunity);

        adapterSubCommunity = new FragmentAddNewItemSpinnerAdapter(getActivity(), R.layout.item_community_spinner, arrayListSubcommunity);
        communitySubsectionSpinner.setAdapter(adapterSubCommunity);

    }


    @OnClick(R.id.attach_image_tv)
    void attachImageClick() {
        attachedImage.setVisibility(VISIBLE);

        DialogFragment dialog = PhotoDialog.newInstance(PhotoFragment.ADD_NEW_ITEM_PHOTO_TAKEN_ALBUM,
                PhotoFragment.ADD_NEW_ITEM_PHOTO_SELECTED_ALBUM);
        dialog.show(getFragmentManager(), null);


//        Intent intent = new Intent();
//        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        intent.setType("image/*");
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 0);
    }

    @OnClick(R.id.add_btn)
    void addItemClick() {
        editTextValidator.validate();
    }

    @OnClick(R.id.date_container)
    void pickDateClick() {
        View view = this.getActivity().getCurrentFocus();
        if (view != null) {
            KeyboardUtils.hideKeyboard(view);
        }
        DialogFragment dateDialog = DialogFragmentDatePicker.newInstance(this);
        dateDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
    }

    @OnClick(R.id.time_container)
    void pickTimeClick() {
        View view = this.getActivity().getCurrentFocus();
        if (view != null) {
            KeyboardUtils.hideKeyboard(view);
        }
        DialogFragment timeDialog = DialogFragmentTimePicker.newInstance(this);
        timeDialog.show(getActivity().getSupportFragmentManager(), "timePicker");
    }

    public void onEvent(NewItemTakePhotoEvent event) {
        String realPath;
        realPath = RealPathUtil.getPath(getActivity(), event.getPhotoUri());
        Log.i("Real path from event ", realPath);
        imagePath = realPath;
        AsyncImageScale asyncTask = new AsyncImageScale(realPath);
        asyncTask.execute();
    }

    public void onEvent(NewItemPhotoSelectedEvent event) {
        String realPath;
        realPath = RealPathUtil.getPath(getActivity(), event.getPhotoUri());
        imagePath = realPath;
        AsyncImageScale asyncTask = new AsyncImageScale(realPath);
        asyncTask.execute();
    }

    private void setupNextFocused() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(titleEt, InputMethodManager.SHOW_IMPLICIT);
        titleEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    fullDescriptionEt.requestFocus();
                    scrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            scrollView.smoothScrollTo(0, fullDescriptionEt.getTop());
                        }
                    });

                }
                return true;
            }
        });
        fullDescriptionEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    addressEt.requestFocus();
                } else if (actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                    titleEt.requestFocus();
                }
                return true;
            }
        });

        addressEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    phoneEt.requestFocus();
                } else if (actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                    fullDescriptionEt.requestFocus();
                } else if (actionId == EditorInfo.IME_ACTION_DONE) {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        KeyboardUtils.hideKeyboard(view);
                    }
                }
                return true;
            }
        });


    }

    private void setImage(int sdk, String realPath) {

        if (this.isVisible()) {

            Uri uriFromPath = Uri.fromFile(new File(realPath));
            Picasso.with(getActivity()).load(uriFromPath).resize(49, 49).centerCrop().into(attachedImage);

            Log.d("HMKCODE", "Build.VERSION.SDK_INT:" + sdk);

            Log.d("HMKCODE", "Real Path: " + realPath);
        }


    }

    private void setImageSdk20Plus(Uri uri) {
        if (this.isVisible()) {
            Picasso.with(getActivity()).load(uri).resize(80, 80).centerCrop().into(attachedImage);
        }

    }


    void updateSubCommunitySpinnerAdapter() {
        selectedGuidCommunity = null;
        selectedGuidSubCommunity = null;
        arrayListSubcommunity.clear();
        ItemCommunities itemSubCommunity = new ItemCommunities("Select sub community");
        arrayListSubcommunity.add(itemSubCommunity);
        adapterSubCommunity = new FragmentAddNewItemSpinnerAdapter(getActivity(), R.layout.item_community_spinner, arrayListSubcommunity);
        communitySubsectionSpinner.setAdapter(adapterSubCommunity);
    }


    @Override
    public void showCommunitySections(List<ItemCommunities> sections) {
        if (this.isVisible()) {
            progressBar.setVisibility(View.GONE);
            arrayListCommunity.clear();
            ItemCommunities itemCommunities = new ItemCommunities("Select community");
            arrayListCommunity.add(itemCommunities);
            arrayListCommunity.addAll(sections);
            adapterCommunity = new FragmentAddNewItemSpinnerAdapter(getActivity(), R.layout.item_community_spinner, arrayListCommunity);
            communitySectionSpinner.setAdapter(adapterCommunity);
            communitySectionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {
                        updateSubCommunitySpinnerAdapter();

                    } else {
                        progressBar.setVisibility(VISIBLE);
                        selectedGuidCommunity = ((ItemCommunities) parent.getAdapter().getItem(position)).getGuid_community();
                        presenter.getCommunitySubsections(selectedGuidCommunity);

                        arrayListSubcommunity.clear();
                        ItemCommunities itemSubCommunity = new ItemCommunities("Select sub community");
                        arrayListSubcommunity.add(itemSubCommunity);
                        adapterSubCommunity = new FragmentAddNewItemSpinnerAdapter(getActivity(), R.layout.item_community_spinner, arrayListSubcommunity);
                        communitySubsectionSpinner.setAdapter(adapterSubCommunity);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    Toast.makeText(getActivity(), "Please, select community", Toast.LENGTH_LONG).show();
                }
            });
            communitySubsectionSpinner.setVisibility(VISIBLE);
        }

    }


    @Override
    public void showCommunitySubsections(List<ItemCommunities> subsections) {
        if (this.isVisible()) {
            progressBar.setVisibility(View.GONE);
            arrayListSubcommunity.clear();
            ItemCommunities itemSubCommunity = new ItemCommunities("Select sub community");
            arrayListSubcommunity.add(itemSubCommunity);
            arrayListSubcommunity.addAll(subsections);
            adapterSubCommunity = new FragmentAddNewItemSpinnerAdapter(getActivity(), R.layout.item_community_spinner, arrayListSubcommunity);
            communitySubsectionSpinner.setAdapter(adapterSubCommunity);
            communitySubsectionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {
                        selectedGuidSubCommunity = null;

                    } else {
                        selectedGuidSubCommunity = ((ItemCommunities) parent.getAdapter().getItem(position)).getGuid_subsection();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    Toast.makeText(getActivity(), "Please, select sub community", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public void onEventMainThread(UploadSuccessEvent event) {
        if (FragmentAddNewItem.this.isVisible()) {

            progressBar.setVisibility(View.GONE);
            Toast.makeText(getActivity(), R.string.fragment_add_new_item_item_added, Toast.LENGTH_LONG).show();
            getActivity().onBackPressed();
        }
        String imageUrl = imagePathServer.substring(7);
        Log.e("Sub link image ", imageUrl);
        SslRequestHandler.getInstance().sendRequest(new ConvertImageRequest(imageUrl), Response.class, new SslRequestHandler.SslRequestListener<Response>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(Response response) {
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {


            }
        });
    }

    @Override
    public void showItemWasAdded(AddNewItemResponse response) {
        if (imagePath != null) {
            imagePathServer = response.getItem().get(0).getImage_url();
            String urlToUpload = UploadUtils.BASE_URL + imagePathServer;
            UploadUtils.uploadImage(urlToUpload, imagePath);
        }

    }

    @Override
    public void showItemWasNotAdded() {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(getActivity(), R.string.fragment_add_new_item_item_not_added, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onValidationSucceeded() {
        View view = this.getActivity().getCurrentFocus();
        KeyboardUtils.hideKeyboard(view);
        addBtn.requestFocus();
        titleEvent = titleEt.getText().toString();
        descriptionEvent = fullDescriptionEt.getText().toString();
        phoneEvent = phoneEt.getText().toString();
        addressEvent = addressEt.getText().toString();


        if (selectedGuidCommunity == null || selectedGuidSubCommunity == null) {
            Toast.makeText(getActivity(), R.string.fragment_add_new_item_toast_select_community, Toast.LENGTH_LONG).show();
        } else if (dateEvent == null) {
            Toast.makeText(getActivity(), R.string.fragment_add_new_item_toast_pick_date, Toast.LENGTH_LONG).show();
        } else if (timeEvent == null) {
            Toast.makeText(getActivity(), R.string.fragment_add_new_item_toast_pick_time, Toast.LENGTH_LONG).show();
        } else {
            String time = dateEvent + " " + timeEvent;
            progressBar.setVisibility(VISIBLE);
            addBtn.setClickable(false);
            presenter.addNewItem(titleEvent, descriptionEvent, time, addressEvent, phoneEvent, selectedGuidSubCommunity);
        }

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this.getActivity());

            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this.getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void setDate(int year, int month, int day) {
        String date = year + ":" + month + ":" + day;
        dateEt.setText(TimeUtil.getFormattedDate(date, "yyyy:MM:dd", "dd, MMMM yyyy", Locale.ENGLISH));
        dateEvent = TimeUtil.getFormattedDate(date, "yyyy:MM:dd", "yyyy-MM-dd", Locale.ENGLISH);

    }


    @Override
    public void setTime(int hour, int minutes) {
        String time = hour + ":" + minutes;
        timeTV.setText(TimeUtil.getFormattedDate(time, "h:m", "h:m a", Locale.getDefault()));
        timeEvent = TimeUtil.getFormattedDate(time, "h:m", "HH:mm:ss", Locale.getDefault());
    }


    private class AsyncImageScale extends AsyncTask<Void, Void, Void> {
        String path;
        String newPath;

        public AsyncImageScale(String path) {
            this.path = path;
        }

        @Override
        protected Void doInBackground(Void... params) {
            newPath = ImageScaleUtils.scaleImage(path);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            imagePath = newPath;
            setImage(Build.VERSION.SDK_INT, newPath);
        }
    }
}
