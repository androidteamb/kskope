package com.mockup.kscope.kscope.viewsPresenter;

import com.mockup.kscope.kscope.entity.ItemEvent;

import java.util.List;

/**
 * Created by Stafiiyevskyi on 01.12.2015.
 */
public interface FragmentCalendarView {

    void showEvents(List<ItemEvent> events);
    void updateEvent();

}
