package com.mockup.kscope.kscope.fragments.tabs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.FriendsAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.AddUserRequest;
import com.mockup.kscope.kscope.api.request.FriendsRequest;
import com.mockup.kscope.kscope.api.request.RejectUserRequest;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.api.response.UsersResponse;
import com.mockup.kscope.kscope.entity.ItemFriend;
import com.mockup.kscope.kscope.events.FragmentStateEvent;
import com.mockup.kscope.kscope.events.FriendsFilterEvent;
import com.mockup.kscope.kscope.events.HideMenuEvent;
import com.mockup.kscope.kscope.events.RefreshProfileFragmentEvent;
import com.mockup.kscope.kscope.events.ShowProgressEvent;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.fragments.Updatable;
import com.mockup.kscope.kscope.fragments.main.FragmentFriendProfile;
import com.mockup.kscope.kscope.fragments.main.FragmentProfile;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;

import static com.mockup.kscope.kscope.adapters.FriendsAdapter.OnFriendsClick;
import static com.mockup.kscope.kscope.adapters.FriendsAdapter.OnStateButton;
import static com.mockup.kscope.kscope.adapters.ViewPagerAdapter.FragmentLifecycle;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
public class FragmentFriends extends BaseFragment implements FragmentLifecycle, OnFriendsClick, Updatable {

    @InjectView(R.id.list)
    ListView mList;
    @InjectView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @InjectView(R.id.empty)
    LinearLayout emptyView;
    private Context mContext;

    private PreferencesManager preferenceManager = PreferencesManager.getInstance();
    private SslRequestHandler requestHandler = SslRequestHandler.getInstance();

    private ArrayList<ItemFriend> friendList;
    private FriendsAdapter mAdapter;
    private List<UsersResponse.UserData> friends = new ArrayList<>();

    private List<UsersResponse.UserData> stateFriends = new ArrayList<>();

    @Nullable
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_friends;
    }

    private void sendFriendsRequest(String sortType, SslRequestHandler.SslRequestListener<UsersResponse> listener) {
        FriendsRequest friendsRequest = new FriendsRequest(preferenceManager.getSessionId(), sortType, "1");
        requestHandler.sendRequest(friendsRequest, UsersResponse.class, listener);
    }

    private void addFriendsRequest(String userLogin) {
        AddUserRequest addUserRequest = new AddUserRequest(preferenceManager.getSessionId(), userLogin);
        requestHandler.sendRequest(addUserRequest, Response.class, addUserListener);
    }

    private void rejectFriendRequest(String userLogin) {
        RejectUserRequest rejectUserRequest = new RejectUserRequest(preferenceManager.getSessionId(), userLogin);
        requestHandler.sendRequest(rejectUserRequest, Response.class, rejectUserListener);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        emptyView.setVisibility(View.GONE);

        mAdapter = new FriendsAdapter(this, mContext, new ArrayList<ItemFriend>());
        mList.setAdapter(mAdapter);
        mList.setTextFilterEnabled(false);
        mAdapter.setCallback(new OnStateButton() {
            @Override
            public void onConfirm(String userLogin) {
                if (Utils.isNetworkConnected(mContext)) {
                    addFriendsRequest(userLogin);
                } else {
                    Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancel(String userLogin) {
                if (Utils.isNetworkConnected(mContext)) {
                    rejectFriendRequest(userLogin);
                } else {
                    Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
                }
            }
        });
        if (Utils.isNetworkConnected(mContext)) {
            sendFriendsRequest("3", requestUsersStateListener);
        } else {
            Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
        }
//        if (System.currentTimeMillis() - preferenceManager.getLastFriendsUpdateTime() > TimeConstants.MINUTE) {
//            sendFriendsRequest("2", requestFriendsListener);
//        } else {
//            if (mProgressBar != null) {
//                mProgressBar.setVisibility(View.GONE);
//            }
//
//            friendList.clear();
//            friendList.addAll(ConvertDbUtils.getFriends(DatabaseHelper.getInstance(getActivity()).getFriends(preferenceManager.getUserId())));
//
//            if (friendList == null || friendList.size() == 0) {
//                emptyView.setVisibility(View.VISIBLE);
//            } else {
//                mAdapter.setItems(friendList);
//            }
//        }
    }

    public void onEvent(FriendsFilterEvent event) {
        mAdapter.getFilter().filter(event.getQueryText());
    }

    @Override
    public void updateToolbar() {

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onPauseFragment() {

    }

    @Override
    public void onFriendsItemClick(String userLogin) {
        Log.e("user click", userLogin + " ");
        FragmentProfile.searchView.setQuery(null, false);
        FragmentProfile.searchView.setIconified(true);
        EventBus.getDefault().post(new HideMenuEvent());
        ((MainActivity) getActivity()).addFragment(FragmentFriendProfile.newInstance(userLogin), userLogin);
    }

    @Override
    public void update() {
        if (Utils.isNetworkConnected(mContext)) {
            if (FragmentProfile.searchView.isIconified()) {
                sendFriendsRequest("3", requestUsersStateListener);
            }
        }
    }

    private SslRequestHandler.SslRequestListener requestFriendsListener = new SslRequestHandler.SslRequestListener<UsersResponse>() {

        @Override
        public void onStart() {

        }

        @Override
        public void onSuccess(UsersResponse response) {
            friendList = new ArrayList<>();
            friends = response.getFriends();
        }

        @Override
        public void onFailure(String errorMsg) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFinish() {
            if (!isVisible()) {
                return;
            }

            if (stateFriends != null) {
                for (int i = 0; i < stateFriends.size(); i++) {
                    ItemFriend friend = new ItemFriend();
                    UsersResponse.UserData userData = stateFriends.get(i);
                    friend.setUser_login(userData.getUser_login());
                    friend.setFirst_name(userData.getFirst_name());
                    friend.setSecond_name(userData.getSecond_name());
                    friend.setId_user(userData.getId_user());
                    friend.setUser_photo(userData.getUser_picture());
                    friend.setConfirm(true);
                    friendList.add(friend);
                }
            }
            if (friends != null) {
                for (int i = 0; i < friends.size(); i++) {
                    ItemFriend friend = new ItemFriend();
                    UsersResponse.UserData userData = friends.get(i);
                    friend.setUser_login(userData.getUser_login());
                    friend.setFirst_name(userData.getFirst_name());
                    friend.setSecond_name(userData.getSecond_name());
                    friend.setId_user(userData.getId_user());
                    friend.setUser_photo(userData.getUser_picture());
                    friendList.add(friend);
                }
            }
//                    if (friends.size() > 0) {
//                        preferenceManager.setLastFriendsUpdateTime(System.currentTimeMillis());
//                        List<DbFriend> dbFriends = new ArrayList<>();
//                        String userId = preferenceManager.getUserId();
//                        for (ItemFriend friend : friendList) {
//                            dbFriends.add(new DbFriend(friend, userId));
//                        }
//                        DatabaseHelper.getInstance(getActivity()).updateFriends(dbFriends, userId);
//                    }

            if (friendList == null || friendList.size() == 0) {
                emptyView.setVisibility(View.VISIBLE);
                mAdapter.setItems(new ArrayList<ItemFriend>());
            } else {
                mAdapter.setItems(friendList);
                EventBus.getDefault().post(new FragmentStateEvent(0));
            }
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }
        }
    };

    private SslRequestHandler.SslRequestListener requestUsersStateListener = new SslRequestHandler.SslRequestListener<UsersResponse>() {

        @Override
        public void onStart() {
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onSuccess(UsersResponse response) {
            stateFriends = response.getFriends();
        }

        @Override
        public void onFailure(String errorMsg) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFinish() {
            if (!isVisible()) {
                return;
            }

            if (Utils.isNetworkConnected(getActivity())) {
                sendFriendsRequest("2", requestFriendsListener);
            } else {
                Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
            }
        }

    };

    private SslRequestHandler.SslRequestListener addUserListener = new SslRequestHandler.SslRequestListener<Response>() {

        public Response response;

        @Override
        public void onStart() {
            EventBus.getDefault().post(new ShowProgressEvent());
        }

        @Override
        public void onSuccess(Response response) {
            this.response = response;
        }

        @Override
        public void onFailure(String errorMsg) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFinish() {
            if (!response.getRequestResult().equals("0")) {
                sendFriendsRequest("3", requestUsersStateListener);
                EventBus.getDefault().post(new RefreshProfileFragmentEvent());
            }
        }
    };

    private SslRequestHandler.SslRequestListener rejectUserListener = new SslRequestHandler.SslRequestListener<Response>() {

        public Response response;

        @Override
        public void onStart() {
            EventBus.getDefault().post(new ShowProgressEvent());
        }

        @Override
        public void onSuccess(Response response) {
            this.response = response;
        }

        @Override
        public void onFailure(String errorMsg) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFinish() {
            if (!response.getRequestResult().equals("0")) {
                sendFriendsRequest("3", requestUsersStateListener);
                EventBus.getDefault().post(new RefreshProfileFragmentEvent());
            }
        }
    };
}
