package com.mockup.kscope.kscope.dialogs;

/**
 * Created by Stafiiyevskyi on 02.12.2015.
 */
public interface DialogSendCommentCallback {

    void confirm(String inputText, String imagePath);

    void cancel();

    void dismiss();
}
