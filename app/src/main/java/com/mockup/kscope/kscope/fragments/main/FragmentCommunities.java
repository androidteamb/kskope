package com.mockup.kscope.kscope.fragments.main;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.CommunityCheckListAdapter;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.customView.DividerItemDecoration;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.presenters.FragmentCommunitiesPresenterImpl;
import com.mockup.kscope.kscope.utils.Utils;
import com.mockup.kscope.kscope.viewsPresenter.FragmentCommunitiesView;

import java.util.List;

import butterknife.InjectView;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class FragmentCommunities extends BaseFragment implements FragmentCommunitiesView,
        CommunityCheckListAdapter.OnItemClickListener {

    @InjectView(R.id.communities_recycler)
    RecyclerView recyclerView;
    @InjectView(R.id.progress)
    ProgressBar progressBar;
    @InjectView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private CommunityCheckListAdapter adapterRecycler;
    private FragmentCommunitiesPresenterImpl presenter;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new FragmentCommunitiesPresenterImpl(this);
        adapterRecycler = new CommunityCheckListAdapter(getActivity(), this);
        firstInitializationOfRecycler();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getData();
            }
        });
        getData();
    }

    private void firstInitializationOfRecycler() {
        adapterRecycler.clearData();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapterRecycler);
    }

    private void initListOfCommunities(List<ItemCommunities> data) {
        adapterRecycler.clearData();
        adapterRecycler.addData(data);
        adapterRecycler.notifyDataSetChanged();
    }

    private void getData() {
        if (Utils.isNetworkConnected(getActivity())) {
            if (!swipeRefreshLayout.isRefreshing()) {
                progressBar.setVisibility(View.VISIBLE);
            }
            presenter.getCommunities();
        } else {
            Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_communities_check;
    }

    @Override
    public void updateToolbar() {
        ((MainActivity) getActivity()).setActionBarTitle("Communities", true);
    }

    @Override
    public void entranceShow(Response response) {

    }

    @Override
    public void exitShow(Response response) {

    }

    @Override
    public void showCommunities(List<ItemCommunities> communities) {
        if (this.isVisible()) {
            swipeRefreshLayout.setRefreshing(false);
            progressBar.setVisibility(View.INVISIBLE);
            initListOfCommunities(communities);
        }
    }

    @Override
    public void onItemClick(final int position) {
        final ItemCommunities community = adapterRecycler.getCommunityByPosition(position);

        if (community.getAdded().equalsIgnoreCase("0")) {
            presenter.entranceToCommunity(community.getGuid_community());
            adapterRecycler.getCommunityByPosition(position).setAdded("1");
            adapterRecycler.notifyItemChanged(position);
        } else {
            presenter.exitFromCommunity(community.getGuid_community());
            adapterRecycler.getCommunityByPosition(position).setAdded("0");
            adapterRecycler.notifyItemChanged(position);
        }
    }

}

