package com.mockup.kscope.kscope.viewsPresenter;

import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.entity.ItemCommentPlace;
import com.mockup.kscope.kscope.entity.ItemSubsection;

import java.util.List;

/**
 * Created by Stafiiyevskyi on 01.12.2015.
 */
public interface FragmentCommunitySectionItemView {

    void showAllItemsData(ItemSubsection item);
    void showComments(List<ItemCommentPlace> comments);
    void showCommentsCount(String commentCount);
    void showRating(String itemRating);
    void showLikesCount(String itemsLikesCount);
    void updateAllData(Response response);
    void updateDataAfterSendComment();
}
