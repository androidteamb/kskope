package com.mockup.kscope.kscope.api.request;

/**
 * Created by Serhii_Slobodyanuk on 10.12.2015.
 */
public class UserGalleryRequest extends Request {

    private Gallery request_param;

    public UserGalleryRequest(String sessionId, String userId) {
        setSessionId(sessionId);
        setRequestCmd("get_gallery");
        request_param = new Gallery();
        request_param.id_user = userId;
    }

    private class Gallery {
        @SuppressWarnings("unused")
        private String id_user;
    }
}
