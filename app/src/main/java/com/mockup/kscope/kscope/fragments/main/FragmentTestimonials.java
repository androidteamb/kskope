package com.mockup.kscope.kscope.fragments.main;

import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.adapters.TestimonialsAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.GetTestimonialsRequest;
import com.mockup.kscope.kscope.api.response.GetTestimonialsResponse;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.customView.TypefaceTextView;
import com.mockup.kscope.kscope.entity.ItemTestimonial;
import com.mockup.kscope.kscope.entity.ItemTestimonials;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import java.util.ArrayList;

import butterknife.InjectView;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class FragmentTestimonials extends BaseFragment {

    @InjectView(R.id.list_testimonials)
    ExpandableListView lvTestimonials;
    @InjectView(R.id.no_data)
    TypefaceTextView noDataTV;
    @InjectView(R.id.progress)
    ProgressBar progressBar;

    TestimonialsAdapter adapter;
    SslRequestHandler.SslRequestListener listener = new SslRequestHandler.SslRequestListener<GetTestimonialsResponse>() {
        GetTestimonialsResponse response;

        @Override
        public void onStart() {

        }

        @Override
        public void onSuccess(GetTestimonialsResponse response) {
            this.response = response;
        }

        @Override
        public void onFailure(String errorMsg) {

        }

        @Override
        public void onFinish() {
            if (FragmentTestimonials.this.isVisible()){
                if (response != null) {
                    ArrayList<ItemTestimonials> testimonialses = new ArrayList<ItemTestimonials>();
                    if (response.getAllTestimonials()!=null){
                        testimonialses.addAll(response.getAllTestimonials());
                        showTestimonials(testimonialses);
                    } else {
                        progressBar.setVisibility(View.GONE);
                        noDataTV.setVisibility(View.VISIBLE);
                    }

                }
            }



        }
    };
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar.setVisibility(View.VISIBLE);
        GetTestimonialsRequest request = new GetTestimonialsRequest();
        SslRequestHandler.getInstance().sendRequest(request, GetTestimonialsResponse.class, listener);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_testimonials;
    }

    @Override
    public void updateToolbar() {

    }

//    private ArrayList<ItemTestimonial> initDummyData() {
//        ArrayList<ItemTestimonials> about = new ArrayList<>();
//        ItemTestimonial testimonial1 = new ItemTestimonial("Collete Philips","15:39 - 15/12/2014", "Phasellus sed urna vestibulum, blandit turpis a, eleif end erat. Aenean eget pulvinar tortor. Donec quam tellus, eleifend non dignissim consequat, egestas vellectus. Praesent neque lorem, commodo et accum san eu, pre tium vel lectus. Curabitur rutrum ligula sed magna lacinia consequat.");
//        ItemTestimonial testimonial2 = new ItemTestimonial("Boston Chamber of Commerce", "15:39 - 15/12/2014", "Phasellus sed urna vestibulum, blandit turpis a, eleif end erat. Aenean eget pulvinar tortor. Donec quam tellus, eleifend non dignissim consequat, egestas vellectus. Praesent neque lorem, commodo et accum san eu, pre tium vel lectus. Curabitur rutrum ligula sed magna lacinia consequat.");
//        ItemTestimonial testimonial3 = new ItemTestimonial("Mayor of Boston", "15:39 - 15/12/2014", "Phasellus sed urna vestibulum, blandit turpis a, eleif end erat. Aenean eget pulvinar tortor. Donec quam tellus, eleifend non dignissim consequat, egestas vellectus. Praesent neque lorem, commodo et accum san eu, pre tium vel lectus. Curabitur rutrum ligula sed magna lacinia consequat.");
//        about.add(testimonial1);
//        about.add(testimonial2);
//        about.add(testimonial3);
//        return about;
//    }
//

    private void showTestimonials(ArrayList<ItemTestimonials> testimonialses){
        noDataTV.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        adapter = new TestimonialsAdapter(getActivity(), testimonialses);
        lvTestimonials.setAdapter(adapter);
        lvTestimonials.setGroupIndicator(null);
    }
    

}