package com.mockup.kscope.kscope.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Button;
import android.widget.TimePicker;

import com.mockup.kscope.kscope.R;

import java.util.Calendar;

/**
 * Created by Stafiiyevskyi on 07.12.2015.
 */
public class DialogFragmentTimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener{

    private TimePickerListener listener;

    public static DialogFragmentTimePicker newInstance(TimePickerListener listener) {
        DialogFragmentTimePicker dialog = new DialogFragmentTimePicker();
        dialog.listener = listener;
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minutes = c.get(Calendar.MINUTE);

        Dialog picker = new TimePickerDialog(getActivity(),this,hour,minutes,false);
        return picker;
    }

    @Override
    public void onStart() {
        super.onStart();

        Button nButton = ((AlertDialog) getDialog())
                .getButton(DialogInterface.BUTTON_POSITIVE);
        nButton.setText(getResources().getString(R.string.date_picker_confirm));

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, android.R.style.Theme_Light_Panel);
    }



    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        listener.setTime(hourOfDay, minute);
    }


    public interface TimePickerListener {
        void setTime(int hour, int minutes);
    }
}
