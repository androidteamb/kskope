package com.mockup.kscope.kscope.entity;

import java.util.Date;

/**
 * Created by Stafiiyevskyi on 28.10.2015.
 */
public class ItemEvent {

    private String added;
    private String address;
    private String description;
    private String guid_item;
    private String guid_subsection;
    private String phone;
    private String rating;
    private String time;
    private String title;
    private String owner;


    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGuid_item() {
        return guid_item;
    }

    public void setGuid_item(String guid_item) {
        this.guid_item = guid_item;
    }

    public String getGuid_subsection() {
        return guid_subsection;
    }

    public void setGuid_subsection(String guid_subsection) {
        this.guid_subsection = guid_subsection;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
