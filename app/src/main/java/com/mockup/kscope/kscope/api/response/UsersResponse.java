package com.mockup.kscope.kscope.api.response;

import java.util.List;

/**
 * Created by Serhii_Slobodianiuk on 02.11.2015.
 */
public class UsersResponse extends Response {
    private FriendsRequestParam request_param = new FriendsRequestParam();

//    public ArrayList<RelationShipFriends> getFriends() {
//        return request_param.relationship;
//    }
    public List<UserData> getFriends() {
        return request_param.getRelationship();
    }

    public FriendsRequestParam getRequestParam() {
        return request_param;
    }

    public static class FriendsRequestParam extends InvalidParam {

        private String data_count;
        private String data_limit;
        private String data_offset;

        private List<UserData> relationship;

        public String getDataCount() {
            return data_count;
        }

        public String getDataLimit() {
            return data_limit;
        }

        public String getDataOffset() {
            return data_offset;
        }

        public List<UserData> getRelationship() {
            return relationship;
        }
    }

    public static class  UserData extends InvalidParam {
        private String user_login;
        private String first_name;
        private String second_name;
        private String id_user;
        private String user_image;

        public String getUser_login() {
            return user_login;
        }

        public void setUser_login(String user_login) {
            this.user_login = user_login;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getSecond_name() {
            return second_name;
        }

        public void setSecond_name(String second_name) {
            this.second_name = second_name;
        }

        public String getId_user() {
            return id_user;
        }

        public void setId_user(String id_user) {
            this.id_user = id_user;
        }

        public String getUser_picture() {
            return user_image;
        }

        public void setUser_picture(String user_picture) {
            this.user_image = user_picture;
        }
    }

}
