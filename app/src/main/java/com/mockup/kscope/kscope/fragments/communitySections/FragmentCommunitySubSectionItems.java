package com.mockup.kscope.kscope.fragments.communitySections;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.SubSectionItemsAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.GetShortItemsList;
import com.mockup.kscope.kscope.api.response.GetSubsectionItemsResponse;
import com.mockup.kscope.kscope.entity.ItemSubsection;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Stafiiyevskyi on 30.10.2015.
 */
public class FragmentCommunitySubSectionItems extends BaseFragment implements AdapterView.OnItemClickListener {
    @InjectView(R.id.communities_list)
    ListView lvCommunities;
    @InjectView(R.id.progress)
    ProgressBar progressBar;
    @InjectView(R.id.empty)
    LinearLayout emptyView;
    @InjectView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    private SubSectionItemsAdapter adapter;
    private String title;
    private String guidSubsection;
    private SslRequestHandler.SslRequestListener listener;

    public static Fragment newInstance(String title, String subSection) {
        Fragment fragment = new FragmentCommunitySubSectionItems();
        Bundle args = new Bundle();
        args.putString(MainActivity.TITLE_KEY, title);
        args.putString(MainActivity.SUBSECTION_GUID, subSection);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        title = getArguments().getString(MainActivity.TITLE_KEY);
        guidSubsection = getArguments().getString(MainActivity.SUBSECTION_GUID);
        lvCommunities.setOnItemClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getData();
            }
        });
        updateToolbar();
        initListener();
        getData();

    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        ((MainActivity) getActivity()).setActionBarTitle(title, true);

    }

    private void initListOfSubsection(List<ItemSubsection> data) {
        adapter = new SubSectionItemsAdapter(getActivity(), data);
        lvCommunities.setAdapter(adapter);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_communities;
    }

    @Override
    public void updateToolbar() {
        ((MainActivity) getActivity()).setActionBarTitle(title, true);
        Toolbar toolbar = ButterKnife.findById(getActivity(), R.id.toolbar);
        LinearLayout message = ButterKnife.findById(toolbar, R.id.menu_messages);
        message.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Fragment fragment = new FragmentCommunitySectionItem();
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.TITLE_KEY, ((ItemSubsection) parent.getAdapter().getItem(position)).getTitle());
        bundle.putString(MainActivity.ITEM_GUID, ((ItemSubsection) parent.getAdapter().getItem(position)).getGuid_item());
        fragment.setArguments(bundle);
        ((MainActivity) getActivity()).addFragment(fragment, ((ItemSubsection) parent.getAdapter().getItem(position)).getTitle());

    }

    private void getData() {
        GetShortItemsList requestNotFullList = new GetShortItemsList(guidSubsection);
        SslRequestHandler.getInstance().sendRequest(requestNotFullList, GetSubsectionItemsResponse.class, listener);
    }

    private void initListener() {
        listener = new SslRequestHandler.SslRequestListener<GetSubsectionItemsResponse>() {
            List<ItemSubsection> data;

            @Override
            public void onStart() {
                if (!swipeRefreshLayout.isRefreshing()) {

                    progressBar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSuccess(GetSubsectionItemsResponse response) {
                data = response.getSubsectionItems();
                Log.i("SubSectionRequest", response.getRequestResult());
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {

                if (FragmentCommunitySubSectionItems.this.isVisible()) {
                    swipeRefreshLayout.setRefreshing(false);
                    progressBar.setVisibility(View.INVISIBLE);
                    if (data == null) {
                        emptyView.setVisibility(View.VISIBLE);
                    } else {
                        initListOfSubsection(data);
                        emptyView.setVisibility(View.INVISIBLE);
                    }
                }

            }
        };
    }

    @Override
    public void onPause() {
        super.onPause();
        swipeRefreshLayout.setRefreshing(false);
        ImageLoader.getInstance().pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        ImageLoader.getInstance().resume();
    }

    @Override
    public void onStop() {
        super.onStop();
        ImageLoader.getInstance().stop();
    }
}
