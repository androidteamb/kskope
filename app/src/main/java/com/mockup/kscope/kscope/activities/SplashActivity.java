package com.mockup.kscope.kscope.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.utils.PreferencesManager;


/**
 * Created by MPODOLSKY on 12.06.2015.
 */

public class SplashActivity extends BaseActivity {

    public static final int DEFAULT_SPLASH_TIME = 2000;
    private final PreferencesManager preferencesManager = PreferencesManager.getInstance();
    private Handler mHandler;
    private Runnable mStartActivityRunnable;
    private boolean isLoggedIn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isLoggedIn = checkSession();

        mHandler = new Handler();
        mStartActivityRunnable = new Runnable() {
            @Override
            public void run() {
                if (!isLoggedIn) {
                    startActivity(new Intent(SplashActivity.this, StartUpActivity.class));
                } else {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                }
                finish();
            }
        };
        mHandler.postDelayed(mStartActivityRunnable, DEFAULT_SPLASH_TIME);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mHandler.removeCallbacks(mStartActivityRunnable);
    }

    private boolean checkSession() {
        if (preferencesManager.getSessionId() == null) {
            return false;
        }
        return true;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }
}
