package com.mockup.kscope.kscope.dialogs;

/**
 * Created by Stafiiyevskyi on 25.11.2015.
 */
public interface DialogRatingCallback {

    void confirm(float rating);

    void cancel();
}
