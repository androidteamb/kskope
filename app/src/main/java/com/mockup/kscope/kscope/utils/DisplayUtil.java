package com.mockup.kscope.kscope.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.media.ExifInterface;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

import java.io.File;
import java.io.IOException;

/**
 * Created by Stafiiyevskyi on 02.11.2015.
 */
public class DisplayUtil {

    private static Point screenSize;

    public static Point getScreenSize(Context context) {
        if (screenSize != null) {
            return screenSize;
        }
        int pxWidth;
        int pxHeight;

        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        pxWidth = display.getWidth();
        pxHeight = display.getHeight();


        screenSize = new Point(pxWidth, pxHeight);
        return screenSize;
    }

    public static int getScreenWidth(Context context) {
        return getScreenSize(context).x;
    }

    public static int getScreenHeight(Context context) {
        return getScreenSize(context).y;
    }

    public static void resizeDialog(Dialog dialog) {
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public static int getImageOrientation(String imagePath) {
        int rotate = 0;
        try {
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotate;
    }
}
