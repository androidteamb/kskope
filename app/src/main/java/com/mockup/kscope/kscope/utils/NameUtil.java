package com.mockup.kscope.kscope.utils;

import android.text.TextUtils;

/**
 * Created by mpodolsky on 16.12.2015.
 */
public class NameUtil {

    public static String getName(String name, String surname, String login) {
        String res = "";
        if (!TextUtils.isEmpty(name) || !TextUtils.isEmpty(surname)) {
            if (!TextUtils.isEmpty(name)) {
                res = res + name + " ";
            }
            if (!TextUtils.isEmpty(surname)) {
                res = res + surname;
            }
        } else {
            if (!TextUtils.isEmpty(login)) {
                res = login;
            }
        }
        return res;
    }

}
