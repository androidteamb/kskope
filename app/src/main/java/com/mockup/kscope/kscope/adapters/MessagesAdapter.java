package com.mockup.kscope.kscope.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.constants.StatusConstants;
import com.mockup.kscope.kscope.entity.ItemMessage;
import com.mockup.kscope.kscope.utils.CircleTransform;
import com.mockup.kscope.kscope.utils.NameUtil;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
public class MessagesAdapter extends BaseAdapter implements Filterable {
    private List<ItemMessage> messagesList;
    private List<ItemMessage> orig;
    private Context mContext;

    private OnItemClickCallback mCallback;

    public interface OnItemClickCallback {
        void onMessageClick(ItemMessage message, int position);
    }

    public MessagesAdapter(Context mContext, List<ItemMessage> messagesList) {
        this.messagesList = messagesList;
        this.mContext = mContext;
    }

    public void setItems(List<ItemMessage> messagesList) {
        this.messagesList = messagesList;
        notifyDataSetChanged();
    }

    public void setCallback(OnItemClickCallback callback) {
        this.mCallback = callback;
    }

    @Override
    public int getCount() {
        return messagesList.size();
    }

    @Override
    public Object getItem(int position) {
        return messagesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_messages, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        final ItemMessage message = (ItemMessage) getItem(position);

        String userImage = message.getUserImage();
        if (!TextUtils.isEmpty(userImage)) {
            Picasso.with(mContext)
                    .load(userImage)
                    .placeholder(R.drawable.ic_photo_placeholder)
                    .fit()
                    .centerCrop()
                    .transform(new CircleTransform())
                    .into(holder.image);
        } else {
            Picasso.with(mContext)
                    .load(R.drawable.ic_photo_placeholder)
                    .placeholder(R.drawable.ic_photo_placeholder)
                    .into(holder.image);
        }
        SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat to = new SimpleDateFormat("hh:mm a");
        String inputDate = message.getMessageTime();
        try {
            String date = to.format(from.parse(inputDate));
            holder.tvDate.setText(date);

        } catch (ParseException e) {
            holder.tvDate.setText(message.getMessageTime());
        }
        holder.tvSenderName.setText(NameUtil.getName(message.getFirstName(), message.getSecondName(), message.getUserLogin()));
        holder.tvMessage.setText(message.getMessageText());

//
        if (message.getMessageStatus().equals(StatusConstants.MESSAGE_READ)) {
            holder.status.setImageResource(R.drawable.ic_message_on);
        } else {
            holder.status.setImageResource(R.drawable.ic_message_off);
        }

        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallback != null) {
                    mCallback.onMessageClick(message, position);
                }
            }
        });

        return view;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults filter = new FilterResults();
                final List<ItemMessage> results = new ArrayList<>();
                if (orig == null)
                    orig = messagesList;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final ItemMessage item : orig) {
                            String firstName = item.getFirstName().toLowerCase();
                            String lastName = item.getSecondName().toLowerCase();
                            String text = item.getMessageText().toLowerCase();
                            if (firstName.contains(constraint.toString()) ||
                                    lastName.contains(constraint.toString()) ||
                                    text.contains(constraint.toString()))
                                results.add(item);
                        }
                    }
                    filter.values = results;
                }
                return filter;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                messagesList = (List<ItemMessage>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    static class ViewHolder {

        @InjectView(R.id.root)
        RelativeLayout root;
        @InjectView(R.id.username)
        TextView tvSenderName;
        @InjectView(R.id.message)
        TextView tvMessage;
        @InjectView(R.id.image)
        ImageView image;
        @InjectView(R.id.status)
        ImageView status;
        @InjectView(R.id.date)
        TextView tvDate;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }


}
