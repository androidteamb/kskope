package com.mockup.kscope.kscope.utils;

import com.mockup.kscope.kscope.events.UploadSuccessEvent;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import de.greenrobot.event.EventBus;

/**
 * Created by Alexander Rubanskiy on 15.12.2015.
 */

public class UploadUtils {
//                String urlToUpload = "http://52.34.106.138/upload.php";
//                String filePath = "/storage/emulated/0/DCIM/Camera/20151210_173945.jpg";
//                UploadUtils.uploadImage(urlToUpload, filePath);

    public final static String BASE_URL = "http://52.34.106.138/upload.php?dir=";

    public static void uploadImage(final String urlToUpload, final String filePath) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                uploadFile(urlToUpload, filePath);
            }
        }).start();
    }

    private static void uploadFile(String urlToUpload, String filePath) {
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary =  "*****";

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1*1024*1024;

        try {
            FileInputStream fileInputStream = new FileInputStream(new File(filePath));

            URL url = new URL(urlToUpload);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Allow Inputs &amp; Outputs.
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            // Set HTTP method to POST.
            connection.setRequestMethod("POST");

            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

            DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + filePath +"\"" + lineEnd);
            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // Read file
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {
                outputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // TODO check response
            // Responses from the server (code and message)
            int serverResponseCode = connection.getResponseCode();
            String serverResponseMessage = connection.getResponseMessage();

            EventBus.getDefault().post(new UploadSuccessEvent());

            fileInputStream.close();
            outputStream.flush();
            outputStream.close();

            fileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
