package com.mockup.kscope.kscope.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.entity.ItemLike;
import com.mockup.kscope.kscope.utils.TimeUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
public class LikesAdapter extends BaseAdapter implements Filterable {

    private List<ItemLike> likeList;
    private List<ItemLike> orig;
    private Context mContext;
    private OnLikesClick callback;

    public interface OnLikesClick {
        public void onLikeItemClick(String guidItem);
    }

    public LikesAdapter(OnLikesClick callback, Context mContext, List<ItemLike> likeList) {
        this.callback = callback;
        this.likeList = likeList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return likeList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;

        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_likes, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        String imageUrl = likeList.get(position).getImageUrl();

        displayImage(imageUrl, holder);

        holder.tvLike.setText(likeList.get(position).getTitle());
        holder.tvDate.setText(TimeUtil.getFormattedDate(likeList.get(position).getTime(), "yyyy-MM-dd HH:mm:ss", "hh:mm aa", Locale.ENGLISH));

        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onLikeItemClick(likeList.get(position).getGuidItem());
            }
        });

        return view;
    }

    private void displayImage(String imageUrl, ViewHolder holder) {
        if (imageUrl != null) {
            if (!TextUtils.isEmpty(imageUrl)) {
                if (imageUrl.startsWith("http://") || imageUrl.startsWith("https://")) {
                    Picasso.with(mContext).load(imageUrl).centerCrop().resize(48, 48).into(holder.image);
                } else if (imageUrl.startsWith("/")) {
                    Picasso.with(mContext).load(SslRequestHandler.IMAGE_URL_PREFIX + imageUrl).centerCrop().resize(48, 48).into(holder.image);
                } else {
                    Picasso.with(mContext).load(R.drawable.bg_calendar_selected).into(holder.image);
                }
            } else {
                Picasso.with(mContext).load(R.drawable.bg_calendar_selected).into(holder.image);
            }
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults filter = new FilterResults();
                final List<ItemLike> results = new ArrayList<>();
                if (orig == null)
                    orig = likeList;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final ItemLike item : orig) {
                            if (item.getTitle().toLowerCase()
                                    .contains(constraint.toString()))
                                results.add(item);
                        }
                    }
                    filter.values = results;
                }
                return filter;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                likeList = (List<ItemLike>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void setItems(List<ItemLike> items) {
        this.likeList = items;
        notifyDataSetChanged();
    }

    static class ViewHolder {

        @InjectView(R.id.title)
        TextView tvLike;
        @InjectView(R.id.image)
        CircleImageView image;
        @InjectView(R.id.date)
        TextView tvDate;
        @InjectView(R.id.root)
        RelativeLayout root;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
