package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 23.11.2015.
 */
public class AddVoteItemRequest extends Request {

    private AddVoteItem request_param;

    public AddVoteItemRequest(String guid_item, String sessionId, String voteStars){
        setRequestCmd("add_vote");
        setSessionId(sessionId);
        request_param = new AddVoteItem();
        request_param.guid_item = guid_item;
        request_param.vote = voteStars;
    }

    private class AddVoteItem {
        @SuppressWarnings("unused")
        private String guid_item;
        private String vote;
    }

}