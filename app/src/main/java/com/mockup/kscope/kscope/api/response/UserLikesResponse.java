package com.mockup.kscope.kscope.api.response;

import com.mockup.kscope.kscope.entity.ItemLike;

import java.util.ArrayList;

/**
 * Created by Serhii_Slobodianiuk on 03.11.2015.
 */
public class UserLikesResponse extends Response {

    private LikesRequestParam request_param = new LikesRequestParam();

    public LikesRequestParam getRequestParam() {
        return request_param;
    }

    public ArrayList<ItemLike> getLikes() {
        return getRequestParam().getLikesResult();
    }

    public static class LikesRequestParam extends InvalidParam {

        private String data_count;
        private String data_limit;
        private String data_offset;

        private ArrayList<ItemLike> user_item;

        public String getDataCount() {
            return data_count;
        }

        public String getDataLimit() {
            return data_limit;
        }

        public String getDataOffset() {
            return data_offset;
        }

        public ArrayList<ItemLike> getLikesResult() {
            return user_item;
        }
    }
}
