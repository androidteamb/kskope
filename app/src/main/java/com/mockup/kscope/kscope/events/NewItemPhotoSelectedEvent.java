package com.mockup.kscope.kscope.events;

import android.net.Uri;

/**
 * Created by Stafiiyevskyi on 10.12.2015.
 */
public class NewItemPhotoSelectedEvent {

    private Uri photoUri;

    public NewItemPhotoSelectedEvent(Uri photoUri) {
        this.photoUri = photoUri;
    }

    public Uri getPhotoUri() {
        return photoUri;
    }
}
