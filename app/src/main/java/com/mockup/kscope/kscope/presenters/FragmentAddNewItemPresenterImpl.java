package com.mockup.kscope.kscope.presenters;

import android.os.AsyncTask;

import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.AddNewItemRequest;
import com.mockup.kscope.kscope.api.request.GetCommunityRequest;
import com.mockup.kscope.kscope.api.request.GetSubsectionRequest;
import com.mockup.kscope.kscope.api.response.AddNewItemResponse;
import com.mockup.kscope.kscope.api.response.GetCommunityResponse;
import com.mockup.kscope.kscope.api.response.GetSubsectionResponse;
import com.mockup.kscope.kscope.utils.ImageUploader;
import com.mockup.kscope.kscope.utils.PathUtil;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.utils.UploadUtils;
import com.mockup.kscope.kscope.viewsPresenter.FragmentAddNewItemView;

/**
 * Created by Stafiiyevskyi on 02.12.2015.
 */
public class FragmentAddNewItemPresenterImpl implements FragmentAddNewItemPresenter {

    private FragmentAddNewItemView view;

    public FragmentAddNewItemPresenterImpl(FragmentAddNewItemView view) {
        this.view = view;
    }

    @Override
    public void getCommunitySections() {

        GetCommunityRequest getCommunityRequest = new GetCommunityRequest();
        SslRequestHandler.getInstance().sendRequest(getCommunityRequest, GetCommunityResponse.class, new SslRequestHandler.SslRequestListener<GetCommunityResponse>() {

            GetCommunityResponse getCommunityResponse;

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(GetCommunityResponse response) {
                this.getCommunityResponse = response;
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                if (getCommunityResponse != null) {
                    if (getCommunityResponse.getCommunity() != null) {
                        view.showCommunitySections(getCommunityResponse.getCommunity());
                    }
                }
            }
        });

    }

    @Override
    public void getCommunitySubsections(String guidSections) {

        GetSubsectionRequest request = new GetSubsectionRequest(guidSections, null);
        SslRequestHandler.getInstance().sendRequest(request, GetSubsectionResponse.class, new SslRequestHandler.SslRequestListener<GetSubsectionResponse>() {

            GetSubsectionResponse getCommunityResponse;

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(GetSubsectionResponse response) {
                this.getCommunityResponse = response;
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                if (getCommunityResponse != null) {
                    if (getCommunityResponse.getSub_section() != null) {
                        view.showCommunitySubsections(getCommunityResponse.getSub_section());
                    }
                }
            }
        });


    }

    @Override
    public void addNewItem(String title, String description, String time, String address, String phone, String guid_subsection) {
        AddNewItemRequest request = new AddNewItemRequest(PreferencesManager.getInstance().getSessionId(),
                guid_subsection, time, title, address, description, phone);

        SslRequestHandler.getInstance().sendRequest(request, AddNewItemResponse.class, new SslRequestHandler.SslRequestListener<AddNewItemResponse>() {
            AddNewItemResponse response;

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(AddNewItemResponse response) {
                this.response = response;
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                if (response != null) {
                    if (response.getRequestResult() != null) {
                        if (response.getRequestResult().equalsIgnoreCase("1")) {
                            view.showItemWasAdded(response);
                        } else {
                            view.showItemWasNotAdded();
                        }
                    }
                }


            }
        });
    }

}


