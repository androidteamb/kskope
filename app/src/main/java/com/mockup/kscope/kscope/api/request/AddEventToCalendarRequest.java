package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 26.11.2015.
 */
public class AddEventToCalendarRequest  extends Request {

    private AddEventToCalendar request_param;

    public AddEventToCalendarRequest(String sessionId, String guid_item, String time){
        setRequestCmd("add_item_to_calendar");
        setSessionId(sessionId);
        request_param = new AddEventToCalendar();
        request_param.guid_item = guid_item;
        request_param.time = time;
    }

    private class AddEventToCalendar {
        @SuppressWarnings("unused")
        private String guid_item;
        @SuppressWarnings("unused")
        private String time;
    }

}
