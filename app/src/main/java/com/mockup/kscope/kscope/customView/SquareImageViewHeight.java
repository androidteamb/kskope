package com.mockup.kscope.kscope.customView;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Custom image view sets width of image equal to its height
 */
public class SquareImageViewHeight extends ImageView {

    public SquareImageViewHeight(Context context) {
        super(context);
    }

    public SquareImageViewHeight(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageViewHeight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int height = getMeasuredHeight();
        setMeasuredDimension(height, height);
    }

}