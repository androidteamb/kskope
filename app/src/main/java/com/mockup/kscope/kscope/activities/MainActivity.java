package com.mockup.kscope.kscope.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.events.AvatarChangedEvent;
import com.mockup.kscope.kscope.events.NewItemPhotoSelectedEvent;
import com.mockup.kscope.kscope.events.NewItemTakePhotoEvent;
import com.mockup.kscope.kscope.events.SelectedProfilePhotoEvent;
import com.mockup.kscope.kscope.events.TakenProfilePhotoEvent;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.fragments.PhotoFragment;
import com.mockup.kscope.kscope.fragments.main.FragmentMainMenu;
import com.mockup.kscope.kscope.fragments.main.FragmentProfile;
import com.mockup.kscope.kscope.utils.KeyboardUtils;
import com.mockup.kscope.kscope.utils.PreferencesManager;

import de.greenrobot.event.EventBus;

public class MainActivity extends BaseActivity {

    public static final String TITLE_KEY = "title_key";
    public static final String SECTION_GUID = "section_guid";
    public static final String SUBSECTION_GUID = "subsection_guid";
    public static final String ITEM_GUID = "item_guid";

    private SslRequestHandler requestHandler = SslRequestHandler.getInstance();
    Fragment fragmentProfile;
    FragmentMainMenu fragmentMainMenu;

    public static void launch(Context context) {
        Intent i = new Intent(context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragmentProfile = new FragmentProfile();
        addFragment(fragmentProfile, "My profile");
        getToolbar().setNavigationIcon(getResources().getDrawable(R.drawable.ic_ab_back));
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    public void addFragment(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .add(R.id.fragments_container, fragment, title)
                .addToBackStack(title)
                .commit();
        getToolbar().setNavigationIcon(getResources().getDrawable(R.drawable.ic_ab_back));
        setActionBarTitle(title, true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        FragmentManager fragmentManager = getSupportFragmentManager();
        int i = fragmentManager.getBackStackEntryCount();
        if (i > 0) {
            FragmentManager.BackStackEntry backEntry = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1);
            String str = backEntry.getName();
            BaseFragment currentFragment = (BaseFragment) fragmentManager.findFragmentByTag(str);
            currentFragment.updateToolbar();
        } else {
            fragmentMainMenu.updateToolbar();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedImage;
        if (resultCode == RESULT_OK) {
            if (data != null && data.getData() != null) {
                selectedImage = data.getData();
            } else {
                selectedImage = PreferencesManager.getInstance().getPhotoTmp();
            }
            EventBus.getDefault().post(new AvatarChangedEvent());
            switch (requestCode) {
                case PhotoFragment.PHOTO_TAKEN_ALBUM:
                    EventBus.getDefault().post(new TakenProfilePhotoEvent(selectedImage));
                    break;
                case PhotoFragment.PHOTO_SELECTED_ALBUM:
                    EventBus.getDefault().post(new SelectedProfilePhotoEvent(selectedImage));
                    break;
                case PhotoFragment.ADD_NEW_ITEM_PHOTO_TAKEN_ALBUM:
                    selectedImage = PreferencesManager.getInstance().getAddNewItemPhotoTmp();
                    EventBus.getDefault().post(new NewItemTakePhotoEvent(selectedImage));
                    break;
                case PhotoFragment.ADD_NEW_ITEM_PHOTO_SELECTED_ALBUM:
                    EventBus.getDefault().post(new NewItemPhotoSelectedEvent(selectedImage));
                    break;
            }
        }

    }

    @Override
    public void onBackPressed() {

        if (requestHandler != null) {
            requestHandler.cancelAllAsyncRequest();
        }

        KeyboardUtils.hideKeyboard(this);

        FragmentManager fragmentManager = getSupportFragmentManager();
        int i = fragmentManager.getBackStackEntryCount();
        if (i >= 2) {
            fragmentManager.popBackStack();
            FragmentManager.BackStackEntry backEntry = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 2);
            String str = backEntry.getName();
            BaseFragment currentFragment = (BaseFragment) fragmentManager.findFragmentByTag(str);
            if (currentFragment != null) {
                currentFragment.updateToolbar();
            }
        } else if (fragmentProfile != null) {

            if (fragmentProfile.isVisible()) {
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentMainMenu = new FragmentMainMenu();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragments_container, fragmentMainMenu)
                        .commit();
                fragmentProfile = null;
            }

        } else {
            super.onBackPressed();
            fragmentMainMenu.updateToolbar();
        }
    }

}
