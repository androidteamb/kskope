package com.mockup.kscope.kscope.api.request;

public class TabLikesRequest extends Request {

    private Likes request_param = new Likes();

    public TabLikesRequest(String userId) {
        setRequestCmd("get_like");
        request_param.id_user = userId;
    }

    private class Likes{
        @SuppressWarnings("unused")
        private String id_user;
    }
}
