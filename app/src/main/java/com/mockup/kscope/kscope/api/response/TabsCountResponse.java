package com.mockup.kscope.kscope.api.response;

/**
 * Created by Serhii_Slobodianiuk on 02.11.2015.
 */
public class TabsCountResponse extends Response {
    private FriendsRequestParam request_param = new FriendsRequestParam();

    public FriendsRequestParam getRequestParam() {
        return request_param;
    }

    public static class FriendsRequestParam extends InvalidParam {

        private String communities_count;
        private String friends_count;
        private String likes_count;
        private String messages_count;
        private String galleries_count;


        public String getCountCommunities() {
            return communities_count;
        }

        public String getCountFriends() {
            return friends_count;
        }

        public String getCountLikes() {
            return likes_count;
        }

        public String getCountMessages() {
            return messages_count;
        }

        public String getCountGallery() {
            return galleries_count;
        }
    }
}
