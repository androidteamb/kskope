package com.mockup.kscope.kscope.presenters;

/**
 * Created by Stafiiyevskyi on 01.12.2015.
 */
public interface FragmentCommunitySectionItemPresenter {

    void getAllItemsData(String guidItem, String userId);
    void getComments(String itemGuid,  String dataLimit, String dataOffset);
    void getCommentsCount(String guidItem);
    void getRating(String guidItem);
    void getLikesCount(String guidItem, String userId, String sessionId);

    void cancelLastSendLike();

    void sendLike(String guid_item, String sessionId, String likeTime);
    void sendRating(String guid_item, String sessionId, String voteStars);
    void sendComment(String guid_item, String comments, String pathToPhoto);

}
