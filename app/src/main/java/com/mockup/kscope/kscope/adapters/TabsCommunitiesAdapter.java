package com.mockup.kscope.kscope.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.mockup.kscope.kscope.entity.ItemSubCommunities;

import java.util.ArrayList;
import java.util.List;

public class TabsCommunitiesAdapter extends BaseExpandableListAdapter implements Filterable {

    public static final int GROUP_ITEM_LAYOUT = R.layout.item_communities_group;
    public static final int CHILD_ITEM_LAYOUT = R.layout.item_communities_child;

    //delete
    public static final String GUID_EDUCATION = "500000";
    public static final String GUID_PROFESSIONAL = "500001";
    public static final String GUID_FAMILY = "500002";
    public static final String GUID_LOCAL_ORGANIZATION = "500005";
    public static final String GUID_BUSINESS = "500003";
    public static final String GUID_SOCIAL = "500004";
    public static final String GUID_HOUSING = "500006";
    public static final String GUID_ARTS = "500007";
    public static final String GUID_ENTERTAINMENT = "500008";
    public static final String GUID_DINING = "500009";
    public static final String GUID_TRAVEL = "500010";

    private List<ItemCommunities> groups;
    private List<ItemCommunities> orig;
    private List<ItemCommunities> children;
    private Context context;
    private OnItemClick onItemClickCallback;

    public TabsCommunitiesAdapter(Context context, List<ItemCommunities> groups) {
        this.context = context;
        this.groups = groups;
    }

    public void setGroupItems(List<ItemCommunities> groups) {
        this.groups.clear();
        if (orig != null) {
            orig.clear();
        }
        this.groups = groups;
        notifyDataSetChanged();
    }

    public List<ItemCommunities> getGroups() {
        return groups;
    }

    public void setCallback(OnItemClick callback) {
        this.onItemClickCallback = callback;
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return groups.get(groupPosition).getSubCommunities().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groups.get(groupPosition).getSubCommunities().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(GROUP_ITEM_LAYOUT, null);
        }

        ImageView ivArrow = (ImageView) convertView.findViewById(R.id.iv_group_about_arrow);
        ImageView ivLogo = (ImageView) convertView.findViewById(R.id.communities_logo);
//        Picasso.with(context)
//                .load(groups.get(groupPosition).getLogo())
//                .noPlaceholder()
//                .into(ivLogo);
        if (groups.get(groupPosition).getGuid_community().equals(GUID_EDUCATION)) {
            ivLogo.setImageResource(R.drawable.img_cm_education);
        } else if (groups.get(groupPosition).getGuid_community().equals(GUID_PROFESSIONAL)) {
            ivLogo.setImageResource(R.drawable.img_cm_professional);
        } else if (groups.get(groupPosition).getGuid_community().equals(GUID_FAMILY)) {
            ivLogo.setImageResource(R.drawable.img_cm_family);
        } else if (groups.get(groupPosition).getGuid_community().equals(GUID_BUSINESS)) {
            ivLogo.setImageResource(R.drawable.img_cm_business);
        } else if (groups.get(groupPosition).getGuid_community().equals(GUID_SOCIAL)) {
            ivLogo.setImageResource(R.drawable.img_cm_social);
        } else if (groups.get(groupPosition).getGuid_community().equals(GUID_LOCAL_ORGANIZATION)) {
            ivLogo.setImageResource(R.drawable.img_cm_local);
        } else if (groups.get(groupPosition).getGuid_community().equals(GUID_HOUSING)) {
            ivLogo.setImageResource(R.drawable.img_cm_house);
        } else if (groups.get(groupPosition).getGuid_community().equals(GUID_ARTS)) {
            ivLogo.setImageResource(R.drawable.img_cm_art);
        } else if (groups.get(groupPosition).getGuid_community().equals(GUID_ENTERTAINMENT)) {
            ivLogo.setImageResource(R.drawable.img_cm_entertainment);
        } else if (groups.get(groupPosition).getGuid_community().equals(GUID_DINING)) {
            ivLogo.setImageResource(R.drawable.img_cm_dining);
        } else if (groups.get(groupPosition).getGuid_community().equals(GUID_TRAVEL)) {
            ivLogo.setImageResource(R.drawable.img_cm_transport);
        }

        if (isExpanded) {
            ivArrow.setImageResource(R.drawable.ic_arrow_light_selected);
        } else {
            ivArrow.setImageResource(R.drawable.ic_arrow_light_normal);
        }

        TextView tvGroupName = (TextView) convertView.findViewById(R.id.tv_community_group_name);
        tvGroupName.setText(groups.get(groupPosition).getName());

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(CHILD_ITEM_LAYOUT, null);
        }


        TextView tvChildName = (TextView) convertView.findViewById(R.id.tv_community_name);
        tvChildName.setText(groups.get(groupPosition).getSubCommunities().get(childPosition).getTitle());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onSubSectionItemClick(groups.get(groupPosition).getSubCommunities().get(childPosition));
            }
        });


        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults filter = new FilterResults();
                final List<ItemCommunities> results = new ArrayList<>();
                if (orig == null)
                    orig = groups;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (int j = 0; j < orig.size(); j++) {
                            if (orig.get(j).getName()
                                    .toLowerCase()
                                    .contains(constraint.toString())) {
                                results.add(orig.get(j));
                            } else {
                                for (int i = 0; i < orig.get(j).getSubCommunities().size(); i++) {
                                    if (orig.get(j).getSubCommunities().get(i).getTitle()
                                            .toLowerCase()
                                            .contains(constraint.toString())) {
                                        if (!results.isEmpty()) {
                                            if (!results.contains(orig.get(j))) {
                                                results.add(orig.get(j));
                                            }
                                        } else {
                                            results.add(orig.get(j));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    filter.values = results;
                }
                return filter;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                groups = (List<ItemCommunities>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public interface OnItemClick {
        public void onSubSectionItemClick(ItemSubCommunities communities);
    }


}
