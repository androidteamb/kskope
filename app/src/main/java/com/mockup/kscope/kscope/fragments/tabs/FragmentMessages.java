package com.mockup.kscope.kscope.fragments.tabs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.MessagesAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.GetMessageRequest;
import com.mockup.kscope.kscope.api.response.GetMessageResponse;
import com.mockup.kscope.kscope.entity.ItemMessage;
import com.mockup.kscope.kscope.events.FragmentStateEvent;
import com.mockup.kscope.kscope.events.MessagesFilterEvent;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.fragments.Updatable;
import com.mockup.kscope.kscope.fragments.main.FragmentProfile;
import com.mockup.kscope.kscope.fragments.main.FragmentSendMessage;
import com.mockup.kscope.kscope.utils.DialogUtil;
import com.mockup.kscope.kscope.utils.NameUtil;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;

import static com.mockup.kscope.kscope.adapters.ViewPagerAdapter.FragmentLifecycle;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
public class FragmentMessages extends BaseFragment implements FragmentLifecycle, Updatable {

    @InjectView(R.id.list)
    ListView mList;
    @InjectView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @InjectView(R.id.empty)
    LinearLayout emptyView;

    private Context mContext;
    private PreferencesManager preferenceManager = PreferencesManager.getInstance();

    private List<ItemMessage> messagesList;
    private MessagesAdapter mAdapter;

    @Nullable
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
        emptyView.setVisibility(View.GONE);

        sendRequest();

        messagesList = new ArrayList<>();

        mAdapter = new MessagesAdapter(mContext, messagesList);
        mAdapter.setCallback(new MessagesAdapter.OnItemClickCallback() {
            @Override
            public void onMessageClick(ItemMessage message, int position) {
                ((MainActivity) getActivity()).addFragment(FragmentSendMessage.newInstance(
                        NameUtil.getName(message.getFirstName(), message.getSecondName(), message.getUserLogin()),
                        message.getIdSender()), message.getUserLogin() + "Message");
            }
        });
        mList.setAdapter(mAdapter);
        mList.setTextFilterEnabled(false);
    }

    private void sendRequest() {
        if (Utils.isNetworkConnected(mContext)) {
            GetMessageRequest getMessageRequest = new GetMessageRequest(preferenceManager.getSessionId());
            SslRequestHandler.getInstance().sendRequest(getMessageRequest, GetMessageResponse.class, listener);
        } else {
            Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
        }
    }

    public void onEvent(MessagesFilterEvent event) {
        mAdapter.getFilter().filter(event.getQueryText());
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_messages;
    }

    @Override
    public void updateToolbar() {

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onPauseFragment() {

    }

    private SslRequestHandler.SslRequestListener listener = new SslRequestHandler.SslRequestListener<GetMessageResponse>() {

        @Override
        public void onStart() {

        }

        @Override
        public void onSuccess(GetMessageResponse response) {
            messagesList = DialogUtil.getUniqueMessages(response.getRequestParam().getMessages());
        }

        @Override
        public void onFailure(String errorMsg) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFinish() {
            if (!FragmentMessages.this.isVisible()) {
                return;
            }
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
                if (messagesList != null) {
                    mAdapter.setItems(messagesList);
                    EventBus.getDefault().post(new FragmentStateEvent(1));
                } else {
                    emptyView.setVisibility(View.VISIBLE);
                }
            }
        }
    };

    @Override
    public void update() {
        if (FragmentProfile.searchView.isIconified()) {
            sendRequest();
        }
    }
}
