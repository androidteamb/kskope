package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 09.11.2015.
 */
public class GetShortItemsList extends Request{

    private GetSubsectionItem request_param;

    public GetShortItemsList(String guid_subsection){
        setRequestCmd("get_item");
        request_param = new GetSubsectionItem();
        request_param.guid_subsection = guid_subsection;
        request_param.not_full = "1";
    }


    private class GetSubsectionItem {
        private String guid_subsection;
        private String not_full;
    }
}

