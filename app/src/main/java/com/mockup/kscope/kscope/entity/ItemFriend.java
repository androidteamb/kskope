package com.mockup.kscope.kscope.entity;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
public class ItemFriend {
    private String user_login;
    private String first_name;
    private String second_name;
    private String id_user;
    private String user_photo;
//    private int followers;
//    private String photo;
    private boolean isConfirm;

    public boolean isConfirm() {
        return isConfirm;
    }

    public void setConfirm(boolean isConfirm) {
        this.isConfirm = isConfirm;
    }

    public String getUser_login() {
        return user_login;
    }

    public void setUser_login(String user_login) {
        this.user_login = user_login;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSecond_name() {
        return second_name;
    }

    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    //    public int getFollowers() {
//        return followers;
//    }
//
//    public void setFollowers(int followers) {
//        this.followers = followers;
//    }
//
//    public String getPhoto() {
//        return photo;
//    }
//
//    public void setPhoto(String photo) {
//        this.photo = photo;
//    }
//
//    public boolean isChecked() {
//        return isChecked;
//    }
//
//    public void setChecked(boolean isChecked) {
//        this.isChecked = isChecked;
//    }
}
