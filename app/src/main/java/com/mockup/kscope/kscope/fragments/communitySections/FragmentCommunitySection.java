package com.mockup.kscope.kscope.fragments.communitySections;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.CommunitySectionRecyclerAdapter;
import com.mockup.kscope.kscope.customView.DividerItemDecoration;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.presenters.FragmentCommunitySectionPresenterImpl;
import com.mockup.kscope.kscope.viewsPresenter.FragmentCommunitySectionView;

import java.util.List;

import butterknife.InjectView;

/**
 * Created by Stafiiyevskyi on 29.10.2015.
 */
public class FragmentCommunitySection extends BaseFragment implements CommunitySectionRecyclerAdapter.OnItemClickListener, FragmentCommunitySectionView {

    @InjectView(R.id.communities_sections_recycler)
    RecyclerView recyclerView;
    @InjectView(R.id.progress)
    ProgressBar progressBar;
    @InjectView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private CommunitySectionRecyclerAdapter adapter;
    private FragmentCommunitySectionPresenterImpl presenter;

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_community_sections;
    }

    @Override
    public void updateToolbar() {
        ((MainActivity) getActivity()).setActionBarTitle("Community Sections", true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        presenter = new FragmentCommunitySectionPresenterImpl(this);
        prepareAdapter();
        prepareRecyclerView();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                presenter.getCommunitySections();
            }
        });

        progressBar.setVisibility(View.VISIBLE);
        presenter.getCommunitySections();

        return view;
    }

    private void prepareRecyclerView() {
//        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private void prepareAdapter(){
        adapter = new CommunitySectionRecyclerAdapter(getActivity(), this);
        adapter.setOnItemClickListener(this);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        ((MainActivity) getActivity()).setActionBarTitle("Community Sections", true);
    }

    @Override
    public void onPause() {
        super.onPause();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemClick(View view, int position) {
        Fragment fragment = new FragmentCommunitySubSection();
        Bundle bundle = new Bundle();
        ItemCommunities itemCommunities = adapter.getCommunityByPosition(position);
        bundle.putString(MainActivity.TITLE_KEY, itemCommunities.getName());
        bundle.putString(MainActivity.SECTION_GUID, itemCommunities.getGuid_community());
        fragment.setArguments(bundle);
        ((MainActivity) getActivity()).addFragment(fragment, itemCommunities.getName());
    }

    @Override
    public void showCommunitySections(List<ItemCommunities> sections) {
        if (this.isVisible()){
            swipeRefreshLayout.setRefreshing(false);
            progressBar.setVisibility(View.INVISIBLE);
            adapter.clearData();
            adapter.addData(sections);
            adapter.notifyDataSetChanged();
        }
    }

}