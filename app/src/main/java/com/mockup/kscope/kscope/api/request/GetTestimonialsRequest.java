package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 09.12.2015.
 */
public class GetTestimonialsRequest extends Request {

    private GetSponsor request_param;

    public GetTestimonialsRequest(String guidSponsor){
        setRequestCmd("get_testimonials");
        request_param = new GetSponsor();
        request_param.guid_sponsor = guidSponsor;
    }

    public GetTestimonialsRequest(){
        setRequestCmd("get_testimonials");
    }

    private class GetSponsor {
        @SuppressWarnings("unused")
        private String guid_sponsor;
    }

}