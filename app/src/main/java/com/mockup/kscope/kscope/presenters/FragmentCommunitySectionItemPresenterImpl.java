package com.mockup.kscope.kscope.presenters;


import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.AddCommentRequest;
import com.mockup.kscope.kscope.api.request.AddVoteItemRequest;
import com.mockup.kscope.kscope.api.request.GetAllCommentsRequest;
import com.mockup.kscope.kscope.api.request.GetLikeItemRequest;
import com.mockup.kscope.kscope.api.request.GetSubsectionItemRequest;
import com.mockup.kscope.kscope.api.request.UpdateLikeRequest;
import com.mockup.kscope.kscope.api.response.GetAllCommentsResponse;
import com.mockup.kscope.kscope.api.response.GetLikeItemResponse;
import com.mockup.kscope.kscope.api.response.GetSubsectionItemsResponse;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.utils.TimeUtil;
import com.mockup.kscope.kscope.viewsPresenter.FragmentCommunitySectionItemView;

import java.util.Date;


/**
 * Created by Stafiiyevskyi on 01.12.2015.
 */
public class FragmentCommunitySectionItemPresenterImpl implements FragmentCommunitySectionItemPresenter {

    private FragmentCommunitySectionItemView view;


    public void setView(FragmentCommunitySectionItemView view) {
        this.view = view;
    }

    @Override
    public void getAllItemsData(String guidItem, String userId) {
        GetSubsectionItemRequest request = new GetSubsectionItemRequest(null, guidItem
                , PreferencesManager.getInstance().getUserId());

        SslRequestHandler.getInstance().sendRequest(request, GetSubsectionItemsResponse.class, new SslRequestHandler.SslRequestListener<GetSubsectionItemsResponse>() {
            GetSubsectionItemsResponse itemSubsection;

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(GetSubsectionItemsResponse response) {
                itemSubsection = response;
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                if (itemSubsection != null)
                    view.showAllItemsData(itemSubsection.getSubsectionItems().get(0));
            }
        });

    }

    @Override
    public void getComments(String itemGuid, String dataLimit, String dataOffset) {

        GetAllCommentsRequest request = new GetAllCommentsRequest(itemGuid, dataLimit, dataOffset);
        SslRequestHandler.getInstance().sendRequest(request, GetAllCommentsResponse.class, new SslRequestHandler.SslRequestListener<GetAllCommentsResponse>() {
            GetAllCommentsResponse getAllCommentsResponse;
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(GetAllCommentsResponse response) {
                getAllCommentsResponse = response;
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                if (getAllCommentsResponse != null)
                    view.showComments(getAllCommentsResponse.getAllComments());
            }
        });

    }

    @Override
    public void getCommentsCount(String guidItem) {

    }

    @Override
    public void getRating(String guidItem) {

    }

    @Override
    public void getLikesCount(String guidItem, String userId, String sessionId) {

        SslRequestHandler.getInstance().sendRequest(new GetLikeItemRequest(guidItem,
                        PreferencesManager.getInstance().getSessionId(),
                        PreferencesManager.getInstance().getUserId()),
                GetLikeItemResponse.class, new SslRequestHandler.SslRequestListener<GetLikeItemResponse>() {
                    GetLikeItemResponse getLikeItemResponse;

                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(GetLikeItemResponse response) {
                        getLikeItemResponse = response;
                    }

                    @Override
                    public void onFailure(String errorMsg) {

                    }

                    @Override
                    public void onFinish() {
                        if (getLikeItemResponse!=null){
                            view.showLikesCount(getLikeItemResponse.getLikesCount());
                        }

                    }
                });

    }

    @Override
    public void cancelLastSendLike() {
        SslRequestHandler.getInstance().cancelAllAsyncRequest();
    }

    @Override
    public void sendLike(String guid_item, String sessionId, String likeTime) {

        SslRequestHandler.getInstance().sendRequest(new UpdateLikeRequest(guid_item,
                        PreferencesManager.getInstance().getSessionId(), likeTime),
                Response.class, new SslRequestHandler.SslRequestListener<Response>() {
                    Response response;
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(Response response) {
                        this.response = response;
                    }

                    @Override
                    public void onFailure(String errorMsg) {

                    }

                    @Override
                    public void onFinish() {
                        if (response != null) {
                            view.updateAllData(null);
                        }
                    }
                });

    }

    @Override
    public void sendRating(String guid_item, String sessionId, String voteStars) {
        AddVoteItemRequest request = new AddVoteItemRequest(guid_item
                , PreferencesManager.getInstance().getSessionId()
                , String.valueOf((int) Float.parseFloat(voteStars)));
        SslRequestHandler.getInstance().sendRequest(request, Response.class, new SslRequestHandler.SslRequestListener<Response>() {
            Response response;
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(Response response) {
                this.response = response;
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                view.updateAllData(response);
            }
        });


    }

    @Override
    public void sendComment(String guid_item, String comments, String pathToPhoto) {
        SslRequestHandler.getInstance()
                .sendRequest(new AddCommentRequest(PreferencesManager.getInstance()
                        .getSessionId(), guid_item, comments
                        , TimeUtil.getAddCommentTimeFromDateObject(new Date())), Response.class, new SslRequestHandler.SslRequestListener<Response>() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(Response response) {

                    }

                    @Override
                    public void onFailure(String errorMsg) {

                    }

                    @Override
                    public void onFinish() {
                        view.updateDataAfterSendComment();
                    }
                });


    }



}
