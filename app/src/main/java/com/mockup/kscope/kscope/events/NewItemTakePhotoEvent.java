package com.mockup.kscope.kscope.events;

import android.net.Uri;

/**
 * Created by Stafiiyevskyi on 10.12.2015.
 */
public class NewItemTakePhotoEvent {
    private Uri photoUri;

    public NewItemTakePhotoEvent(Uri photoUri) {
        this.photoUri = photoUri;
    }

    public Uri getPhotoUri() {
        return photoUri;
    }
}
