package com.mockup.kscope.kscope.entity;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */
public class ItemTestimonial {

    private String name;
    private String date;
    private String value;

    public ItemTestimonial(String name, String date, String value) {
        this.name = name;
        this.date = date;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


}
