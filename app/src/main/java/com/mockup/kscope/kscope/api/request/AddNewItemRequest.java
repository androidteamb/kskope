package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 07.12.2015.
 */
public class AddNewItemRequest extends Request {

    private AddNewItem request_param;

    public AddNewItemRequest(String sessionId, String guid_subsection, String time, String title, String address, String description, String phone){
        setRequestCmd("add_item");
        setSessionId(sessionId);
        request_param = new AddNewItem();
        request_param.guid_subsection = guid_subsection;
        request_param.time = time;
        request_param.address = address;
        request_param.description = description;
        request_param.phone = phone;
        request_param.title = title;
        request_param.image_url = "item.jpeg";
    }

    private class AddNewItem {
        @SuppressWarnings("unused")
        private String guid_subsection;
        @SuppressWarnings("unused")
        private String time;
        @SuppressWarnings("unused")
        private String title;
        @SuppressWarnings("unused")
        private String description;
        @SuppressWarnings("unused")
        private String address;
        @SuppressWarnings("unused")
        private String phone;
        @SuppressWarnings("unused")
        private String image_url;
    }

}