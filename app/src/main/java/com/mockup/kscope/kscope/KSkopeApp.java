package com.mockup.kscope.kscope;

import android.app.Application;
import android.content.Context;

import com.facebook.FacebookSdk;
import com.mockup.kscope.kscope.utils.FTPImageDownloader;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Serhii_Slobodianiuk on 02.11.2015.
 */
public class KSkopeApp extends Application {

    private static final String TWITTER_KEY = "6zpXuFi4wDVrHUFccnnYHLAmC";
    private static final String TWITTER_SECRET = " 6HYRG4cnx3WN5cdmoy5UA3SxmZgJRk8sH0G1jOaQ18vr7IR6V1";

    @Override
    public void onCreate() {
        super.onCreate();


        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        FacebookSdk.sdkInitialize(getApplicationContext());
        PreferencesManager.initializeInstance(this);
        initImageLoader(getApplicationContext());
    }


    public static void initImageLoader(Context context) {

        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.MAX_PRIORITY);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.threadPoolSize(9);
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());

    }


}
