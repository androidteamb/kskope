package com.mockup.kscope.kscope.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Stafiiyevskyi on 25.11.2015.
 */
public class DialogFragmentRating extends DialogFragment {


    @InjectView(R.id.ratingBar)
    RatingBar ratingBar;
    @InjectView(R.id.confirm_button)
    LinearLayout confirm;
    @InjectView(R.id.cancel_button)
    LinearLayout cancel;
    @InjectView(R.id.progress)
    ProgressBar progressBar;
    DialogRatingCallback callback;

    public static DialogFragmentRating newInstance() {
        DialogFragmentRating dialog = new DialogFragmentRating();

        return dialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_fragment_rating, container, false);
        ButterKnife.inject(this, view);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ratingBar.getRating() > 0) {
                    progressBar.setVisibility(View.VISIBLE);
                    callback.confirm(ratingBar.getRating());
                } else {
                    Toast.makeText(getActivity(), "Please enter a rating", Toast.LENGTH_LONG).show();
                }

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.cancel();
            }
        });

        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, android.R.style.Theme_Light_Panel);
    }

    public void setCallback(DialogRatingCallback callback) {
        this.callback = callback;
    }


}
