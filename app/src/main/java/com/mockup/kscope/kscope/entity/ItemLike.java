package com.mockup.kscope.kscope.entity;

/**
 * Created by Serhii_Slobodianiuk on 28.10.2015.
 */
public class ItemLike {
    private String comments;
    private String first_name;
    private String guid_comment;
    private String guid_item;
    private String id_user;
    private String likes;
    private String owner;
    private String image_url;
    private String second_name;
    private String like_time;
    private String views;
    private String vote;
    private String title;

    public String getImageUrl() {
        return image_url;
    }

    public String getTitle() {
        return title;
    }

    public String getComments() {
        return comments;
    }

    public String getFirstName() {
        return first_name;
    }

    public String getGuidComment() {
        return guid_comment;
    }

    public String getGuidItem() {
        return guid_item;
    }

    public String getIdUser() {
        return id_user;
    }

    public String getLikes() {
        return likes;
    }

    public String getOwner() {
        return owner;
    }

    public String getSecondName() {
        return second_name;
    }

    public String getTime() {
        return like_time;
    }

    public String getViews() {
        return views;
    }

    public String getVote() {
        return vote;
    }
}
