package com.mockup.kscope.kscope.entity;

/**
 * Created by Stafiiyevskyi on 09.12.2015.
 */
public class ItemTestimonials {

    String active;
    String guid_testimonials;
    String name;
    String time_added;
    String text;

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getGuid_testimonials() {
        return guid_testimonials;
    }

    public void setGuid_testimonials(String guid_testimonials) {
        this.guid_testimonials = guid_testimonials;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime_added() {
        return time_added;
    }

    public void setTime_added(String time_added) {
        this.time_added = time_added;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
