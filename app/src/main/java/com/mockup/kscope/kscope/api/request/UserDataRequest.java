package com.mockup.kscope.kscope.api.request;

public class UserDataRequest extends Request {

    private UserData request_param;

    public UserDataRequest(String sessionId, String userLogin) {
        setRequestCmd("get_user_data");
        setSessionId(sessionId);
        request_param = new UserData();
        request_param.user_login = userLogin;
    }

    private class UserData {
        @SuppressWarnings("unused")
        private String user_login;
    }

}
