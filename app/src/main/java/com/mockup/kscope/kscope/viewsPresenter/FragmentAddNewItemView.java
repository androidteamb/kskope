package com.mockup.kscope.kscope.viewsPresenter;

import com.mockup.kscope.kscope.api.response.AddNewItemResponse;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.mockup.kscope.kscope.entity.ItemSubsection;

import java.util.List;

/**
 * Created by Stafiiyevskyi on 02.12.2015.
 */
public interface FragmentAddNewItemView {

    void showCommunitySections(List<ItemCommunities> sections);
    void showCommunitySubsections(List<ItemCommunities> subsections);
    void showItemWasAdded(AddNewItemResponse response);
    void showItemWasNotAdded();
}
