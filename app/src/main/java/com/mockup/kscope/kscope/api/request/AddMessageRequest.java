package com.mockup.kscope.kscope.api.request;

/**
 * Created by Alexander Rubanskiy on 08.12.2015.
 */

public class AddMessageRequest extends Request {

    private MessageData request_param;

    public AddMessageRequest(String sessionId, String userId, String text, String time) {
        setRequestCmd("add_message");
        setSessionId(sessionId);
        request_param = new MessageData();
        request_param.id_user = userId;
        request_param.message_text = text;
        request_param.message_time = time;
    }

    private class MessageData {
        @SuppressWarnings("unused")
        private String id_user;
        private String message_text;
        private String message_time;
    }

}