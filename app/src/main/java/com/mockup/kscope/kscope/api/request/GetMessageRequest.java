package com.mockup.kscope.kscope.api.request;

/**
 * Created by Serhii_Slobodianiuk on 03.11.2015.
 */

public class GetMessageRequest extends Request {

    private RequestParam request_param;

    public GetMessageRequest(String sessionId) {
        setRequestCmd("get_message");
        setSessionId(sessionId);
    }

    public GetMessageRequest(String sessionId, String userId) {
        setRequestCmd("get_message");
        setSessionId(sessionId);
        request_param = new RequestParam();
        request_param.id_user = userId;
    }

    private class RequestParam {
        @SuppressWarnings("unused")
        private String id_user;
    }
}
