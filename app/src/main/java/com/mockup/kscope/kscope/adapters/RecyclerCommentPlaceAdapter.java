package com.mockup.kscope.kscope.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.customView.TypefaceTextView;
import com.mockup.kscope.kscope.entity.ItemCommentPlace;
import com.mockup.kscope.kscope.fragments.main.FragmentFriendProfile;
import com.mockup.kscope.kscope.fragments.main.FragmentProfile;
import com.mockup.kscope.kscope.utils.NameUtil;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.utils.TimeUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Stafiiyevskyi on 04.11.2015.
 */
public class RecyclerCommentPlaceAdapter extends RecyclerView.Adapter<RecyclerCommentPlaceAdapter.CommentPlaceViewHolder> {
    private List<ItemCommentPlace> data;
    private Context context;
    private OnItemClickListener onItemClickListener;

    private DisplayImageOptions options;

    public RecyclerCommentPlaceAdapter(Context context, List<ItemCommentPlace> data) {
        this.context = context;
        this.data = data;
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .considerExifParams(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

    }


    @Override
    public CommentPlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        CommentPlaceViewHolder viewHolder = new CommentPlaceViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CommentPlaceViewHolder holder, int position) {
        ItemCommentPlace commentPlace = data.get(position);
        holder.comment.setText(commentPlace.getComment());
        holder.daysAgo.setText(TimeUtil.getTimeAddedForComment(commentPlace.getTime()));
        holder.userName.setText(NameUtil.getName(commentPlace.getFirst_name(),
                commentPlace.getSecond_name(), commentPlace.getUser_login()));
        String urlImage = commentPlace.getUser_image();
        if (!urlImage.isEmpty()) {

            if (urlImage.startsWith("http://") || urlImage.startsWith("https://")) {
                Picasso.with(context)
                        .load(urlImage)
                        .placeholder(R.drawable.bg_calendar_selected)
                        .resize(60, 60)
                        .into(holder.userLogo);

            } else if (urlImage.startsWith("/")) {
                Picasso.with(context)
                        .load(SslRequestHandler.IMAGE_URL_PREFIX + urlImage)
                        .placeholder(R.drawable.bg_calendar_selected)
                        .resize(60, 60)
                        .into(holder.userLogo);

            } else {
                Picasso.with(context)
                        .load(R.drawable.bg_calendar_selected)
                        .into(holder.userLogo);
            }
        } else {
            Picasso.with(context)
                    .load(R.drawable.bg_calendar_selected)
                    .into(holder.userLogo);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setOnItemClickListener(final OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public ItemCommentPlace getCommentByPosition(int position) {
        ItemCommentPlace commentPlace = data.get(position);
        return commentPlace;
    }

    public void addData(List<ItemCommentPlace> data) {
        if (data != null) {
            this.data.addAll(data);
            notifyDataSetChanged();
        }

    }

    public void cleanData() {
        data.clear();
        notifyDataSetChanged();
    }


    public interface OnItemClickListener {
        void onItemCommentClick(View view, int position);
    }


    public class CommentPlaceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @InjectView(R.id.user_logo)
        public CircleImageView userLogo;
        @InjectView(R.id.username)
        public TypefaceTextView userName;
        @InjectView(R.id.comment)
        public TypefaceTextView comment;
        @InjectView(R.id.text_day_ago)
        public TypefaceTextView daysAgo;


        public CommentPlaceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            userLogo.setOnClickListener(this);
            userName.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (data.get(getAdapterPosition()).getId_user().equals(PreferencesManager.getInstance().getUserId())) {

                ((MainActivity) context).addFragment(FragmentProfile.newInstance(), "My profile");

            } else {

                ((MainActivity) context).addFragment(FragmentFriendProfile.newInstance(data.get(getAdapterPosition()).getUser_login())
                        , data.get(getAdapterPosition()).getUsername());
            }
//            PopupMenu menu = new PopupMenu(context, v);
//            menu.inflate(R.menu.popup_menu_item_event);
//            menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                @Override
//                public boolean onMenuItemClick(MenuItem itemMenu) {
//
//
//
//                    return false;
//                }
//            });
//            menu.show();
        }
    }
}
