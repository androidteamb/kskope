package com.mockup.kscope.kscope.fragments.tabs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.LikesAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.TabLikesRequest;
import com.mockup.kscope.kscope.api.response.UserLikesResponse;
import com.mockup.kscope.kscope.entity.ItemLike;
import com.mockup.kscope.kscope.events.FragmentStateEvent;
import com.mockup.kscope.kscope.events.HideMenuEvent;
import com.mockup.kscope.kscope.events.LikesFilterEvent;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.fragments.Updatable;
import com.mockup.kscope.kscope.fragments.communitySections.FragmentCommunitySectionItem;
import com.mockup.kscope.kscope.fragments.main.FragmentProfile;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;

import static com.mockup.kscope.kscope.adapters.LikesAdapter.OnLikesClick;
import static com.mockup.kscope.kscope.adapters.ViewPagerAdapter.FragmentLifecycle;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
public class FragmentLikes extends BaseFragment implements FragmentLifecycle, OnLikesClick, Updatable {

    private static final String USER_ID = "user_id";

    @InjectView(R.id.list)
    ListView mList;
    @InjectView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @InjectView(R.id.empty)
    LinearLayout emptyView;

    private Context mContext;

    private List<ItemLike> likesList;
    private LikesAdapter mAdapter;

    private PreferencesManager preferenceManager = PreferencesManager.getInstance();

    private String userId;

    public static Fragment newInstance(String userId) {
        FragmentLikes fragment = new FragmentLikes();
        Bundle args = new Bundle();
        args.putString(USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userId = getArguments().getString(USER_ID);
        if (userId.equals("")) {
            userId = preferenceManager.getUserId();
        }
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        emptyView.setVisibility(View.GONE);

        sendRequest();

        likesList = new ArrayList<>();

        mAdapter = new LikesAdapter(this, mContext, likesList);
        mList.setAdapter(mAdapter);
        mList.setTextFilterEnabled(false);
    }

    private void sendRequest() {
        if (Utils.isNetworkConnected(mContext)) {
            TabLikesRequest likesRequest = new TabLikesRequest(userId);
            SslRequestHandler.getInstance().sendRequest(likesRequest, UserLikesResponse.class, likesRequestListener);
        } else {
            Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
        }
    }

    public void onEvent(LikesFilterEvent event) {
        mAdapter.getFilter().filter(event.getQueryText());
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_likes;
    }

    @Override
    public void updateToolbar() {

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onLikeItemClick(String guidItem) {
        EventBus.getDefault().post(new HideMenuEvent());
        Fragment fragment = new FragmentCommunitySectionItem();
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.TITLE_KEY, null);
        bundle.putString(MainActivity.ITEM_GUID, guidItem);
        fragment.setArguments(bundle);
        ((MainActivity) getActivity()).addFragment(fragment, null);
    }

    @Override
    public void onPauseFragment() {

    }

    private SslRequestHandler.SslRequestListener likesRequestListener = new SslRequestHandler.SslRequestListener<UserLikesResponse>() {

        @Override
        public void onStart() {

        }

        @Override
        public void onSuccess(UserLikesResponse response) {
            likesList = response.getLikes();
        }

        @Override
        public void onFailure(String errorMsg) {
            Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFinish() {
            if (!FragmentLikes.this.isVisible()) {
                return;
            }
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }
            if (likesList != null) {
                mAdapter.setItems(likesList);
                EventBus.getDefault().post(new FragmentStateEvent(3));
            } else {
                emptyView.setVisibility(View.VISIBLE);
            }

        }
    };

    @Override
    public void update() {
        if (FragmentProfile.searchView.isIconified()) {
            sendRequest();
        }
    }
}
