package com.mockup.kscope.kscope.api.response;

import com.mockup.kscope.kscope.entity.ItemMessage;

import java.util.ArrayList;

/**
 * Created by Serhii_Slobodianiuk on 03.11.2015.
 */
public class GetMessageResponse extends Response {

    private MessageRequestParam request_param = new MessageRequestParam();

    public MessageRequestParam getRequestParam() {
        return request_param;
    }

    public static class MessageRequestParam extends InvalidParam {

        private String data_count;
        private String data_limit;
        private String data_offset;

        private ArrayList<ItemMessage> messages;

        public String getDataCount() {
            return data_count;
        }

        public String getDataLimit() {
            return data_limit;
        }

        public String getDataOffset() {
            return data_offset;
        }

        public ArrayList<ItemMessage> getMessages() {
            return messages;
        }
    }
}
