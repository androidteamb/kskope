package com.mockup.kscope.kscope.events;

/**
 * Created by Serhii_Slobodyanuk on 15.12.2015.
 */

public class FragmentStateEvent {

    private boolean[] fragmentsState = new boolean[5];
    private int position;

    public FragmentStateEvent(int position) {
        this.position = position;
        fragmentsState[position] = true;
    }

    public int getPosition() {
        return position;
    }

    public boolean[] getFragmentsState(){
        return fragmentsState;
    }
}
