package com.mockup.kscope.kscope.fragments.main;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.MessageDialogAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.AddMessageRequest;
import com.mockup.kscope.kscope.api.request.GetMessageRequest;
import com.mockup.kscope.kscope.api.request.Request;
import com.mockup.kscope.kscope.api.response.GetMessageResponse;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.constants.TimeConstants;
import com.mockup.kscope.kscope.customView.DividerItemDecoration;
import com.mockup.kscope.kscope.entity.ItemMessage;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.utils.PreferencesManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnEditorAction;

/**
 * Created by mpodolsky on 07.12.2015.
 */
public class FragmentSendMessage extends BaseFragment {

    public static final String RECEIVER = "receiver";
    public static final String RECEIVER_ID = "receiver_id";

    @InjectView(R.id.messages_list)
    RecyclerView mMessagesRecyclerView;
    @InjectView(R.id.progress)
    ProgressBar mProgress;
    @InjectView(R.id.et_message)
    EditText mEtMessage;

    private Activity mActivity;
    private String mReceiver;
    private String mReceiverId;
    private String mUserId;
    private MessageDialogAdapter mAdapter;
    private List<ItemMessage> mMessages = new ArrayList<>();
    PreferencesManager preferencesManager = PreferencesManager.getInstance();

    private final Handler mHandler = new Handler();
    private Runnable mUpdateMsgRunnable;
    private boolean isRunning;

    public static Fragment newInstance(String receiver, String receiverId) {
        Fragment f = new FragmentSendMessage();
        Bundle args = new Bundle();
        args.putString(RECEIVER, receiver);
        args.putString(RECEIVER_ID, receiverId);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        mActivity = getActivity();
        mUserId = preferencesManager.getUserId();
        
        Bundle args = getArguments();
        mReceiver = args.getString(RECEIVER);
        mReceiverId = args.getString(RECEIVER_ID);

        initRecycler();
        showProgress();
        updateToolbar();

        isRunning = true;
        mUpdateMsgRunnable = new Runnable() {
            @Override
            public void run() {
                if (isRunning) {
                    updateMessages();
                    mHandler.postDelayed(mUpdateMsgRunnable, TimeConstants.TEN_SECONDS);
                }
            }
        };
        mHandler.post(mUpdateMsgRunnable);
    }

    @Override
    public void onDestroy() {
        stopTasks();
        super.onDestroy();
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_send_message;
    }

    @Override
    public void updateToolbar() {
        ((MainActivity) mActivity).setActionBarTitle(mReceiver, true);
        Toolbar toolbar = ButterKnife.findById(mActivity, R.id.toolbar);
        ButterKnife.findById(toolbar, R.id.menu_messages).setVisibility(View.GONE);
        ButterKnife.findById(toolbar, R.id.searchView).setVisibility(View.GONE);
    }

    @OnClick(R.id.send)
    public void send() {
        sendMessage();
    }

    @OnEditorAction(R.id.et_message)
    boolean onEditorAction(EditText v, int actionId, KeyEvent event) {
        if (event == null && actionId == EditorInfo.IME_ACTION_SEND) {
            sendMessage();
        }
        return true;
    }

    private void initRecycler() {
        mAdapter = new MessageDialogAdapter(mActivity, preferencesManager.getUserId(), mMessages);
        LinearLayoutManager lm = new LinearLayoutManager(mActivity);
        mMessagesRecyclerView.setLayoutManager(lm);
        mMessagesRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        mMessagesRecyclerView.setAdapter(mAdapter);
    }
    
    private void updateMessages() {
        Request request = new GetMessageRequest(preferencesManager.getSessionId(), mReceiverId);
        SslRequestHandler.getInstance().sendRequest(request, GetMessageResponse.class, new SslRequestHandler.SslRequestListener<GetMessageResponse>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(GetMessageResponse response) {
                if (response != null && response.getRequestParam() != null
                        && response.getRequestParam().getMessages() != null) {
                    List<ItemMessage> items = response.getRequestParam().getMessages();
                    if (items.size() > 0) {
                        mMessages.clear();
                        mMessages.addAll(items);
                    }
                }

            }

            @Override
            public void onFailure(String errorMsg) {
                Toast.makeText(mActivity, getString(R.string.error_getting_messages), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {
                if (mMessages != null && mMessages.size() > 0) {
                    mAdapter.notifyDataSetChanged();
                    showLastMessage();
                }
                hideProgress();
            }
        });
    }

    private void sendMessage() {
        if (mEtMessage == null || TextUtils.isEmpty(mEtMessage.getText().toString())) {
            return;
        }

        String text = mEtMessage.getText().toString();
        long timeStamp = Calendar.getInstance().getTimeInMillis();
        Date date = new Date(timeStamp);
        String formattedTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);

        ItemMessage message = new ItemMessage();
        message.setMessage_time(formattedTime);
        message.setMessage_text(text);
        message.setId_sender(mUserId);
        message.setFirst_name(preferencesManager.getFirstName());
        message.setSecond_name(preferencesManager.getLastName());
        message.setId_reciever(mReceiverId);
        mMessages.add(message);
        mAdapter.notifyDataSetChanged();
        showLastMessage();
        mEtMessage.setText("");
        Request request = new AddMessageRequest(preferencesManager.getSessionId(), mReceiverId, text, formattedTime);
        SslRequestHandler.getInstance().sendRequest(request, Response.class, new SslRequestHandler.SslRequestListener<Response>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(Response response) {

            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void showProgress() {
        if (mProgress != null) {
            mProgress.setVisibility(View.VISIBLE);
        }
    }

    private void hideProgress() {
        if (mProgress != null) {
            mProgress.setVisibility(View.GONE);
        }
    }

    private void stopTasks() {
        isRunning = false;
        mHandler.removeCallbacksAndMessages(null);
        SslRequestHandler.getInstance().cancelAllAsyncRequest();
    }

    private void showLastMessage() {
        if (mAdapter.getItemCount() - 1 >= 0) {
            mMessagesRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
        }
    }
}
