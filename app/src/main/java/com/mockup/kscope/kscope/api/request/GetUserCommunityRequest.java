package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 06.11.2015.
 */
public class GetUserCommunityRequest extends Request {

    private GetCommunity request_param;

    public GetUserCommunityRequest(String guidCommunity, String userId) {
        setRequestCmd("get_user_community");
        request_param = new GetCommunity();
        request_param.guid_community = guidCommunity;
        request_param.id_user = userId;
    }

    public GetUserCommunityRequest() {
        setRequestCmd("get_user_community");
    }

    private class GetCommunity {
        @SuppressWarnings("unused")
        private String guid_community;
        @SuppressWarnings("unused")
        private String id_user;
    }
}
