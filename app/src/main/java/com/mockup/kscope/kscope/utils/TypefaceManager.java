package com.mockup.kscope.kscope.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.SparseArray;


public class TypefaceManager {

    public final static int RALEWAY_REGULAR = 0;
    public final static int RALEWAY_BOLD = 1;
    public final static int RALEWAY_MEDIUM = 2;
    public final static int RALEWAY_LIGHT = 3;
    public final static int RALEWAY_SEMIBOLD = 4;
    /**
     * Array of created typefaces for later reused.
     */
    private final static SparseArray<Typeface> mTypefaces = new SparseArray<>(20);

    /**
     * Obtain typeface.
     *
     * @param context       The Context the widget is running in, through which it can access the current theme, resources, etc.
     * @param typefaceValue The value of "typeface" attribute
     * @return specify {@link android.graphics.Typeface}
     * @throws IllegalArgumentException if unknown `typeface` attribute value.
     */
    public static Typeface obtainTypeface(Context context, int typefaceValue) throws IllegalArgumentException {
        Typeface typeface = mTypefaces.get(typefaceValue);
        if (typeface == null) {
            typeface = createTypeface(context, typefaceValue);
            mTypefaces.put(typefaceValue, typeface);
        }
        return typeface;
    }

    /**
     * Create typeface from assets.
     *
     * @param context       The Context the widget is running in, through which it can
     *                      access the current theme, resources, etc.
     * @param typefaceValue The value of "typeface" attribute
     * @return typeface {@link android.graphics.Typeface}
     * @throws IllegalArgumentException if unknown `typeface` attribute value.
     */
    private static Typeface createTypeface(Context context, int typefaceValue) throws IllegalArgumentException {
        Typeface typeface;
        switch (typefaceValue) {
            case RALEWAY_REGULAR:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Regular.ttf");
                break;
            case RALEWAY_BOLD:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Bold.ttf");
                break;
            case RALEWAY_MEDIUM:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Medium.ttf");
                break;
            case RALEWAY_LIGHT:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Light.ttf");
                break;
            case RALEWAY_SEMIBOLD:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-SemiBold.ttf");
                break;
            default:
                throw new IllegalArgumentException("Unknown `typeface` attribute value " + typefaceValue);
        }
        return typeface;
    }

}
