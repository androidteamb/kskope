package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 23.11.2015.
 */
public class GetEventItemCalendarRequest extends Request {

    private GetEventItemCalendar request_param;

    public GetEventItemCalendarRequest(String sessionId, String date){
        setRequestCmd("get_item_from_calendar");
        setSessionId(sessionId);
        request_param = new GetEventItemCalendar();
        request_param.date = date;

    }

    private class GetEventItemCalendar {
        @SuppressWarnings("unused")
        private String date;
    }

}
