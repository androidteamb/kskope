package com.mockup.kscope.kscope.presenters;

import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.EntranceToCommunityRequest;
import com.mockup.kscope.kscope.api.request.ExitFromCommunities;
import com.mockup.kscope.kscope.api.request.GetCommunityRequest;
import com.mockup.kscope.kscope.api.response.GetCommunityResponse;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.viewsPresenter.FragmentCommunitiesView;

/**
 * Created by Stafiiyevskyi on 04.12.2015.
 */

public class FragmentCommunitiesPresenterImpl implements FragmentCommunitiesPresenter{

    private FragmentCommunitiesView view;

    public FragmentCommunitiesPresenterImpl(FragmentCommunitiesView view) {
        this.view = view;
    }

    @Override
    public void entranceToCommunity(String guidCommunity) {
        EntranceToCommunityRequest requestEntrance = new EntranceToCommunityRequest(guidCommunity, PreferencesManager.getInstance().getSessionId());
        SslRequestHandler.getInstance().sendRequest(requestEntrance, Response.class, new SslRequestHandler.SslRequestListener<Response>() {

            private Response response;

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(Response response) {
                this.response = response;
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                if (response != null) {
                    view.entranceShow(response);
                }
            }
        });
    }

    @Override
    public void exitFromCommunity(String guidCommunity) {
        ExitFromCommunities exitFromCommunitiesRequest = new ExitFromCommunities(PreferencesManager.getInstance().getSessionId(), guidCommunity);
        SslRequestHandler.getInstance().sendRequest(exitFromCommunitiesRequest, Response.class, new SslRequestHandler.SslRequestListener<Response>() {
            Response response;
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(Response response) {
                this.response = response;
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                view.exitShow(response);
            }
        });
    }

    @Override
    public void getCommunities() {
        GetCommunityRequest getCommunityRequest = new GetCommunityRequest(PreferencesManager.getInstance().getUserId());
        SslRequestHandler.getInstance().sendRequest(getCommunityRequest, GetCommunityResponse.class, new SslRequestHandler.SslRequestListener<GetCommunityResponse>() {

            private GetCommunityResponse response;

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(GetCommunityResponse response) {
                this.response = response;
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                if (response != null) {
                    if (response.getCommunity() != null) {
                        view.showCommunities(response.getCommunity());
                    }
                }
            }
        });
    }

}
