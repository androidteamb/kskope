package com.mockup.kscope.kscope.fragments.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.FriendsAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.AddUserRequest;
import com.mockup.kscope.kscope.api.request.FriendsRequest;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.api.response.UsersResponse;
import com.mockup.kscope.kscope.dialogs.ConfirmDialog;
import com.mockup.kscope.kscope.dialogs.DialogCallback;
import com.mockup.kscope.kscope.entity.ItemFriend;
import com.mockup.kscope.kscope.events.FriendsFilterEvent;
import com.mockup.kscope.kscope.events.HideMenuEvent;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.utils.PreferencesManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import de.greenrobot.event.EventBus;

import static com.mockup.kscope.kscope.adapters.FriendsAdapter.OnFriendsClick;
import static com.mockup.kscope.kscope.adapters.ViewPagerAdapter.FragmentLifecycle;

/**
 * Created by Serhii_Slobodianiuk on 20.11.2015.
 */
public class FragmentUsers extends BaseFragment implements FragmentLifecycle, OnFriendsClick {

    @InjectView(R.id.list)
    ListView mList;
    @InjectView(R.id.progress_bar)
    ProgressBar mProgressBar;

    private Context mContext;
    private PreferencesManager preferenceManager = PreferencesManager.getInstance();

    private ArrayList<ItemFriend> usersList;
    private FriendsAdapter mAdapter;
    private SslRequestHandler.SslRequestListener<UsersResponse> requestUsersListener;
    private List<UsersResponse.UserData> users;

    private Thread mThread;
    private SslRequestHandler.SslRequestListener<Response> requestAddUserListener;

    public static Fragment newInstance() {
        FragmentUsers fragmentUsers = new FragmentUsers();
        return fragmentUsers;
    }

    @Nullable
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    private void sendRequest() {
        FriendsRequest friendsRequest = new FriendsRequest(preferenceManager.getSessionId(), "1", "1");
        SslRequestHandler
                .getInstance()
                .sendRequest(friendsRequest, UsersResponse.class, requestUsersListener);
    }

    private void requestListeners() {
        requestUsersListener = new SslRequestHandler.SslRequestListener<UsersResponse>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(UsersResponse response) {
                usersList = new ArrayList<>();
                users = response.getFriends();

            }

            @Override
            public void onFailure(String errorMsg) {
                Toast.makeText(getActivity(), "Sorry, no connection to server. Please try again later.", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFinish() {
                if (!FragmentUsers.this.isVisible()) {
                    return;
                }
                if (users != null) {
                    for (int i = 0; i < users.size(); i++) {
                        ItemFriend friend = new ItemFriend();
                        UsersResponse.UserData userData = users.get(i);
                        friend.setUser_login(userData.getUser_login());
                        friend.setFirst_name(userData.getFirst_name());
                        friend.setSecond_name(userData.getSecond_name());
                        friend.setId_user(userData.getId_user());
                        friend.setUser_photo(userData.getUser_picture());
                        usersList.add(friend);
                    }
                }
                if (mProgressBar != null) {
                    mProgressBar.setVisibility(View.GONE);
                    mAdapter.setItems(usersList);
                }
            }
        };

        requestAddUserListener = new SslRequestHandler.SslRequestListener<Response>() {

            public Response response;

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(Response response) {
                this.response = response;
            }

            @Override
            public void onFailure(String errorMsg) {
                Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {
                if (response.getRequestResult().equals("0")) {
                    Toast.makeText(mContext, "Cannot send request.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "Successful! Please, wait for response.", Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        EventBus.getDefault().post(new HideMenuEvent());
        FragmentProfile.searchView.setVisibility(View.VISIBLE);

        requestListeners();

        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                sendRequest();
            }
        });
        mThread.start();

        mAdapter = new FriendsAdapter(this, mContext, new ArrayList<ItemFriend>());
        mList.setAdapter(mAdapter);
        mList.setTextFilterEnabled(false);
    }

    private void addUser(String userLogin) {
        AddUserRequest addUserRequest = new AddUserRequest(preferenceManager.getSessionId(), userLogin);
        SslRequestHandler
                .getInstance()
                .sendRequest(addUserRequest, Response.class, requestAddUserListener);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_friends;
    }

    public void onEvent(FriendsFilterEvent event) {
        mAdapter.getFilter().filter(event.getQueryText());
    }

    @Override
    public void updateToolbar() {
        ((MainActivity) getActivity()).setActionBarTitle("Users", true);
    }

    @Override
    public void onPauseFragment() {
        if (mThread != null) {
            mThread.interrupt();
            mThread = null;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onFriendsItemClick(final String userLogin) {
        ConfirmDialog dialog = ConfirmDialog.newInstance(R.string.dialog_add_friends,
                R.string.dialog_add_friends_messages,
                R.string.dialog_confirm,
                R.string.dialog_cancel);
        dialog.setCallback(new DialogCallback.EmptyCallback() {
            @Override
            public void confirm() {
                mThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        addUser(userLogin);
                    }
                });
                mThread.start();
            }
        });
        dialog.show(getFragmentManager(), "dialog");
    }
}
