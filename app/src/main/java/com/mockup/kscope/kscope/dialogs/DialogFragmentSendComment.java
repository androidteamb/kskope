package com.mockup.kscope.kscope.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.customView.TypefaceTextView;
import com.mockup.kscope.kscope.utils.ImageScaleUtils;
import com.mockup.kscope.kscope.utils.RealPathUtil;
import com.mockup.kscope.kscope.utils.TypefaceManager;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Stafiiyevskyi on 25.11.2015.
 */
public class DialogFragmentSendComment extends DialogFragment {


    @InjectView(R.id.edit_text_comment)
    EditText editTextComment;
    @InjectView(R.id.confirm_button)
    LinearLayout confirm;
    @InjectView(R.id.cancel_button)
    LinearLayout cancel;
    @InjectView(R.id.add_image_comment)
    TypefaceTextView addImageToComment;
    @InjectView(R.id.added_image)
    ImageView addedImage;
    @InjectView(R.id.progress)
    ProgressBar progressBar;

    private DialogSendCommentCallback callback;
    private String imagePath;

    public static DialogFragmentSendComment newInstance() {
        DialogFragmentSendComment dialog = new DialogFragmentSendComment();
        return dialog;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, android.R.style.Theme_Light_Panel);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_fragment_send_comment, container, false);
        ButterKnife.inject(this, view);
        editTextComment.setTypeface(TypefaceManager.obtainTypeface(getActivity(), TypefaceManager.RALEWAY_REGULAR));
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!editTextComment.getText().toString().isEmpty()) {
                    progressBar.setVisibility(View.VISIBLE);
                    callback.confirm(editTextComment.getText().toString(), imagePath);
                } else {
                    Toast.makeText(getActivity(), "Please, enter your comment", Toast.LENGTH_LONG).show();
                }

            }
        });
        addImageToComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
//                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 0);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.cancel();
            }
        });

        return view;
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        callback.dismiss();
    }

    public void setCallback(DialogSendCommentCallback callback) {
        this.callback = callback;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && data != null) {
            String realPath = null;
            realPath = RealPathUtil.getPath(getActivity(),data.getData());


            if (realPath == null) {
                setImageSdk20Plus(data.getData());
            } else {
                imagePath = realPath;
                AsyncImageScale asyncImageScale = new AsyncImageScale(imagePath);
                asyncImageScale.execute();
            }

        }

    }


    private void setImageSdk20Plus(Uri uri) {

        Picasso.with(getActivity()).load(uri).resize(80, 80).centerCrop().into(addedImage);

    }

    private void setImage(int sdk, String realPath) {

        Uri uriFromPath = Uri.fromFile(new File(realPath));
        Picasso.with(getActivity()).load(uriFromPath).resize(80, 80).centerCrop().into(addedImage);

        Log.d("HMKCODE", "Build.VERSION.SDK_INT:" + sdk);
        Log.d("HMKCODE", "Real Path: " + realPath);
    }

    private class AsyncImageScale extends AsyncTask<Void, Void, Void>{
        String path;
        String newPath;
        public AsyncImageScale(String path) {
            this.path = path;
        }

        @Override
        protected Void doInBackground(Void... params) {
            newPath = ImageScaleUtils.scaleImage(path);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            imagePath = newPath;
            setImage(Build.VERSION.SDK_INT, newPath);
        }
    }
}
