package com.mockup.kscope.kscope.api.request;

/**
 * Created by Alexander Rubanskiy on 30.10.2015.
 */

public abstract class Request {

    private String request_cmd;
    private String session_id;

    public String getRequest_cmd() {
        return request_cmd;
    }

    public void setRequest_cmd(String request_cmd) {
        this.request_cmd = request_cmd;
    }

    public String getSessionId() {
        return session_id;
    }

    public void setSessionId(String session_id) {
        this.session_id = session_id;
    }

    protected String getRequestCmd() {
        return request_cmd;
    }

    protected void setRequestCmd(String requestCmd) {
        this.request_cmd = requestCmd;
    }

}
