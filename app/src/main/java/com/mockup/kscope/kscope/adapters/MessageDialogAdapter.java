package com.mockup.kscope.kscope.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.entity.ItemMessage;

import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Yuriy Mysochenko on 24.02.2015.
 */
public class MessageDialogAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_SENT = 0;
    private static final int TYPE_RECEIVED = 1;

    private Context context;
    private List<ItemMessage> mItems;
    private String ownerId;

    private OnItemClickCallback mCallback;

    public interface OnItemClickCallback {
        void onMessageClick(ItemMessage message, int position);
    }

    public MessageDialogAdapter(Context context, String ownerId, List<ItemMessage> messages) {
        this.context = context;
        this.ownerId = ownerId;
        if (messages == null) {
            mItems = Collections.emptyList();
        }
        mItems = messages;
    }

    public void setItems(List<ItemMessage> newItems) {
        mItems.clear();
        if (newItems != null) {
            mItems.addAll(newItems);
            notifyDataSetChanged();
        } else {
            notifyDataSetChanged();
        }
    }

    public void addItems(List<ItemMessage> newItems) {
        if (newItems != null) {
            mItems.addAll(newItems);
            notifyItemRangeInserted(getItemCount(), newItems.size());
        }
    }

    public void addItem(ItemMessage message) {
        mItems.add(message);
        notifyDataSetChanged();
    }

    public void setCallback(OnItemClickCallback callback) {
        this.mCallback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v;
        switch (viewType) {
            case TYPE_RECEIVED:
                v = inflater.inflate(R.layout.item_message_received, parent, false);
                return new MessageViewHolder(v);
            case TYPE_SENT:
                v = inflater.inflate(R.layout.item_message_sent, parent, false);
                return new MessageViewHolder(v);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType +
                " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ItemMessage message = getItem(position);
        final MessageViewHolder viewHolder = (MessageViewHolder) holder;

        viewHolder.message.setText(message.getMessageText());
        viewHolder.time.setText(message.getMessageTime());

        viewHolder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallback != null) {
                    mCallback.onMessageClick(message, position);
                }
            }
        });
    }

    private ItemMessage getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        // Note that unlike in ListView adapters, types don't have to be contiguous
        ItemMessage item = getItem(position);
        if (item.getIdSender().equals(ownerId)) {
            return TYPE_SENT;
        } else {
            return TYPE_RECEIVED;
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    static class MessageViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.root)
        RelativeLayout root;
        @InjectView(R.id.message)
        TextView message;
        @InjectView(R.id.message_time)
        TextView time;

        public MessageViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

    }

}
