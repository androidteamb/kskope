package com.mockup.kscope.kscope.fragments.signup;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Order;
import com.mobsandgeeks.saripaar.annotation.Pattern;
import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.SignupContainerActivity;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.utils.TypefaceManager;

import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by MPODOLSKY on 12.06.2015.
 */
public class SignupSecondFragment extends BaseFragment implements Validator.ValidationListener {


    @NotEmpty
    @InjectView(R.id.first_name)
    EditText firstName;


    @NotEmpty
    @InjectView(R.id.last_name)
    EditText lastName;

    @InjectView(R.id.street_address_1)
    EditText streetAddressFirst;
    @InjectView(R.id.street_address_2)
    EditText streetAddressSecond;
    @NotEmpty
    @Pattern(regex = "[0-9]+", message = "Should contain only numbers")
    @InjectView(R.id.zipcode)
    EditText zipcode;
    @InjectView(R.id.city)
    EditText city;
    @InjectView(R.id.state)
    EditText state;
    @InjectView(R.id.work_phone)
    EditText workPhone;
    @InjectView(R.id.mobile_phone)
    EditText mobilePhone;
    @InjectView(R.id.next)
    Button next;

    Validator validator;

    public static Fragment newInstance() {
        SignupSecondFragment fragment = new SignupSecondFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((SignupContainerActivity) getActivity()).setTitle(String.format(getString(R.string.signup_title_n), SignupContainerActivity.SECOND_FRAGMENT_ID));
        validator = new Validator(this);
        validator.setValidationListener(this);
        setTypefaceElements();
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_signup_p2;
    }

    @Override
    public void updateToolbar() {

    }

    @OnClick(R.id.next)
    public void next() {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        ((SignupContainerActivity) getActivity()).setFragment(SignupContainerActivity.THIRD_FRAGMENT_ID);
    }

    private void setTypefaceElements() {
        firstName.setTypeface(TypefaceManager.obtainTypeface(getActivity(),TypefaceManager.RALEWAY_REGULAR));
        lastName.setTypeface(TypefaceManager.obtainTypeface(getActivity(),TypefaceManager.RALEWAY_REGULAR));
        streetAddressFirst.setTypeface(TypefaceManager.obtainTypeface(getActivity(),TypefaceManager.RALEWAY_REGULAR));
        streetAddressSecond.setTypeface(TypefaceManager.obtainTypeface(getActivity(),TypefaceManager.RALEWAY_REGULAR));
        zipcode.setTypeface(TypefaceManager.obtainTypeface(getActivity(),TypefaceManager.RALEWAY_REGULAR));
        city.setTypeface(TypefaceManager.obtainTypeface(getActivity(),TypefaceManager.RALEWAY_REGULAR));
        state.setTypeface(TypefaceManager.obtainTypeface(getActivity(),TypefaceManager.RALEWAY_REGULAR));
        workPhone.setTypeface(TypefaceManager.obtainTypeface(getActivity(),TypefaceManager.RALEWAY_REGULAR));
        mobilePhone.setTypeface(TypefaceManager.obtainTypeface(getActivity(),TypefaceManager.RALEWAY_REGULAR));
        next.setTypeface(TypefaceManager.obtainTypeface(getActivity(),TypefaceManager.RALEWAY_BOLD));
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this.getActivity());

            // Display error messages
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this.getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
