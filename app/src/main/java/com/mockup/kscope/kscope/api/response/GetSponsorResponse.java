package com.mockup.kscope.kscope.api.response;

import com.mockup.kscope.kscope.entity.ItemSponsor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stafiiyevskyi on 02.11.2015.
 */
public class GetSponsorResponse extends Response {

    private SponsorRequestParam request_param = new SponsorRequestParam();

    public List<ItemSponsor> getSponsor(){
        return request_param.sponsor;
    }

    public SponsorRequestParam getRequestParam() {
        return request_param;
    }

    public static class SponsorRequestParam extends InvalidParam {

        private String data_count;
        private String data_limit;
        private String data_offset;
        private ArrayList<ItemSponsor> sponsor;

        public String getDataCount() {
            return data_count;
        }

        public String getDataLimit() {
            return data_limit;
        }

        public String getDataOffset() {
            return data_offset;
        }

        public ArrayList<ItemSponsor> getSponsor(){
            return sponsor;
        }
    }

}
