package com.mockup.kscope.kscope.utils;

import android.util.Log;

import com.nostra13.universalimageloader.core.download.ImageDownloader;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Stafiiyevskyi on 26.11.2015.
 */
public class FTPImageDownloader implements ImageDownloader {
    public static String FTP_SERVER_HOST = "52.34.106.138";
    public static int FTP_SERVER_PORT = 0;
    public static String FTP_LOGIN = "kscope_user";
    public static String FTP_PASSWORD = "kscope";

    @Override
    public InputStream getStream(String imageUri, Object extra) throws IOException {
        return getFTPStream(imageUri);
    }

    public InputStream getFTPStream(String url) throws IOException {
        FTPClient con = null;
        final String imageUrl = url;

        try {
            con = new FTPClient();
            con.connect(FTP_SERVER_HOST);

            if (con.login(FTP_LOGIN, FTP_PASSWORD)) {
                con.enterLocalPassiveMode();
                con.setFileType(FTP.BINARY_FILE_TYPE);

                return con.retrieveFileStream(imageUrl);
            }
        } catch (Exception e) {
            Log.v("download result", "failed");
            e.printStackTrace();
        } finally {
            con.logout();
            con.disconnect();
        }

        return null;

    }
}
