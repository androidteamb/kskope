package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 11.12.2015.
 */
public class ConvertImageRequest extends Request{
    private ConvertImage request_param;

    public ConvertImageRequest(String imageUrl){
        setRequestCmd("convert_image");
        request_param = new ConvertImage();
        request_param.image_url = imageUrl;
    }


    private class ConvertImage {
        @SuppressWarnings("unused")
        private String image_url;
    }

}
