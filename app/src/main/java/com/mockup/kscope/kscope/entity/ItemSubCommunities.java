package com.mockup.kscope.kscope.entity;

/**
 * Created by Serhii_Slobodianiuk on 08.12.2015.
 */
public class ItemSubCommunities {
    private String guid_subsection;
    private String title;

    public ItemSubCommunities(String guid_subsection, String title) {
        this.guid_subsection = guid_subsection;
        this.title = title;
    }

    public String getGuidSubsection() {
        return guid_subsection;
    }

    public String getTitle() {
        return title;
    }
}
